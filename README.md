# Procedural Level Generator for Unity
This is the project for my master's thesis, where I'm creating a procedural level generator for Unity based on the [wave function collapse algorithm](https://github.com/mxgmn/WaveFunctionCollapse).

A unique aspect of this generator is that it can create levels with multiple directions of gravity, making it possible to walk on walls or ceilings.

[Latest video](https://youtu.be/bSssbCbqDf4)
