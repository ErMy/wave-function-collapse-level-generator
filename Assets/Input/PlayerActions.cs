// GENERATED AUTOMATICALLY FROM 'Assets/Input/PlayerActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerActions"",
    ""maps"": [
        {
            ""name"": ""VR Player"",
            ""id"": ""bd6f589a-6699-4a0d-b5d3-78e0b5705ee7"",
            ""actions"": [
                {
                    ""name"": ""PositionHmd"",
                    ""type"": ""Value"",
                    ""id"": ""a9299f79-beb4-4124-a231-1c0b8bc80d60"",
                    ""expectedControlType"": ""Vector3"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RotationHmd"",
                    ""type"": ""Value"",
                    ""id"": ""b2de5660-705f-427d-abd3-1b4f48a6c09c"",
                    ""expectedControlType"": ""Quaternion"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DirectionHmd"",
                    ""type"": ""Value"",
                    ""id"": ""2c5aae11-c65c-4ab0-b039-6ac7d5cfa383"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PositionR"",
                    ""type"": ""Value"",
                    ""id"": ""68adbaf9-741c-4bcb-9991-c831bc406edc"",
                    ""expectedControlType"": ""Vector3"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RotationR"",
                    ""type"": ""Value"",
                    ""id"": ""38cff03f-f24a-46dd-9990-c21af02b0a50"",
                    ""expectedControlType"": ""Quaternion"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SelectR"",
                    ""type"": ""Button"",
                    ""id"": ""74b39705-7547-4230-ba25-0400d2b473cc"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveR"",
                    ""type"": ""Button"",
                    ""id"": ""c36ff34e-3a21-4b78-bdb2-23f195feceab"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""DirectionR"",
                    ""type"": ""Value"",
                    ""id"": ""bf71b78b-5159-47a0-9541-d32ccc65f670"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PositionL"",
                    ""type"": ""Value"",
                    ""id"": ""58d3c163-7b9c-4942-9091-b1487a18d8d7"",
                    ""expectedControlType"": ""Vector3"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RotationL"",
                    ""type"": ""Value"",
                    ""id"": ""7f26890c-e2b2-467b-ac99-d9603106b7ec"",
                    ""expectedControlType"": ""Quaternion"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SelectL"",
                    ""type"": ""Button"",
                    ""id"": ""d234f670-8043-447f-a8d8-80057e68bd1c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveL"",
                    ""type"": ""Button"",
                    ""id"": ""311a4208-fd80-4edc-933c-dc15184485e3"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""DirectionL"",
                    ""type"": ""Value"",
                    ""id"": ""323d93eb-552c-4aa3-9be0-a028eb45d556"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MenuR"",
                    ""type"": ""Button"",
                    ""id"": ""8f9779c5-921c-497a-91eb-9a3bdeb57309"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MenuL"",
                    ""type"": ""Button"",
                    ""id"": ""d6bbd05f-ebf0-47af-a36f-088905afedc5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""02af84b2-da56-4e97-ae91-1f5eb4a926a3"",
                    ""path"": ""<XRHMD>/centerEyePosition"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""PositionHmd"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""50fc2eb9-320d-4281-a74c-647410ab7cb7"",
                    ""path"": ""<XRHMD>/centerEyeRotation"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""RotationHmd"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e1e058b9-bdd0-41e7-b8c3-87c1dff0e6fe"",
                    ""path"": ""<XRController>{RightHand}/primary2DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""DirectionHmd"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dd80b64a-39ab-4eec-817b-d5f86a5bdb11"",
                    ""path"": ""<XRController>{RightHand}/devicePosition"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""PositionR"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""81039bb5-157b-4bb5-9434-0536bb5fa438"",
                    ""path"": ""<XRController>{RightHand}/deviceRotation"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""RotationR"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b9325948-8ef3-4ed5-80f4-c5d6411f0182"",
                    ""path"": ""<XRController>{RightHand}/triggerPressed"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""SelectR"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7cd984a1-c8bb-41e0-b284-0cba8a25a3f9"",
                    ""path"": ""<XRController>{RightHand}/trackpadClicked"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""MoveR"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b08ecf20-9ec3-41af-bbd9-ed09f97231fd"",
                    ""path"": ""<XRController>{LeftHand}/devicePosition"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""PositionL"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ed4ed904-4893-4ad8-88db-bd8741d66c04"",
                    ""path"": ""<XRController>{LeftHand}/deviceRotation"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""RotationL"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fe2c8062-62c2-4e1b-9fb3-f1bdc754c29a"",
                    ""path"": ""<XRController>{LeftHand}/triggerPressed"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""SelectL"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e3d17c04-8cd9-4a5d-b3ee-2e8dcd58bcf5"",
                    ""path"": ""<XRController>{LeftHand}/trackpadClicked"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""MoveL"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""85aaa8b1-e7ff-44f7-9526-90323c154951"",
                    ""path"": ""<XRController>{RightHand}/primary2DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""DirectionR"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d56edbd5-e037-4d69-944e-a7c891ad4fa5"",
                    ""path"": ""<XRController>{LeftHand}/primary2DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""DirectionL"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b2ea1299-33d3-4f65-bf78-c5589034d7da"",
                    ""path"": ""<XRController>{RightHand}/menu"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MenuR"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ecd4a7cf-9d11-4461-a5f7-f3d71d21037d"",
                    ""path"": ""<XRController>{LeftHand}/menu"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MenuL"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""FPS Player"",
            ""id"": ""578e1f7e-e99c-4bf6-bf7e-8407de6487a8"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Value"",
                    ""id"": ""5139efef-3fc4-425c-87ce-18af282dad52"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CamYaw"",
                    ""type"": ""Value"",
                    ""id"": ""7739df64-e304-4d19-8308-5b201bf829ee"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CamPitch"",
                    ""type"": ""Value"",
                    ""id"": ""60fd12a4-1371-4ab5-8e30-56e2be91daad"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Sprint"",
                    ""type"": ""Button"",
                    ""id"": ""dcb98b14-b43c-4d30-83f8-656b985bb97c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""a4c0abcc-4234-44d8-9355-5aafcaedaf7a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""c9f9801d-13f6-4e7f-b30f-21019d4c8211"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""259b77c2-4f23-4a47-a55d-50bab0292554"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""3e2c28a0-b6fe-4778-b20e-0b429927c359"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""a6ca0d50-ebf3-4751-bdc8-080666b8d183"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""7837ef68-7e81-4672-9406-bf476bd7598e"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""ca45a306-6c6c-453e-a7a8-3b6fb879e395"",
                    ""path"": ""<Mouse>/delta/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""CamYaw"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ca02c659-bea9-462e-88eb-2756a3cd47b0"",
                    ""path"": ""<Mouse>/delta/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""CamPitch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""99bb5c19-1573-4957-a9e3-5295bfdd80a2"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": ""Press(behavior=2)"",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Sprint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""40bf56b7-547e-4a43-a565-9daf4726bb2a"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""XR"",
            ""bindingGroup"": ""XR"",
            ""devices"": [
                {
                    ""devicePath"": ""<XRHMD>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<XRController>{RightHand}"",
                    ""isOptional"": true,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<XRController>{LeftHand}"",
                    ""isOptional"": true,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Keyboard&Mouse"",
            ""bindingGroup"": ""Keyboard&Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // VR Player
        m_VRPlayer = asset.FindActionMap("VR Player", throwIfNotFound: true);
        m_VRPlayer_PositionHmd = m_VRPlayer.FindAction("PositionHmd", throwIfNotFound: true);
        m_VRPlayer_RotationHmd = m_VRPlayer.FindAction("RotationHmd", throwIfNotFound: true);
        m_VRPlayer_DirectionHmd = m_VRPlayer.FindAction("DirectionHmd", throwIfNotFound: true);
        m_VRPlayer_PositionR = m_VRPlayer.FindAction("PositionR", throwIfNotFound: true);
        m_VRPlayer_RotationR = m_VRPlayer.FindAction("RotationR", throwIfNotFound: true);
        m_VRPlayer_SelectR = m_VRPlayer.FindAction("SelectR", throwIfNotFound: true);
        m_VRPlayer_MoveR = m_VRPlayer.FindAction("MoveR", throwIfNotFound: true);
        m_VRPlayer_DirectionR = m_VRPlayer.FindAction("DirectionR", throwIfNotFound: true);
        m_VRPlayer_PositionL = m_VRPlayer.FindAction("PositionL", throwIfNotFound: true);
        m_VRPlayer_RotationL = m_VRPlayer.FindAction("RotationL", throwIfNotFound: true);
        m_VRPlayer_SelectL = m_VRPlayer.FindAction("SelectL", throwIfNotFound: true);
        m_VRPlayer_MoveL = m_VRPlayer.FindAction("MoveL", throwIfNotFound: true);
        m_VRPlayer_DirectionL = m_VRPlayer.FindAction("DirectionL", throwIfNotFound: true);
        m_VRPlayer_MenuR = m_VRPlayer.FindAction("MenuR", throwIfNotFound: true);
        m_VRPlayer_MenuL = m_VRPlayer.FindAction("MenuL", throwIfNotFound: true);
        // FPS Player
        m_FPSPlayer = asset.FindActionMap("FPS Player", throwIfNotFound: true);
        m_FPSPlayer_Movement = m_FPSPlayer.FindAction("Movement", throwIfNotFound: true);
        m_FPSPlayer_CamYaw = m_FPSPlayer.FindAction("CamYaw", throwIfNotFound: true);
        m_FPSPlayer_CamPitch = m_FPSPlayer.FindAction("CamPitch", throwIfNotFound: true);
        m_FPSPlayer_Sprint = m_FPSPlayer.FindAction("Sprint", throwIfNotFound: true);
        m_FPSPlayer_Jump = m_FPSPlayer.FindAction("Jump", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // VR Player
    private readonly InputActionMap m_VRPlayer;
    private IVRPlayerActions m_VRPlayerActionsCallbackInterface;
    private readonly InputAction m_VRPlayer_PositionHmd;
    private readonly InputAction m_VRPlayer_RotationHmd;
    private readonly InputAction m_VRPlayer_DirectionHmd;
    private readonly InputAction m_VRPlayer_PositionR;
    private readonly InputAction m_VRPlayer_RotationR;
    private readonly InputAction m_VRPlayer_SelectR;
    private readonly InputAction m_VRPlayer_MoveR;
    private readonly InputAction m_VRPlayer_DirectionR;
    private readonly InputAction m_VRPlayer_PositionL;
    private readonly InputAction m_VRPlayer_RotationL;
    private readonly InputAction m_VRPlayer_SelectL;
    private readonly InputAction m_VRPlayer_MoveL;
    private readonly InputAction m_VRPlayer_DirectionL;
    private readonly InputAction m_VRPlayer_MenuR;
    private readonly InputAction m_VRPlayer_MenuL;
    public struct VRPlayerActions
    {
        private @PlayerActions m_Wrapper;
        public VRPlayerActions(@PlayerActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @PositionHmd => m_Wrapper.m_VRPlayer_PositionHmd;
        public InputAction @RotationHmd => m_Wrapper.m_VRPlayer_RotationHmd;
        public InputAction @DirectionHmd => m_Wrapper.m_VRPlayer_DirectionHmd;
        public InputAction @PositionR => m_Wrapper.m_VRPlayer_PositionR;
        public InputAction @RotationR => m_Wrapper.m_VRPlayer_RotationR;
        public InputAction @SelectR => m_Wrapper.m_VRPlayer_SelectR;
        public InputAction @MoveR => m_Wrapper.m_VRPlayer_MoveR;
        public InputAction @DirectionR => m_Wrapper.m_VRPlayer_DirectionR;
        public InputAction @PositionL => m_Wrapper.m_VRPlayer_PositionL;
        public InputAction @RotationL => m_Wrapper.m_VRPlayer_RotationL;
        public InputAction @SelectL => m_Wrapper.m_VRPlayer_SelectL;
        public InputAction @MoveL => m_Wrapper.m_VRPlayer_MoveL;
        public InputAction @DirectionL => m_Wrapper.m_VRPlayer_DirectionL;
        public InputAction @MenuR => m_Wrapper.m_VRPlayer_MenuR;
        public InputAction @MenuL => m_Wrapper.m_VRPlayer_MenuL;
        public InputActionMap Get() { return m_Wrapper.m_VRPlayer; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(VRPlayerActions set) { return set.Get(); }
        public void SetCallbacks(IVRPlayerActions instance)
        {
            if (m_Wrapper.m_VRPlayerActionsCallbackInterface != null)
            {
                @PositionHmd.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnPositionHmd;
                @PositionHmd.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnPositionHmd;
                @PositionHmd.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnPositionHmd;
                @RotationHmd.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnRotationHmd;
                @RotationHmd.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnRotationHmd;
                @RotationHmd.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnRotationHmd;
                @DirectionHmd.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnDirectionHmd;
                @DirectionHmd.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnDirectionHmd;
                @DirectionHmd.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnDirectionHmd;
                @PositionR.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnPositionR;
                @PositionR.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnPositionR;
                @PositionR.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnPositionR;
                @RotationR.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnRotationR;
                @RotationR.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnRotationR;
                @RotationR.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnRotationR;
                @SelectR.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnSelectR;
                @SelectR.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnSelectR;
                @SelectR.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnSelectR;
                @MoveR.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnMoveR;
                @MoveR.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnMoveR;
                @MoveR.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnMoveR;
                @DirectionR.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnDirectionR;
                @DirectionR.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnDirectionR;
                @DirectionR.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnDirectionR;
                @PositionL.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnPositionL;
                @PositionL.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnPositionL;
                @PositionL.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnPositionL;
                @RotationL.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnRotationL;
                @RotationL.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnRotationL;
                @RotationL.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnRotationL;
                @SelectL.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnSelectL;
                @SelectL.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnSelectL;
                @SelectL.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnSelectL;
                @MoveL.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnMoveL;
                @MoveL.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnMoveL;
                @MoveL.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnMoveL;
                @DirectionL.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnDirectionL;
                @DirectionL.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnDirectionL;
                @DirectionL.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnDirectionL;
                @MenuR.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnMenuR;
                @MenuR.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnMenuR;
                @MenuR.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnMenuR;
                @MenuL.started -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnMenuL;
                @MenuL.performed -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnMenuL;
                @MenuL.canceled -= m_Wrapper.m_VRPlayerActionsCallbackInterface.OnMenuL;
            }
            m_Wrapper.m_VRPlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @PositionHmd.started += instance.OnPositionHmd;
                @PositionHmd.performed += instance.OnPositionHmd;
                @PositionHmd.canceled += instance.OnPositionHmd;
                @RotationHmd.started += instance.OnRotationHmd;
                @RotationHmd.performed += instance.OnRotationHmd;
                @RotationHmd.canceled += instance.OnRotationHmd;
                @DirectionHmd.started += instance.OnDirectionHmd;
                @DirectionHmd.performed += instance.OnDirectionHmd;
                @DirectionHmd.canceled += instance.OnDirectionHmd;
                @PositionR.started += instance.OnPositionR;
                @PositionR.performed += instance.OnPositionR;
                @PositionR.canceled += instance.OnPositionR;
                @RotationR.started += instance.OnRotationR;
                @RotationR.performed += instance.OnRotationR;
                @RotationR.canceled += instance.OnRotationR;
                @SelectR.started += instance.OnSelectR;
                @SelectR.performed += instance.OnSelectR;
                @SelectR.canceled += instance.OnSelectR;
                @MoveR.started += instance.OnMoveR;
                @MoveR.performed += instance.OnMoveR;
                @MoveR.canceled += instance.OnMoveR;
                @DirectionR.started += instance.OnDirectionR;
                @DirectionR.performed += instance.OnDirectionR;
                @DirectionR.canceled += instance.OnDirectionR;
                @PositionL.started += instance.OnPositionL;
                @PositionL.performed += instance.OnPositionL;
                @PositionL.canceled += instance.OnPositionL;
                @RotationL.started += instance.OnRotationL;
                @RotationL.performed += instance.OnRotationL;
                @RotationL.canceled += instance.OnRotationL;
                @SelectL.started += instance.OnSelectL;
                @SelectL.performed += instance.OnSelectL;
                @SelectL.canceled += instance.OnSelectL;
                @MoveL.started += instance.OnMoveL;
                @MoveL.performed += instance.OnMoveL;
                @MoveL.canceled += instance.OnMoveL;
                @DirectionL.started += instance.OnDirectionL;
                @DirectionL.performed += instance.OnDirectionL;
                @DirectionL.canceled += instance.OnDirectionL;
                @MenuR.started += instance.OnMenuR;
                @MenuR.performed += instance.OnMenuR;
                @MenuR.canceled += instance.OnMenuR;
                @MenuL.started += instance.OnMenuL;
                @MenuL.performed += instance.OnMenuL;
                @MenuL.canceled += instance.OnMenuL;
            }
        }
    }
    public VRPlayerActions @VRPlayer => new VRPlayerActions(this);

    // FPS Player
    private readonly InputActionMap m_FPSPlayer;
    private IFPSPlayerActions m_FPSPlayerActionsCallbackInterface;
    private readonly InputAction m_FPSPlayer_Movement;
    private readonly InputAction m_FPSPlayer_CamYaw;
    private readonly InputAction m_FPSPlayer_CamPitch;
    private readonly InputAction m_FPSPlayer_Sprint;
    private readonly InputAction m_FPSPlayer_Jump;
    public struct FPSPlayerActions
    {
        private @PlayerActions m_Wrapper;
        public FPSPlayerActions(@PlayerActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_FPSPlayer_Movement;
        public InputAction @CamYaw => m_Wrapper.m_FPSPlayer_CamYaw;
        public InputAction @CamPitch => m_Wrapper.m_FPSPlayer_CamPitch;
        public InputAction @Sprint => m_Wrapper.m_FPSPlayer_Sprint;
        public InputAction @Jump => m_Wrapper.m_FPSPlayer_Jump;
        public InputActionMap Get() { return m_Wrapper.m_FPSPlayer; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(FPSPlayerActions set) { return set.Get(); }
        public void SetCallbacks(IFPSPlayerActions instance)
        {
            if (m_Wrapper.m_FPSPlayerActionsCallbackInterface != null)
            {
                @Movement.started -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnMovement;
                @CamYaw.started -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnCamYaw;
                @CamYaw.performed -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnCamYaw;
                @CamYaw.canceled -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnCamYaw;
                @CamPitch.started -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnCamPitch;
                @CamPitch.performed -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnCamPitch;
                @CamPitch.canceled -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnCamPitch;
                @Sprint.started -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnSprint;
                @Sprint.performed -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnSprint;
                @Sprint.canceled -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnSprint;
                @Jump.started -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_FPSPlayerActionsCallbackInterface.OnJump;
            }
            m_Wrapper.m_FPSPlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @CamYaw.started += instance.OnCamYaw;
                @CamYaw.performed += instance.OnCamYaw;
                @CamYaw.canceled += instance.OnCamYaw;
                @CamPitch.started += instance.OnCamPitch;
                @CamPitch.performed += instance.OnCamPitch;
                @CamPitch.canceled += instance.OnCamPitch;
                @Sprint.started += instance.OnSprint;
                @Sprint.performed += instance.OnSprint;
                @Sprint.canceled += instance.OnSprint;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
            }
        }
    }
    public FPSPlayerActions @FPSPlayer => new FPSPlayerActions(this);
    private int m_XRSchemeIndex = -1;
    public InputControlScheme XRScheme
    {
        get
        {
            if (m_XRSchemeIndex == -1) m_XRSchemeIndex = asset.FindControlSchemeIndex("XR");
            return asset.controlSchemes[m_XRSchemeIndex];
        }
    }
    private int m_KeyboardMouseSchemeIndex = -1;
    public InputControlScheme KeyboardMouseScheme
    {
        get
        {
            if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard&Mouse");
            return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
        }
    }
    public interface IVRPlayerActions
    {
        void OnPositionHmd(InputAction.CallbackContext context);
        void OnRotationHmd(InputAction.CallbackContext context);
        void OnDirectionHmd(InputAction.CallbackContext context);
        void OnPositionR(InputAction.CallbackContext context);
        void OnRotationR(InputAction.CallbackContext context);
        void OnSelectR(InputAction.CallbackContext context);
        void OnMoveR(InputAction.CallbackContext context);
        void OnDirectionR(InputAction.CallbackContext context);
        void OnPositionL(InputAction.CallbackContext context);
        void OnRotationL(InputAction.CallbackContext context);
        void OnSelectL(InputAction.CallbackContext context);
        void OnMoveL(InputAction.CallbackContext context);
        void OnDirectionL(InputAction.CallbackContext context);
        void OnMenuR(InputAction.CallbackContext context);
        void OnMenuL(InputAction.CallbackContext context);
    }
    public interface IFPSPlayerActions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnCamYaw(InputAction.CallbackContext context);
        void OnCamPitch(InputAction.CallbackContext context);
        void OnSprint(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
    }
}
