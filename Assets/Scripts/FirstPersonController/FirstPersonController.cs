using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FirstPersonController : MonoBehaviour
{
    [SerializeField] Transform cam;
    [SerializeField] Transform groundConnector;
    Transform _transform;

    [SerializeField] float stepHeight = 0.2f;
    [SerializeField] float camRotationSpeed = 5f;
    [SerializeField] float cameraMinimumY = -60f;
    [SerializeField] float cameraMaximumY = 75f;
    [SerializeField] float rotationSmoothSpeed = 10f;
    [SerializeField] LayerMask rayCastMask;

    [SerializeField] float walkSpeed = 9f;
    [SerializeField] float runSpeed = 14f;


    float bodyRotationY = 0;
    float camRotation = 0;
    float speed;

    Vector2 moveVal = Vector2.zero;
    float camX = 0;
    float camY = 0;
    bool sprint = false;

    public bool grounded { get; private set; }
    Vector3 groundUp;
    Quaternion bodyTargetRotation;

    private void Start()
    {
        _transform = transform;
        groundUp = transform.up;
        bodyTargetRotation = transform.rotation;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        PositionOnGround();
    }

    private void PositionOnGround()
    {
        if (Physics.Raycast(_transform.position, -_transform.up, out RaycastHit hit, 100f, rayCastMask))
        {
            groundUp = hit.normal;
            _transform.position = hit.point;
        }
    }

    void OnMovement(InputValue value)
    {
        moveVal = value.Get<Vector2>();
    }

    void OnCamYaw(InputValue value)
    {
        camX = value.Get<float>();
    }

    void OnCamPitch(InputValue value)
    {
        camY = value.Get<float>();
    }

    void OnSprint(InputValue value)
    {
        sprint = value.isPressed;
    }

    private void Update()
    {
        GetGroundNormal();
        RotateBody();
        RotateCamera();
        MoveCharacter();
    }

    void RotateBody()
    {
        //_transform.RotateAround(groundConnector.position, Vector3.Cross(_transform.up, groundUp), Vector3.Angle(_transform.up, groundUp) * Time.deltaTime * rotationSmoothSpeed);
        //_transform.rotation = Quaternion.Slerp(_transform.rotation, Quaternion.FromToRotation(_transform.up, groundUp) * _transform.rotation, rotationSmoothSpeed * Time.deltaTime);
        _transform.rotation = Quaternion.RotateTowards(_transform.rotation, Quaternion.FromToRotation(_transform.up, groundUp) * _transform.rotation, rotationSmoothSpeed * Time.deltaTime);
    }

    void RotateCamera()
    {
        // Get camera and body rotational values
        bodyRotationY = camX * camRotationSpeed * Time.deltaTime;
        camRotation += camY * camRotationSpeed * Time.deltaTime;

        // Stop our camera from rotating 360�
        camRotation = Mathf.Clamp(camRotation, cameraMinimumY, cameraMaximumY);

        // Create Rotation Targets and handle rotations of the body and camera
        Quaternion camTargetRotation = Quaternion.Euler(-camRotation, 0, 0);

        // handle rotations
        _transform.rotation = Quaternion.AngleAxis(bodyRotationY, _transform.up) * _transform.rotation;
        cam.localRotation = camTargetRotation;
    }

    void MoveCharacter()
    {
        // Control our speed based on our movement state
        speed = sprint ? runSpeed : walkSpeed;

        Vector3 nextPosition = _transform.position + (_transform.forward * moveVal.y + _transform.right * moveVal.x) * speed * Time.deltaTime;
        if (Physics.Raycast(nextPosition + _transform.up * stepHeight, -_transform.up, out RaycastHit hit, stepHeight + 0.1f))
        {
            _transform.position += hit.point - groundConnector.position;
        }
    }

    void GetGroundNormal()
    {
        if (Physics.Raycast(groundConnector.position + groundUp * stepHeight, -groundUp, out RaycastHit hit, stepHeight + 0.1f, rayCastMask))
        {
            groundUp = hit.normal;
        }
    }
}
