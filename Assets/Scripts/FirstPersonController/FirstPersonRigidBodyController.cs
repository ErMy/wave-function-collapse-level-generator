﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

#pragma warning disable 0649

public class FirstPersonRigidBodyController : MonoBehaviour
{
    [SerializeField] Transform cam;
    [SerializeField] Rigidbody rb;

    [SerializeField] float camRotationSpeed = 5f;
    [SerializeField] float cameraMinimumY = -60f;
    [SerializeField] float cameraMaximumY = 75f;
    [SerializeField] float rotationSmoothSpeed = 10f;

    [SerializeField] float walkSpeed = 9f;
    [SerializeField] float runSpeed = 14f;
    [SerializeField] float maxSpeed = 20f;
    [SerializeField] float jumpPower = 30f;

    [SerializeField] float extraGravity = 45f;

    float bodyRotationY = 0;
    float camRotation = 0;
    float speed;
    bool jump = false;

    Vector2 moveVal = Vector2.zero;
    float camX = 0;
    float camY = 0;
    bool sprint = false;

    public bool grounded { get; private set; }
    Vector3 groundUp;
    Quaternion bodyTargetRotation;

    private void Start()
    {
        groundUp = transform.up;
        bodyTargetRotation = transform.rotation;
    }

    void OnMovement(InputValue value)
    {
        moveVal = value.Get<Vector2>();
    }

    void OnCamYaw(InputValue value)
    {
        camX = value.Get<float>();
    }

    void OnCamPitch(InputValue value)
    {
        camY = value.Get<float>();
    }

    void OnSprint(InputValue value)
    {
        sprint = value.isPressed;
    }

    void OnJump()
    {
        jump = true;
    }

    private void Update()
    {
        GroundCheck();
        LookRotation();
        Movement();
        ExtraGravity();
        if (jump)
        {
            jump = false;
            if (grounded)
            {
                Jump();
            }
        }
    }

    void LookRotation()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        // Get camera and body rotational values
        //bodyRotationY = Input.GetAxis("Mouse X") * camRotationSpeed;
        //camRotation += Input.GetAxis("Mouse Y") * camRotationSpeed;

        bodyRotationY = camX * camRotationSpeed;
        camRotation += camY * camRotationSpeed;

        // Stop our camera from rotating 360°
        camRotation = Mathf.Clamp(camRotation, cameraMinimumY, cameraMaximumY);

        // Create Rotation Targets and handle rotations of the body and camera
        Quaternion camTargetRotation = Quaternion.Euler(-camRotation, 0, 0);

        bodyTargetRotation = Quaternion.FromToRotation(transform.up, groundUp) * bodyTargetRotation;
        bodyTargetRotation = Quaternion.AngleAxis(bodyRotationY, groundUp) * bodyTargetRotation;

        // handle rotations
        transform.rotation = Quaternion.Lerp(transform.rotation, bodyTargetRotation, Time.deltaTime * rotationSmoothSpeed);
        cam.localRotation = Quaternion.Lerp(cam.localRotation, camTargetRotation, Time.deltaTime * rotationSmoothSpeed);
    }

    void Movement()
    {
        // Control our speed based on our movement state
        //speed = Input.GetKey(KeyCode.LeftShift) ? runSpeed : walkSpeed;
        speed = sprint ? runSpeed : walkSpeed;

        Vector3 fallVelocity = Vector3.Project(rb.velocity, -groundUp);

        // Change our character's velocity in this direction
        //rb.velocity = transform.forward * Input.GetAxis("Vertical") * speed + transform.right * Input.GetAxis("Horizontal") * speed + fallVelocity;
        rb.velocity = transform.forward * moveVal.y * speed + transform.right * moveVal.x * speed + fallVelocity;
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
    }

    void ExtraGravity()
    {
        rb.AddForce(-groundUp * extraGravity);
    }

    void GroundCheck()
    {
        RaycastHit hit;
        //grounded = Physics.SphereCast(transform.position, 0.2f, -transform.up, out hit, 1.1f);
        grounded = Physics.Raycast(transform.position, -transform.up, out hit, 1.1f);

        if (grounded)
        {
            groundUp = hit.normal;
        }
    }

    void Jump()
    {
        rb.AddForce(transform.up * jumpPower, ForceMode.Impulse);
    }
}
