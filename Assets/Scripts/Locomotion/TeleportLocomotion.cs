using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class TeleportLocomotion : BaseLocomotion
{
    [SerializeField] private GameObject teleportInteractor;
    [SerializeField] private GameObject teleportReticle;
    [SerializeField] private Transform cam;
    private XRInteractorLineVisual lineVisual;
    private XRRayInteractor rayInteractor;

    private void Awake()
    {
        lineVisual = teleportInteractor.GetComponent<XRInteractorLineVisual>();
        rayInteractor = teleportInteractor.GetComponent<XRRayInteractor>();
        cam = GetComponentInChildren<Camera>(true).transform;
    }

    private void OnEnable()
    {
        // enable teleportation
        teleportInteractor.SetActive(true);
        lineVisual.enabled = false;
    }

    private void OnDisable()
    {
        // disable teleportation
        teleportInteractor.SetActive(false);
    }

    public override void OnMoveEnter()
    {
        // display the line renderer
        lineVisual.enabled = true;
    }

    public override void OnMoveExit()
    {
        // teleport to position of reticle and adjust rotation
        if (teleportReticle.activeInHierarchy && rayInteractor.GetCurrentRaycastHit(out RaycastHit hit) && teleportReticle.transform.position == hit.point)
        {
            transform.rotation = Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation;
            transform.position += hit.point - transform.TransformPoint(cam.localPosition.x, 0, cam.localPosition.z);

            teleportReticle.SetActive(false);
        }

        // hide line renderer
        lineVisual.enabled = false;
    }

    public override void OnMove(Vector2 direction)
    {

    }

    public GameObject GetTeleportReticle()
    {
        return teleportReticle;
    }
}
