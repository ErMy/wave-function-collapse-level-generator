using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class LocomotionController : MonoBehaviour
{
    public BaseLocomotion[] locomotionOptions;
    private BaseLocomotion locomotion;
    private int currentLocomotionIndex = 0;

    private void Start()
    {
        if (locomotionOptions != null && locomotionOptions.Length > 0)
        {
            locomotion = locomotionOptions[0];
            for (int i = 1; i < locomotionOptions.Length; i++)
            {
                locomotionOptions[i].enabled = false;
            }
        }
        else
        {
            enabled = false;
        }
    }

    void OnMoveR(InputValue value)
    {
        if (value.isPressed)
        {
            locomotion.OnMoveEnter();
        }
        else
        {
            locomotion.OnMoveExit();
        }
    }

    void OnDirectionR(InputValue value)
    {
        locomotion.OnMove(value.Get<Vector2>());
    }

    void OnMenuR()
    {
        locomotionOptions[currentLocomotionIndex].enabled = false;
        currentLocomotionIndex = (currentLocomotionIndex + 1) % locomotionOptions.Length;
        locomotion = locomotionOptions[currentLocomotionIndex];
        locomotionOptions[currentLocomotionIndex].enabled = true;
    }
}
