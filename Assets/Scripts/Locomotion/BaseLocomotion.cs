using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseLocomotion : MonoBehaviour
{
    public virtual void OnMoveEnter()
    {

    }
    public virtual void OnMove(Vector2 direction)
    {

    }
    public virtual void OnMoveExit()
    {

    }
}
