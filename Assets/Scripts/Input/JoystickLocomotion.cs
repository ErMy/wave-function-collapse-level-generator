using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

#pragma warning disable 0649

public class JoystickLocomotion : BaseLocomotion
{
    public Transform controller;
    public float reach = 0.1f;
    public float speed = 1f;
    public float headRadius = 0.2f;
    public float rotationSmoothSpeed = 30f;
    public LayerMask rayCastMask;

    private Transform cam;
    private Vector3 groundUp;

    bool move = false;
    Vector2 axis;

    private void Awake()
    {
        cam = GetComponentInChildren<Camera>(true).transform;
        groundUp = transform.up;
    }

    private void Start()
    {
        PositionPlayerOnGround();
    }

    private void PositionPlayerOnGround()
    {
        if (Physics.Raycast(transform.position + transform.up * 0.05f, -transform.up, out RaycastHit hit, 100f, rayCastMask))
        {
            groundUp = hit.normal;
            transform.position = hit.point;
        }
    }

    public override void OnMoveEnter()
    {
        move = true;
    }

    public override void OnMoveExit()
    {
        move = false;
    }

    public override void OnMove(Vector2 direction)
    {
        axis = direction;
    }

    private void Update()
    {
        GetGroundNormal();
        RotatePlayer();
        if (move)
        {
            MovePlayer();
        }
    }

    private void GetGroundNormal()
    {
        if (Physics.Raycast(cam.position, -groundUp, out RaycastHit hit, 2.5f, rayCastMask))
        {
            groundUp = hit.normal;
        }
    }

    private void MovePlayer()
    {
        Vector3 direction = Quaternion.AngleAxis(-Vector2.SignedAngle(Vector2.up, axis), transform.up) * Vector3.ProjectOnPlane(controller.forward, transform.up);
        direction.Normalize();

        Vector3 nextPos = cam.position + direction * reach;

        if (Physics.Raycast(nextPos, -groundUp, out RaycastHit hit, 2.5f))
        {
            if (!Physics.CheckCapsule(nextPos, hit.point + groundUp * headRadius * 1.1f, headRadius, rayCastMask))
            {
                transform.position = Vector3.Lerp(transform.position, transform.position + ((hit.point + hit.normal * cam.localPosition.y) - cam.position), speed * Time.deltaTime);
            }
        }
    }

    private void RotatePlayer()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.FromToRotation(transform.up, groundUp) * transform.rotation, rotationSmoothSpeed * Time.deltaTime);
    }


}