using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaveFunctionCollapse
{
    public static class TransformHelper
    {
        /// <summary>
        /// Transform a position using rotator/reflector. Used to find correct tile positions after rotating/scaling them.
        /// </summary>
        public static Vector3Int TransformPosition(Vector3Int position, ERotator rotator, EReflector reflector)
        {
            Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, rotator.ToRotation(), reflector.ToVector());
            Vector3 result = m.MultiplyPoint3x4(position);

            return new Vector3Int(Mathf.RoundToInt(result.x), Mathf.RoundToInt(result.y), Mathf.RoundToInt(result.z));
        }

        public static Vector3 TransformOffset(Vector3 position, ERotator rotator, EReflector reflector)
        {
            Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, rotator.ToRotation(), reflector.ToVector());
            return m.MultiplyPoint3x4(position);
        }

        public static Vector3 SignOnly(this Vector3 vector)
        {
            return new Vector3(vector.x < 0 ? -1 : 1, vector.y < 0 ? -1 : 1, vector.z < 0 ? -1 : 1);
        }
    }
}
