using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using Priority_Queue;

namespace WaveFunctionCollapse
{
    public class CellBuilder
    {
        public static void BuildCells(Map map, Vector3Int gridSize)
        {
            // Calculate cache values for entropy calculation
            // for now every tile has the same candidates => same entropy (+noise)
            float weightSum = 0;
            float weightLogWeight = 0;
            for (int i = 0; i < map.mapTiles.Length; i++)
            {
                weightSum += map.mapSegments[map.mapTiles[i].segmentIndex].weight;
                weightLogWeight += map.mapSegments[map.mapTiles[i].segmentIndex].weight * Mathf.Log(map.mapSegments[map.mapTiles[i].segmentIndex].weight, 2f);
            }

            // Create initial candidates to copy from
            HashSet<int> initialCandidates = new HashSet<int>();
            for (int i = 0; i < map.mapTiles.Length; i++)
            {
                initialCandidates.Add(i);
            }


            // Create tile enabler counts to copy from
            int[][] tileEnablerCounts = new int[map.mapTiles.Length][];

            // for every tile
            for (int tileIndex = 0; tileIndex < map.mapTiles.Length; tileIndex++)
            {
                tileEnablerCounts[tileIndex] = new int[6];

                // for every direction
                for (int dir = 0; dir < 6; dir++)
                {
                    // enabler count in direction equals count of compatible tiles that exist in direction
                    tileEnablerCounts[tileIndex][dir] = map.adjacencyRules.GetCompatibleTiles(tileIndex, dir).Length;
                }
            }

            var rand = new System.Random();

            // Initialize cell array
            map.cellGrid = new Cell[gridSize.x][][];

            for (int x = 0; x < gridSize.x; x++)
            {
                map.cellGrid[x] = new Cell[gridSize.y][];
                for (int y = 0; y < gridSize.y; y++)
                {
                    map.cellGrid[x][y] = new Cell[gridSize.z];
                    for (int z = 0; z < gridSize.z; z++)
                    {
                        // Create cells and calculate their entropy
                        map.cellGrid[x][y][z] = new Cell(initialCandidates, tileEnablerCounts, weightSum, weightLogWeight);
                    }
                }
            }
        }
    }
}