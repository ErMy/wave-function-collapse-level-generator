using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaveFunctionCollapse
{
    public class AdjacencyRules
    {
        #region Variables
        // tileIndex, direction -> array of compatible tiles
        int[][][] compatibleTiles;
        int[][] borderIncompatibleTiles;
        #endregion

        #region Create rules
        public void CreateRules(Map map, TileColorSettings settings, bool useBorderRule, MapTile borderRuleTile)
        {
            // Initialize array and list
            List<int>[][] newCompatibles = new List<int>[map.mapTiles.Length][];
            compatibleTiles = new int[map.mapTiles.Length][][];

            for (int i = 0; i < map.mapTiles.Length; i++)
            {
                newCompatibles[i] = new List<int>[6];
                compatibleTiles[i] = new int[6][];
                for (int dir = 0; dir < 6; dir++)
                {
                    newCompatibles[i][dir] = new List<int>();
                }
            }

            // for every direction
            for (int dir = 0; dir < 6; dir++)
            {
                int opposite = Direction.Opposite(dir);

                // for every tile
                for (int tile1 = 0; tile1 < map.mapTiles.Length; tile1++)
                {
                    // If tile has no face in direction (meaning its segment contains a neighboring tile in direction)
                    if (map.mapTiles[tile1].faceIDs[dir][0] == -1)
                    {
                        // add its neighbor to compatibles
                        MapSegment segment = map.mapSegments[map.mapTiles[tile1].segmentIndex];
                        int neighbor = segment.posToTile[map.mapTiles[tile1].position + Direction.ToVector(dir)];
                        newCompatibles[tile1][dir].Add(neighbor);
                    } else
                    {
                        // If it has a face, test compatibility with all other tiles
                        for (int tile2 = tile1; tile2 < map.mapTiles.Length; tile2++)
                        {
                            // skip tile if it's an inner tile with no face in this direction
                            if (map.mapTiles[tile2].faceIDs[opposite][0] == -1) continue;

                            // skip if tile is marked as invalid neighbor
                            MapSegment segment1 = map.mapSegments[map.mapTiles[tile1].segmentIndex];
                            MapSegment segment2 = map.mapSegments[map.mapTiles[tile2].segmentIndex];
                            if (segment1.invalidNeighbors.Contains(segment2.spawnableObject)) continue;

                            // if tile1 and tile2 are compatible in direction
                            if (AreTilesCompatible(settings, map.mapTiles[tile1].faceIDs[dir], map.mapTiles[tile2].faceIDs[opposite]))
                            {
                                newCompatibles[tile1][dir].Add(tile2);
                                newCompatibles[tile2][opposite].Add(tile1);
                            }
                        }
                    }
                }
            }

            // copy all compatibles to array
            for (int dir = 0; dir < 6; dir++)
            {
                for (int tileIndex = 0; tileIndex < map.mapTiles.Length; tileIndex++)
                {
                    compatibleTiles[tileIndex][dir] = newCompatibles[tileIndex][dir].ToArray();
                }
            }


            // Create rule for border
            if (useBorderRule)
            {
                borderIncompatibleTiles = new int[6][];
                // For every direction
                for (int dir = 0; dir < 6; dir++)
                {
                    List<int> incompatibles = new List<int>();
                    // for all tiles
                    for (int tileIndex = 0; tileIndex < map.mapTiles.Length; tileIndex++)
                    {
                        // check if they're compatible with border
                        if (!AreTilesCompatible(settings, map.mapTiles[tileIndex].faceIDs[dir], borderRuleTile.faceIDs[Direction.Opposite(dir)]))
                        {
                            // if not, add to list of incompatibles
                            incompatibles.Add(tileIndex);
                        }
                    }

                    // convert the list to array
                    borderIncompatibleTiles[dir] = incompatibles.ToArray();
                }
            }
        }

        private bool AreTilesCompatible(TileColorSettings settings, int[] faceids1, int[] faceids2)
        {
            for (int subfaceIndex = 0; subfaceIndex < 4; subfaceIndex++)
            {
                // compare each subface of the two tiles
                if (!settings.ColorsAreCompatible(faceids1[subfaceIndex], faceids2[subfaceIndex]))
                {
                    return false;
                }
            }

            return true;
        }


        #endregion

        public int[] GetCompatibleTiles(int tileIndex, int direction)
        {
            return compatibleTiles[tileIndex][direction];
        }

        /// <summary>
        /// Get all tiles that should not be used at the border
        /// </summary>
        /// <param name="direction">E.g. left returns all tiles that should not be at x=0</param>
        public int[] GetIncompatibleBorderTiles(int direction)
        {
            return borderIncompatibleTiles[direction];
        }
    }
}