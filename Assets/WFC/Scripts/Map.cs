using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Concurrent;
using System;
using Priority_Queue;
using System.Linq;
using System.Threading.Tasks;

namespace WaveFunctionCollapse
{
    #region Structs
    public struct RemovalUpdate
    {
        public Vector3Int cellPosition;
        public int tileIndex;
    }

    public struct Sibling
    {
        public Cell cell;
        public Vector3Int cellPosition;
        public int tileIndex;
    }
    #endregion

    public class Map
    {
        #region Variables
        public Cell[][][] cellGrid;
        public MapTile[] mapTiles;
        public MapSegment[] mapSegments;
        public AdjacencyRules adjacencyRules;
        long remainingCellCount;
        public SimplePriorityQueue<Vector3Int> cellQueue;
        public Queue<RemovalUpdate> removalUpdates;

        long remainingCellCountCopy;
        Cell[][][] cellGridCopy;
        #endregion

        #region Constructor
        public Map()
        {

        }
        #endregion

        public void Initialize(Vector3Int gridSize)
        {
            remainingCellCount = gridSize.x * gridSize.y * gridSize.z;
            removalUpdates = new Queue<RemovalUpdate>();
        }

        #region Map Creation

        public bool ApplyBorderFilter(bool useBorderFilter)
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            if (useBorderFilter)
            {
                // remove all incompatible tiles from the border
                Propagator.FilterBorderCells(this);
                stopwatch.Restart();

                // propagate changes
                Propagator.Propagate(this, false);
                Debug.Log($"Apply Border Filter: {stopwatch.ElapsedMilliseconds / 1000f}");
            }
            else
            {
                Propagator.FilterMisfits(this);
                Propagator.Propagate(this, false);
            }

            bool validMapData = SaveMapData();
            return validMapData;
        }
        public void ApplyPathFilter(bool createPaths, bool filterPathOnlyTiles, IEnumerable<PathPoint> pathPoints = null)
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            if (createPaths && pathPoints != null)
            {
                // apply path filter
                Propagator.FilterPaths(this, pathPoints, filterPathOnlyTiles);
                Propagator.Propagate(this, false);
                Debug.Log($"Apply Path Filter: {stopwatch.ElapsedMilliseconds / 1000f}");
            }
        }

        private bool SaveMapData()
        {
            remainingCellCountCopy = remainingCellCount;
            cellGridCopy = new Cell[cellGrid.Length][][];
            for (int x = 0; x < cellGrid.Length; x++)
            {
                cellGridCopy[x] = new Cell[cellGrid[x].Length][];
                for (int y = 0; y < cellGrid[0].Length; y++)
                {
                    cellGridCopy[x][y] = new Cell[cellGrid[x][y].Length];
                    for (int z = 0; z < cellGrid[0][0].Length; z++)
                    {
                        cellGridCopy[x][y][z] = new Cell(cellGrid[x][y][z]);
                        if (cellGrid[x][y][z].candidates.Count == 0)
                        {
                            Debug.LogWarning("A cell has no candidate after filtering");
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        public bool Create()
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            FillCellQueue();

            // collapse every cell until all are collapsed or there's an error
            while (remainingCellCount > 0)
            {
                // Get next cell with lowest entropy that hasn't been collapsed yet
                if (TryGetNextCell(out Vector3Int cellPos, out Cell cell))
                {
                    if (cell.candidates.Count == 0)
                    {
                        Debug.Log("No candidate for cell");
                        return false;
                    }

                    // Use weighted random choice to choose one of the tile's candidates
                    int tileIndex = ChooseRandomTile(cell);

                    var siblings = GetSiblings(tileIndex, cellPos);

                    for (int i = 0; i < siblings.Count; i++)
                    {
                        CollapseCell(siblings[i].cell, siblings[i].cellPosition, siblings[i].tileIndex);
                    }

                    // Propagate changes
                    if (!Propagator.Propagate(this))
                    {
                        return false;
                    }

                }
            }

            return true;
        }

        private void FillCellQueue()
        {
            cellQueue = new SimplePriorityQueue<Vector3Int>();

            for (int x = 0; x < cellGrid.Length; x++)
            {
                for (int y = 0; y < cellGrid[0].Length; y++)
                {
                    for (int z = 0; z < cellGrid[0][0].Length; z++)
                    {
                        Cell cell = cellGrid[x][y][z];
                        float entropy = cell.AddNoiseToEntropy(UnityEngine.Random.value * 0.00001f);
                        cellQueue.Enqueue(new Vector3Int(x, y, z), entropy);
                    }
                }
            }
        }

        public void LoadMapData()
        {
            remainingCellCount = remainingCellCountCopy;
            removalUpdates.Clear();

            for (int x = 0; x < cellGrid.Length; x++)
            {
                for (int y = 0; y < cellGrid[0].Length; y++)
                {
                    for (int z = 0; z < cellGrid[0][0].Length; z++)
                    {
                        cellGrid[x][y][z] = new Cell(cellGridCopy[x][y][z]);
                    }
                }
            }
        }

        /// <summary>
        /// Get next cell with lowest entropy.
        /// </summary>
        /// <returns>True if next cell not collapsed yet.</returns>
        private bool TryGetNextCell(out Vector3Int cellPos, out Cell cell)
        {
            cellPos = cellQueue.Dequeue();
            cell = cellGrid[cellPos.x][cellPos.y][cellPos.z];
            return !cell.isCollapsed;
        }

        /// <summary>
        /// Choose a weighted random tile from the cell's candidates
        /// </summary>
        /// <returns>Index of chosen candidate tile.</returns>
        private int ChooseRandomTile(in Cell cell)
        {
            float randomVal = cell.GetTotalWeight() * UnityEngine.Random.value;

            foreach (int candidate in cell.candidates)
            {
                randomVal -= mapSegments[mapTiles[candidate].segmentIndex].weight;
                if (randomVal <= Mathf.Epsilon)
                {
                    return candidate;
                }
            }

            return cell.candidates.First();
        }

        /// <summary>
        /// Sets the chosen index as the only candidate.
        /// Enqueues all removed candidates.
        /// </summary>
        private void CollapseCell(Cell cell, Vector3Int cellPos, int chosenIndex)
        {
            // for every candidate in the chosen cell to collapse
            foreach (int candidate in cell.candidates)
            {
                // if the candidate is not the chosen candidate
                if (candidate != chosenIndex)
                {
                    // remove the candidate with all its siblings
                    var siblings = GetSiblings(candidate, cellPos);
                    for (int i = 0; i < siblings.Count; i++)
                    {
                        if (siblings[i].tileIndex == candidate || siblings[i].cell.candidates.Remove(siblings[i].tileIndex))
                        {
                            removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[i].cellPosition, tileIndex = siblings[i].tileIndex });
                        }
                    }
                }
            }

            cell.isCollapsed = true;
            remainingCellCount--;
            cell.candidates = new HashSet<int> { chosenIndex };
        }
        #endregion

        #region Helpers
        public bool TryGetCellAt(Vector3Int position, out Cell cell)
        {
            if (position.x >= 0 && position.x <= cellGrid.GetUpperBound(0)
                && position.y >= 0 && position.y <= cellGrid[position.x].GetUpperBound(0)
                && position.z >= 0 && position.z <= cellGrid[position.x][position.y].GetUpperBound(0))
            {
                cell = cellGrid[position.x][position.y][position.z];
                return true;
            }

            cell = null;
            return false;
        }

        /// <summary>
        /// Gets all tiles belonging to the same segment
        /// </summary>
        public List<Sibling> GetSiblings(int tileIndex, Vector3Int cellPos)
        {
            MapSegment parent = mapSegments[mapTiles[tileIndex].segmentIndex];
            Vector3Int segmentPos = cellPos - mapTiles[tileIndex].position;

            List<Sibling> siblings = new List<Sibling>();

            foreach (var entry in parent.posToTile)
            {
                TryGetCellAt(segmentPos + entry.Key, out Cell newCell);
                if (newCell != null)
                {
                    siblings.Add(new Sibling() { cell = newCell, cellPosition = segmentPos + entry.Key, tileIndex = entry.Value });
                }
            }

            return siblings;
        }

        public bool TryGetNeighborInDirection(int direction, ref Vector3Int cellPosition, out Cell cell)
        {
            switch (direction)
            {
                case Direction.UP:
                    cellPosition = new Vector3Int(cellPosition.x, cellPosition.y + 1, cellPosition.z);
                    if (cellPosition.y > cellGrid[cellPosition.x].GetUpperBound(0))
                    {
                        cell = null;
                        return false;
                    }
                    break;
                case Direction.DOWN:
                    cellPosition = new Vector3Int(cellPosition.x, cellPosition.y - 1, cellPosition.z);
                    if (cellPosition.y < 0)
                    {
                        cell = null;
                        return false;
                    }
                    break;
                case Direction.FRONT:
                    cellPosition = new Vector3Int(cellPosition.x, cellPosition.y, cellPosition.z + 1);
                    if (cellPosition.z > cellGrid[cellPosition.x][cellPosition.y].GetUpperBound(0))
                    {
                        cell = null;
                        return false;
                    }
                    break;
                case Direction.BACK:
                    cellPosition = new Vector3Int(cellPosition.x, cellPosition.y, cellPosition.z - 1);
                    if (cellPosition.z < 0)
                    {
                        cell = null;
                        return false;
                    }
                    break;
                case Direction.RIGHT:
                    cellPosition = new Vector3Int(cellPosition.x + 1, cellPosition.y, cellPosition.z);
                    if (cellPosition.x > cellGrid.GetUpperBound(0))
                    {
                        cell = null;
                        return false;
                    }
                    break;
                case Direction.LEFT:
                    cellPosition = new Vector3Int(cellPosition.x - 1, cellPosition.y, cellPosition.z);
                    if (cellPosition.x < 0)
                    {
                        cell = null;
                        return false;
                    }
                    break;
            }

            cell = cellGrid[cellPosition.x][cellPosition.y][cellPosition.z];
            return true;
        }
        #endregion

        #region Print Functions
        public void PrintAllCandidates()
        {
            for (int x = 0; x < cellGrid.Length; x++)
            {
                for (int y = 0; y < cellGrid[0].Length; y++)
                {
                    for (int z = 0; z < cellGrid[0][0].Length; z++)
                    {
                        Cell cell = cellGrid[x][y][z];
                        Debug.Log($"Cell at: {x},{y},{z}. Entropy:{cell.GetEntropy()}. Candidates: {string.Join(",", cell.candidates)}");
                    }
                }
            }
        }
        #endregion
    }
}
