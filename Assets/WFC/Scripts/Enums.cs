﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

// TODO: Clean up enum file

[System.Serializable]
public enum DirectionOld
{
    Up = 1, Down = -1, Front = 2, Back = -2, Right = 3, Left = -3
}

public static class ExtensionMethods
{
    public static DirectionOld Opposite (this DirectionOld dir)
    {
        switch (dir)
        {
            case DirectionOld.Up:
                return DirectionOld.Down;
            case DirectionOld.Down:
                return DirectionOld.Up;
            case DirectionOld.Front:
                return DirectionOld.Back;
            case DirectionOld.Back:
                return DirectionOld.Front;
            case DirectionOld.Right:
                return DirectionOld.Left;
            case DirectionOld.Left:
                return DirectionOld.Right;
        }
        return DirectionOld.Right;
    }

    public static Vector3 Forward(this DirectionOld dir, Transform transform)
    {
        int reflectX = transform.lossyScale.x < 0 ? -1 : 1;
        int reflectY = transform.lossyScale.y < 0 ? -1 : 1;
        int reflectZ = transform.lossyScale.z < 0 ? -1 : 1;
        switch (dir)
        {
            case DirectionOld.Up:
                return transform.up * reflectY;
            case DirectionOld.Down:
                return -transform.up * reflectY;
            case DirectionOld.Front:
                return transform.forward * reflectZ;
            case DirectionOld.Back:
                return -transform.forward * reflectZ;
            case DirectionOld.Right:
                return transform.right * reflectX;
            case DirectionOld.Left:
                return -transform.right * reflectX;
            default:
                return Vector3.zero;
        }
    }

    public static float ForwardLength(this DirectionOld dir, Vector3 size)
    {
        switch (dir)
        {
            case DirectionOld.Up:
            case DirectionOld.Down:
                return size.y;
            case DirectionOld.Front:
            case DirectionOld.Back:
                return size.z;
            case DirectionOld.Right:
            case DirectionOld.Left:
                return size.x;
            default:
                return 0;
        }
    }

    public static Vector3 Up(this DirectionOld dir, Transform transform)
    {
        int reflectY = transform.lossyScale.y < 0 ? -1 : 1;
        int reflectZ = transform.lossyScale.z < 0 ? -1 : 1;
        switch (dir)
        {
            case DirectionOld.Up:
                return transform.forward * reflectZ;
            case DirectionOld.Down:
                return transform.forward * reflectZ;
            case DirectionOld.Front:
                return transform.up * reflectY;
            case DirectionOld.Back:
                return transform.up * reflectY;
            case DirectionOld.Right:
                return transform.up * reflectY;
            case DirectionOld.Left:
                return transform.up * reflectY;
            default:
                return Vector3.zero;
        }
    }

    public static float UpLength(this DirectionOld dir, Vector3 size)
    {
        switch (dir)
        {
            case DirectionOld.Up:
            case DirectionOld.Down:
                return size.z;
            case DirectionOld.Front:
            case DirectionOld.Back:
                return size.y;
            case DirectionOld.Right:
            case DirectionOld.Left:
                return size.y;
            default:
                return 0;
        }
    }

    public static Vector3 Right(this DirectionOld dir, Transform transform)
    {
        int reflectX = transform.lossyScale.x < 0 ? -1 : 1;
        int reflectZ = transform.lossyScale.z < 0 ? -1 : 1;
        switch (dir)
        {
            case DirectionOld.Up:
                return transform.right * reflectX;
            case DirectionOld.Down:
                return transform.right * reflectX;
            case DirectionOld.Front:
                return transform.right * reflectX;
            case DirectionOld.Back:
                return transform.right * reflectX;
            case DirectionOld.Right:
                return transform.forward * reflectZ;
            case DirectionOld.Left:
                return transform.forward * reflectZ;
            default:
                return Vector3.zero;
                // Removed - from back and right
        }
    }

    public static float RightLength(this DirectionOld dir, Vector3 size)
    {
        switch (dir)
        {
            case DirectionOld.Up:
            case DirectionOld.Down:
                return size.x;
            case DirectionOld.Front:
            case DirectionOld.Back:
                return size.x;
            case DirectionOld.Right:
            case DirectionOld.Left:
                return size.z;
            default:
                return 0;
        }
    }

    public static Vector3Int ToVector3Int(this DirectionOld dir)
    {
        switch (dir)
        {
            case DirectionOld.Up:
                return new Vector3Int(0, 1, 0);
            case DirectionOld.Down:
                return new Vector3Int(0, -1, 0);
            case DirectionOld.Front:
                return new Vector3Int(0, 0, 1);
            case DirectionOld.Back:
                return new Vector3Int(0, 0, -1);
            case DirectionOld.Right:
                return new Vector3Int(1, 0, 0);
            case DirectionOld.Left:
                return new Vector3Int(-1, 0, 0);
            default:
                return new Vector3Int(1, 1, 1);
        }
    }

    public static Vector3Int TransformVector(this Vector3Int vector, ERotator rotator, EReflector reflector)
    {
        Vector3Int output = new Vector3Int(vector.x, vector.y, vector.z);

        switch (reflector)
        {
            case EReflector.NegX:
                output = new Vector3Int(-output.x, output.y, output.z);
                break;
            case EReflector.NegY:
                output = new Vector3Int(-output.x, -output.y, output.z);
                break;
            case EReflector.NegZ:
                output = new Vector3Int(output.x, output.y, -output.z);
                break;
            case EReflector.NegXY:
                output = new Vector3Int(-output.x, -output.y, output.z);
                break;
            case EReflector.NegXZ:
                output = new Vector3Int(-output.x, output.y, -output.z);
                break;
            case EReflector.NegYZ:
                output = new Vector3Int(output.x, -output.y, -output.z);
                break;
            case EReflector.NegXYZ:
                output = new Vector3Int(-output.x, -output.y, -output.z);
                break;
        }

        switch (rotator)
        {
            case ERotator.x90:
                output = new Vector3Int(output.x, -output.z, output.y);
                break;
            case ERotator.x180:
                output = new Vector3Int(output.x, -output.y, -output.z);
                break;
            case ERotator.x270:
                output = new Vector3Int(output.x, output.z, -output.y);
                break;
            case ERotator.y90:
                output = new Vector3Int(output.z, output.y, -output.x);
                break;
            case ERotator.y180:
                output = new Vector3Int(-output.x, output.y, -output.z);
                break;
            case ERotator.y270:
                output = new Vector3Int(-output.z, output.y, output.x);
                break;
            case ERotator.z90:
                output = new Vector3Int(-output.y, output.x, output.z);
                break;
            case ERotator.z180:
                output = new Vector3Int(-output.x, -output.y, output.z);
                break;
            case ERotator.z270:
                output = new Vector3Int(output.y, -output.x, output.z);
                break;
            case ERotator.x90y90:
                output = new Vector3Int(output.y, -output.z, -output.x);
                break;
            case ERotator.x90y180:
                output = new Vector3Int(-output.x, -output.z, -output.y);
                break;
            case ERotator.x90y270:
                output = new Vector3Int(-output.y, -output.z, output.x);
                break;
            case ERotator.x180y90:
                output = new Vector3Int(-output.z, -output.y, -output.x);
                break;
            case ERotator.x180y180:
                output = new Vector3Int(-output.x, -output.y, output.z);
                break;
            case ERotator.x180y270:
                output = new Vector3Int(output.z, -output.y, output.x);
                break;
            case ERotator.x270y90:
                output = new Vector3Int(-output.y, output.z, -output.x);
                break;
            case ERotator.x270y180:
                output = new Vector3Int(-output.x, output.z, output.y);
                break;
            case ERotator.x270y270:
                output = new Vector3Int(output.y, output.z, output.x);
                break;
            case ERotator.x90z90:
                output = new Vector3Int(-output.y, -output.z, output.x);
                break;
            case ERotator.x90z180:
                output = new Vector3Int(-output.x, -output.z, -output.y);
                break;
            case ERotator.x90z270:
                output = new Vector3Int(output.y, -output.z, -output.x);
                break;
            case ERotator.x180z90:
                output = new Vector3Int(-output.y, -output.x, -output.z);
                break;
            case ERotator.x180z180:
                output = new Vector3Int(-output.x, output.y, -output.z);
                break;
            case ERotator.x180z270:
                output = new Vector3Int(output.y, output.x, -output.z);
                break;
            case ERotator.x270z90:
                output = new Vector3Int(-output.y, output.z, -output.x);
                break;
            case ERotator.x270z180:
                output = new Vector3Int(-output.x, output.z, output.y);
                break;
            case ERotator.x270z270:
                output = new Vector3Int(output.y, output.z, output.x);
                break;
            case ERotator.y90z90:
                output = new Vector3Int(output.z, output.x, output.y);
                break;
            case ERotator.y90z180:
                output = new Vector3Int(output.z, -output.y, output.x);
                break;
            case ERotator.y90z270:
                output = new Vector3Int(output.z, -output.x, -output.y);
                break;
            case ERotator.y180z90:
                output = new Vector3Int(output.y, output.x, -output.z);
                break;
            case ERotator.y180z180:
                output = new Vector3Int(output.x, -output.y, -output.z);
                break;
            case ERotator.y180z270:
                output = new Vector3Int(-output.y, -output.x, -output.z);
                break;
            case ERotator.y270z90:
                output = new Vector3Int(-output.z, output.x, -output.y);
                break;
            case ERotator.y270z180:
                output = new Vector3Int(-output.z, -output.y, -output.x);
                break;
            case ERotator.y270z270:
                output = new Vector3Int(-output.z, -output.x, output.y);
                break;
        }

        //switch (alteration)
        //{
        //    case TransformAlteration.X90:
        //        return new Vector3Int(vector.x, -vector.z, vector.y);
        //    case TransformAlteration.X180:
        //        return new Vector3Int(vector.x, -vector.y, -vector.z);
        //    case TransformAlteration.X270:
        //        return new Vector3Int(vector.x, vector.z, -vector.y);
        //    case TransformAlteration.Y90:
        //        return new Vector3Int(vector.z, vector.y, -vector.x);
        //    case TransformAlteration.Y180:
        //        return new Vector3Int(-vector.x, vector.y, -vector.z);
        //    case TransformAlteration.Y270:
        //        return new Vector3Int(-vector.z, vector.y, vector.x);
        //    case TransformAlteration.Z90:
        //        return new Vector3Int(-vector.y, vector.x, vector.z);
        //    case TransformAlteration.Z180:
        //        return new Vector3Int(-vector.x, -vector.y, vector.z);
        //    case TransformAlteration.Z270:
        //        return new Vector3Int(vector.y, -vector.x, vector.z);
        //    case TransformAlteration.NegX:
        //        return new Vector3Int(-vector.x, vector.y, vector.z);
        //    case TransformAlteration.NegY:
        //        return new Vector3Int(vector.x, -vector.y, vector.z);
        //    case TransformAlteration.NegZ:
        //        return new Vector3Int(vector.x, vector.y, -vector.z);
        //}

        return output;
    }


    public static DirectionOld TransformDirection(this DirectionOld dir, ERotator rotator, EReflector reflector)
    {
        DirectionOld result = dir;

        switch (reflector)
        {
            case EReflector.NegX:
                if (dir == DirectionOld.Right || dir == DirectionOld.Left)
                    result = dir.Opposite();
                break;
            case EReflector.NegY:
                if (dir == DirectionOld.Up || dir == DirectionOld.Down)
                    result = dir.Opposite();
                break;
            case EReflector.NegZ:
                if (dir == DirectionOld.Front || dir == DirectionOld.Back)
                    result = dir.Opposite();
                break;
            case EReflector.NegXY:
                if (dir != DirectionOld.Front && dir != DirectionOld.Back)
                    result = dir.Opposite();
                break;
            case EReflector.NegXZ:
                if (dir != DirectionOld.Up && dir != DirectionOld.Down)
                    result = dir.Opposite();
                break;
            case EReflector.NegYZ:
                if (dir != DirectionOld.Right && dir != DirectionOld.Left)
                    result = dir.Opposite();
                break;
            case EReflector.NegXYZ:
                result = dir.Opposite();
                break;
        }

        switch (rotator)
        {
            case ERotator.x90:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Down;
                        break;
                }
                break;
            case ERotator.x180:
                switch (dir)
                {
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                }
                break;
            case ERotator.x270:
                switch (dir)
                {
                    case DirectionOld.Down:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Down;
                        break;
                }
                break;
            case ERotator.y90:
                switch (dir)
                {
                    case DirectionOld.Left:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.y180:
                switch (dir)
                {
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.y270:
                switch (dir)
                {
                    case DirectionOld.Right:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.z90:
                switch (dir)
                {
                    case DirectionOld.Right:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.z270:
                switch (dir)
                {
                    case DirectionOld.Left:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.x90y90:
                switch (dir)
                {
                    case DirectionOld.Back:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.x90y180:
                switch (dir)
                {
                    case DirectionOld.Back:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.x90y270:
                switch (dir)
                {
                    case DirectionOld.Back:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.x180y90:
                switch (dir)
                {
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.x180y180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.x180y270:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.x270y90:
                switch (dir)
                {
                    case DirectionOld.Front:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.x270y180:
                switch (dir)
                {
                    case DirectionOld.Front:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.x270y270:
                switch (dir)
                {
                    case DirectionOld.Front:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.x90z90:
                switch (dir)
                {
                    case DirectionOld.Back:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.x90z180:
                switch (dir)
                {
                    case DirectionOld.Back:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.x90z270:
                switch (dir)
                {
                    case DirectionOld.Back:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.x180z90:
                switch (dir)
                {
                    case DirectionOld.Left:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.x180z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.x180z270:
                switch (dir)
                {
                    case DirectionOld.Right:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.x270z90:
                switch (dir)
                {
                    case DirectionOld.Front:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.x270z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.x270z270:
                switch (dir)
                {
                    case DirectionOld.Front:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.y90z90:
                switch (dir)
                {
                    case DirectionOld.Right:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.y90z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.y90z270:
                switch (dir)
                {
                    case DirectionOld.Left:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.y180z90:
                switch (dir)
                {
                    case DirectionOld.Right:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.y180z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.y180z270:
                switch (dir)
                {
                    case DirectionOld.Left:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.y270z90:
                switch (dir)
                {
                    case DirectionOld.Right:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.y270z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.y270z270:
                switch (dir)
                {
                    case DirectionOld.Left:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Left;
                        break;
                }
                break;
        }

        return result;
    }

    public static DirectionOld InverseTransformDirection(this DirectionOld dir, ERotator rotator, EReflector reflector)
    {
        DirectionOld result = dir;

        switch (reflector)
        {
            case EReflector.NegX:
                if (dir == DirectionOld.Right || dir == DirectionOld.Left)
                    result = dir.Opposite();
                break;
            case EReflector.NegY:
                if (dir == DirectionOld.Up || dir == DirectionOld.Down)
                    result = dir.Opposite();
                break;
            case EReflector.NegZ:
                if (dir == DirectionOld.Front || dir == DirectionOld.Back)
                    result = dir.Opposite();
                break;
            case EReflector.NegXY:
                if (dir != DirectionOld.Front && dir != DirectionOld.Back)
                    result = dir.Opposite();
                break;
            case EReflector.NegXZ:
                if (dir != DirectionOld.Up && dir != DirectionOld.Down)
                    result = dir.Opposite();
                break;
            case EReflector.NegYZ:
                if (dir != DirectionOld.Right && dir != DirectionOld.Left)
                    result = dir.Opposite();
                break;
            case EReflector.NegXYZ:
                result = dir.Opposite();
                break;
        }

        switch (rotator)
        {
            case ERotator.x90:
                switch (dir)
                {
                    case DirectionOld.Front:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Front;
                        break;
                }
                break;
            case ERotator.x180:
                switch (dir)
                {
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                }
                break;
            case ERotator.x270:
                switch (dir)
                {
                    case DirectionOld.Front:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Up:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Back;
                        break;
                }
                break;
            case ERotator.y90:
                switch (dir)
                {
                    case DirectionOld.Front:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Back;
                        break;
                }
                break;
            case ERotator.y180:
                switch (dir)
                {
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.y270:
                switch (dir)
                {
                    case DirectionOld.Front:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Front;
                        break;
                }
                break;
            case ERotator.z90:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Up;
                        break;
                }
                break;
            case ERotator.z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.z270:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Down;
                        break;
                }
                break;
            case ERotator.x90y90:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Down;
                        break;
                }
                break;
            case ERotator.x90y180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.x90y270:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Up;
                        break;
                }
                break;
            case ERotator.x180y90:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Front;
                        break;
                }
                break;
            case ERotator.x180y180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.x180y270:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Back;
                        break;
                }
                break;
            case ERotator.x270y90:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Up;
                        break;
                }
                break;
            case ERotator.x270y180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.x270y270:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Down;
                        break;
                }
                break;
            case ERotator.x90z90:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Up;
                        break;
                }
                break;
            case ERotator.x90z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.x90z270:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Down;
                        break;
                }
                break;
            case ERotator.x180z90:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Up;
                        break;
                }
                break;
            case ERotator.x180z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.x180z270:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Down;
                        break;
                }
                break;
            case ERotator.x270z90:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Up;
                        break;
                }
                break;
            case ERotator.x270z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Right;
                        break;
                }
                break;
            case ERotator.x270z270:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Down;
                        break;
                }
                break;
            case ERotator.y90z90:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Back;
                        break;
                }
                break;
            case ERotator.y90z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Back;
                        break;
                }
                break;
            case ERotator.y90z270:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Back;
                        break;
                }
                break;
            case ERotator.y180z90:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Down;
                        break;
                }
                break;
            case ERotator.y180z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Left;
                        break;
                }
                break;
            case ERotator.y180z270:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Front;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Up;
                        break;
                }
                break;
            case ERotator.y270z90:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Front;
                        break;
                }
                break;
            case ERotator.y270z180:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Front;
                        break;
                }
                break;
            case ERotator.y270z270:
                switch (dir)
                {
                    case DirectionOld.Up:
                        result = DirectionOld.Left;
                        break;
                    case DirectionOld.Down:
                        result = DirectionOld.Right;
                        break;
                    case DirectionOld.Front:
                        result = DirectionOld.Up;
                        break;
                    case DirectionOld.Back:
                        result = DirectionOld.Down;
                        break;
                    case DirectionOld.Right:
                        result = DirectionOld.Back;
                        break;
                    case DirectionOld.Left:
                        result = DirectionOld.Front;
                        break;
                }
                break;
        }

        return result;
    }

    public static DirectionOld ToDirection(this Vector3Int vector)
    {
        if (vector.x == 1)
        {
            return DirectionOld.Right;
        } else if (vector.x == -1)
        {
            return DirectionOld.Left;
        } else if (vector.y == 1)
        {
            return DirectionOld.Up;
        } else if (vector.y == -1)
        {
            return DirectionOld.Down;
        } else if (vector.z == 1)
        {
            return DirectionOld.Front;
        } else
        {
            return DirectionOld.Back;
        }
    }

    public static IList<T> Swap<T>(this IList<T> list, int indexA, int indexB)
    {
        T tmp = list[indexA];
        list[indexA] = list[indexB];
        list[indexB] = tmp;
        return list;
    }

    //public static List<T> Swap<T>(this IList<T> list, int indexA, int indexB)
    //{
    //    List<T> tmp = new List<T>(list);
    //    tmp[indexA] = list[indexB];
    //    tmp[indexB] = list[indexA];
    //    return tmp;
    //}

    public static Dictionary<DirectionOld, List<int>> CopyDictionary (this Dictionary<DirectionOld, List<int>> dict)
    {
        Dictionary<DirectionOld, List<int>> result = new Dictionary<DirectionOld, List<int>>();
        foreach (DirectionOld dir in dict.Keys)
        {
            List<int> ls = new List<int>();
            for (int i = 0; i < dict[dir].Count; i++)
            {
                ls.Add(dict[dir][i]);
            }

            result.Add(dir, ls);
        }

        return result;
    }

    //public static Dictionary<Vector3Int, Slot> CopyDictionary (this Dictionary<Vector3Int, Slot> dict)
    //{
    //    Dictionary<Vector3Int, Slot> result = new Dictionary<Vector3Int, Slot>();
    //    foreach (Vector3Int pos in dict.Keys)
    //    {
    //        result.Add(pos, new Slot(dict[pos]));
    //    }

    //    return result;
    //}

    public static Quaternion ToRotation(this ERotator rotator)
    {
        switch (rotator)
        {
            case ERotator.x90:
                return Quaternion.Euler(90, 0, 0);
            case ERotator.x180:
                return Quaternion.Euler(180, 0, 0);
            case ERotator.x270:
                return Quaternion.Euler(270, 0, 0);
            case ERotator.y90:
                return Quaternion.Euler(0, 90, 0);
            case ERotator.y180:
                return Quaternion.Euler(0, 180, 0);
            case ERotator.y270:
                return Quaternion.Euler(0, 270, 0);
            case ERotator.z90:
                return Quaternion.Euler(0, 0, 90);
            case ERotator.z180:
                return Quaternion.Euler(0, 0, 180);
            case ERotator.z270:
                return Quaternion.Euler(0, 0, 270);
            case ERotator.x90y90:
                return Quaternion.Euler(90, 90, 0);
            case ERotator.x90y180:
                return Quaternion.Euler(90, 180, 0);
            case ERotator.x90y270:
                return Quaternion.Euler(90, 270, 0);
            case ERotator.x180y90:
                return Quaternion.Euler(180, 90, 0);
            case ERotator.x180y180:
                return Quaternion.Euler(180, 180, 0);
            case ERotator.x180y270:
                return Quaternion.Euler(180, 270, 0);
            case ERotator.x270y90:
                return Quaternion.Euler(270, 90, 0);
            case ERotator.x270y180:
                return Quaternion.Euler(270, 180, 0);
            case ERotator.x270y270:
                return Quaternion.Euler(270, 270, 0);
            case ERotator.x90z90:
                return Quaternion.Euler(90, 0, 90);
            case ERotator.x90z180:
                return Quaternion.Euler(90, 0, 180);
            case ERotator.x90z270:
                return Quaternion.Euler(90, 0, 270);
            case ERotator.x180z90:
                return Quaternion.Euler(180, 0, 90);
            case ERotator.x180z180:
                return Quaternion.Euler(180, 0, 180);
            case ERotator.x180z270:
                return Quaternion.Euler(180, 0, 270);
            case ERotator.x270z90:
                return Quaternion.Euler(270, 0, 90);
            case ERotator.x270z180:
                return Quaternion.Euler(270, 0, 180);
            case ERotator.x270z270:
                return Quaternion.Euler(270, 0, 270);
            case ERotator.y90z90:
                return Quaternion.Euler(0, 90, 90);
            case ERotator.y90z180:
                return Quaternion.Euler(0, 90, 180);
            case ERotator.y90z270:
                return Quaternion.Euler(0, 90, 270);
            case ERotator.y180z90:
                return Quaternion.Euler(0, 180, 90);
            case ERotator.y180z180:
                return Quaternion.Euler(0, 180, 180);
            case ERotator.y180z270:
                return Quaternion.Euler(0, 180, 270);
            case ERotator.y270z90:
                return Quaternion.Euler(0, 270, 90);
            case ERotator.y270z180:
                return Quaternion.Euler(0, 270, 180);
            case ERotator.y270z270:
                return Quaternion.Euler(0, 270, 270);
            default:
                return Quaternion.identity;
        }
    }

    public static Vector3 ToVector(this EReflector reflector)
    {
        switch (reflector)
        {
            case EReflector.NegX:
                return new Vector3(-1, 1, 1);
            case EReflector.NegY:
                return new Vector3(1, -1, 1);
            case EReflector.NegZ:
                return new Vector3(1, 1, -1);
            case EReflector.NegXY:
                return new Vector3(-1, -1, 1);
            case EReflector.NegXZ:
                return new Vector3(-1, 1, -1);
            case EReflector.NegYZ:
                return new Vector3(1, -1, -1);
            case EReflector.NegXYZ:
                return new Vector3(-1, -1, -1);
        }

        return Vector3.one;
    }

    public static Vector3 ToScale(this EReflector reflector, Vector3 unalteredScale)
    {
        unalteredScale = new Vector3(Mathf.Abs(unalteredScale.x), Mathf.Abs(unalteredScale.y), Mathf.Abs(unalteredScale.z));
        switch (reflector)
        {
            case EReflector.NegX:
                return Vector3.Scale(unalteredScale, new Vector3(-1, 1, 1));
            case EReflector.NegY:
                return Vector3.Scale(unalteredScale, new Vector3(1, -1, 1));
            case EReflector.NegZ:
                return Vector3.Scale(unalteredScale, new Vector3(1, 1, -1));
            case EReflector.NegXY:
                return Vector3.Scale(unalteredScale, new Vector3(-1, -1, 1));
            case EReflector.NegXZ:
                return Vector3.Scale(unalteredScale, new Vector3(-1, 1, -1));
            case EReflector.NegYZ:
                return Vector3.Scale(unalteredScale, new Vector3(1, -1, -1));
            case EReflector.NegXYZ:
                return Vector3.Scale(unalteredScale, new Vector3(-1, -1, -1));
            default:
                return unalteredScale;
        }
    }

    public static EReflector RotateReflector(this EReflector reflector, ERotator rotator)
    {
        switch (rotator)
        {
            case ERotator.x90:
            case ERotator.x270:
            case ERotator.x90y180:
            case ERotator.x270y180:
            case ERotator.x90z180:
            case ERotator.x270z180:
                switch (reflector)
                {
                    case EReflector.NegY:
                        return EReflector.NegZ;
                    case EReflector.NegZ:
                        return EReflector.NegY;
                    case EReflector.NegXY:
                        return EReflector.NegXZ;
                    case EReflector.NegXZ:
                        return EReflector.NegXY;
                }
                break;
            case ERotator.y90:
            case ERotator.y270:
            case ERotator.x180y90:
            case ERotator.x180y270:
            case ERotator.y90z180:
            case ERotator.y270z180:
                switch (reflector)
                {
                    case EReflector.NegX:
                        return EReflector.NegZ;
                    case EReflector.NegZ:
                        return EReflector.NegX;
                    case EReflector.NegXY:
                        return EReflector.NegYZ;
                    case EReflector.NegYZ:
                        return EReflector.NegXY;
                }
                break;
            case ERotator.z90:
            case ERotator.z270:
            case ERotator.x180z90:
            case ERotator.x180z270:
            case ERotator.y180z90:
            case ERotator.y180z270:
                switch (reflector)
                {
                    case EReflector.NegX:
                        return EReflector.NegY;
                    case EReflector.NegY:
                        return EReflector.NegX;
                    case EReflector.NegXZ:
                        return EReflector.NegYZ;
                    case EReflector.NegYZ:
                        return EReflector.NegXZ;
                }
                break;
            case ERotator.x90y90:
            case ERotator.x90y270:
            case ERotator.x270y90:
            case ERotator.x270y270:
            case ERotator.x90z90:
            case ERotator.x90z270:
            case ERotator.x270z90:
            case ERotator.x270z270:
                switch (reflector)
                {
                    case EReflector.NegX:
                        return EReflector.NegZ;
                    case EReflector.NegY:
                        return EReflector.NegX;
                    case EReflector.NegZ:
                        return EReflector.NegY;
                    case EReflector.NegXY:
                        return EReflector.NegXZ;
                    case EReflector.NegXZ:
                        return EReflector.NegYZ;
                    case EReflector.NegYZ:
                        return EReflector.NegXY;
                }
                break;
            case ERotator.y90z90:
            case ERotator.y90z270:
            case ERotator.y270z90:
            case ERotator.y270z270:
                switch (reflector)
                {
                    case EReflector.NegX:
                        return EReflector.NegY;
                    case EReflector.NegY:
                        return EReflector.NegZ;
                    case EReflector.NegZ:
                        return EReflector.NegX;
                    case EReflector.NegXY:
                        return EReflector.NegYZ;
                    case EReflector.NegXZ:
                        return EReflector.NegXY;
                    case EReflector.NegYZ:
                        return EReflector.NegXZ;
                }
                break;
        }

        return reflector;
    }
}

public struct Variation
{
    public ERotator rotator;
    public EReflector reflector;
}

public enum ERotator
{
    None, 
    x90, x180, x270, 
    y90, y180, y270, 
    z90, z180, z270, 
    x90y90, x90y180, x90y270, 
    x180y90, x180y180, x180y270, 
    x270y90, x270y180, x270y270,
    x90z90, x90z180, x90z270,
    x180z90, x180z180, x180z270,
    x270z90, x270z180, x270z270,
    y90z90, y90z180, y90z270,
    y180z90, y180z180, y180z270,
    y270z90, y270z180, y270z270
}

public enum EReflector
{
    None, 
    NegX, NegY, NegZ,
    NegXY, NegXZ, NegYZ, 
    NegXYZ
}