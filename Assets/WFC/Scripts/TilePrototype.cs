﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEditor;


namespace WaveFunctionCollapse
{
    [ExecuteInEditMode]
    /// <summary>
    /// Add this component to every tile used for procedural generation.
    /// Tiles can be colored to define neighbor compatibility
    /// </summary>
    public class TilePrototype : MonoBehaviour
    {
        /// <summary>
        /// Map position (relative to local pivot) to tile data
        /// </summary>
        public Dictionary<Vector3Int, EditorTile> posToTile = new Dictionary<Vector3Int, EditorTile>();
        public Vector3 tileSize = Vector3.one;
        public Vector3 origin = Vector3.zero;
        public TileColorSettings colorSettings;
        [Range(0, 99)]
        public float weight = 1;
        public bool spawn = true;
        public bool isRamp = false;
        public bool onlyOnPath = false;
        public bool onlyOnNonPath = false;
        public List<GameObject> invalidNeighbors;



        /// <summary>
        /// Save tile data here, so they can be serialized
        /// </summary>
        [HideInInspector] public List<SavedTile> savedTiles = new List<SavedTile>();

        private void OnEnable()
        {
            if (posToTile.Count == 0)
            {
                if (savedTiles.Count == 0)
                {
                    AddTile(new Vector3Int(0, 0, 0));
                }
                else
                {
                    FillDictionaryWithSavedTiles();
                }
            }
        }

        public void AddTile(Vector3Int tilePos)
        {
            EditorTile tile = new EditorTile();

            // for every direction
            for (int dir = 0; dir < 6; dir++)
            {
                // If there's no neighbor in direction
                if (!posToTile.ContainsKey(tilePos + Direction.ToVector(dir)))
                {
                    // Add a face in direction
                    tile.AddFace(dir);
                }
                else
                {
                    // else remove the face from the neighbor that points towards this tile 
                    EditorTile neighbor = posToTile[tilePos + Direction.ToVector(dir)];
                    neighbor.RemoveFace(Direction.Opposite(dir));
                    // and from this tile
                    tile.RemoveFace(dir);
                }
            }

            posToTile.Add(tilePos, tile);

            SaveToList();
        }

        public void RemoveTile(Vector3Int tilePos)
        {
            // Remove tile from dictionary
            posToTile.Remove(tilePos);

            // Add the face back for each neighboring tile
            for (int dir = 0; dir < 6; dir++)
            {
                Vector3Int neighborPos = tilePos + Direction.ToVector(dir);
                if (posToTile.ContainsKey(neighborPos))
                {
                    posToTile[neighborPos].AddFace(Direction.Opposite(dir));
                }
            }

            SaveToList();
        }

        public void FindAllUniqueVariations()
        {

        }

        public void SaveToList()
        {
            savedTiles = new List<SavedTile>(savedTiles.Count);
            foreach (var posToTile in posToTile)
            {
                EditorTile editorTile = posToTile.Value;
                SavedTile tile = new SavedTile();
                tile.position = posToTile.Key;

                for (int dir = 0; dir < 6; dir++)
                {
                    tile.isSplit[dir] = editorTile.isSplit[dir];
                }

                editorTile.faceIDs[Direction.UP].CopyTo(tile.faceUp, 0);
                editorTile.faceIDs[Direction.DOWN].CopyTo(tile.faceDown, 0);
                editorTile.faceIDs[Direction.FRONT].CopyTo(tile.faceFront, 0);
                editorTile.faceIDs[Direction.BACK].CopyTo(tile.faceBack, 0);
                editorTile.faceIDs[Direction.RIGHT].CopyTo(tile.faceRight, 0);
                editorTile.faceIDs[Direction.LEFT].CopyTo(tile.faceLeft, 0);

                editorTile.path[Direction.UP].CopyTo(tile.pathUp, 0);
                editorTile.path[Direction.DOWN].CopyTo(tile.pathDown, 0);
                editorTile.path[Direction.FRONT].CopyTo(tile.pathFront, 0);
                editorTile.path[Direction.BACK].CopyTo(tile.pathBack, 0);
                editorTile.path[Direction.RIGHT].CopyTo(tile.pathRight, 0);
                editorTile.path[Direction.LEFT].CopyTo(tile.pathLeft, 0);

                savedTiles.Add(tile);
            }
        }

        public void FillDictionaryWithSavedTiles()
        {
            for (int i = 0; i < savedTiles.Count; i++)
            {
                SavedTile savedTile = savedTiles[i];
                EditorTile tile = new EditorTile();

                for (int dir = 0; dir < 6; dir++)
                {
                    tile.isSplit[dir] = savedTile.isSplit[dir];
                }

                savedTile.faceUp.CopyTo(tile.faceIDs[Direction.UP], 0);
                savedTile.faceDown.CopyTo(tile.faceIDs[Direction.DOWN], 0);
                savedTile.faceFront.CopyTo(tile.faceIDs[Direction.FRONT], 0);
                savedTile.faceBack.CopyTo(tile.faceIDs[Direction.BACK], 0);
                savedTile.faceRight.CopyTo(tile.faceIDs[Direction.RIGHT], 0);
                savedTile.faceLeft.CopyTo(tile.faceIDs[Direction.LEFT], 0);

                savedTile.pathUp.CopyTo(tile.path[Direction.UP], 0);
                savedTile.pathDown.CopyTo(tile.path[Direction.DOWN], 0);
                savedTile.pathFront.CopyTo(tile.path[Direction.FRONT], 0);
                savedTile.pathBack.CopyTo(tile.path[Direction.BACK], 0);
                savedTile.pathRight.CopyTo(tile.path[Direction.RIGHT], 0);
                savedTile.pathLeft.CopyTo(tile.path[Direction.LEFT], 0);

                posToTile.Add(savedTile.position, tile);
            }
        }

        private void Reset()
        {
            if (posToTile.Count > 0)
            {
                posToTile.Clear();
                savedTiles.Clear();
                AddTile(new Vector3Int(0, 0, 0));
            }
        }
    }







    /// <summary>
    /// Contains all faceids of a tile in every direction
    /// </summary>
    public class EditorTile
    {
        /// <summary>
        /// int[direction][faceIDs]
        /// </summary>
        public int[][] faceIDs;
        public bool[] isSplit;

        // path [upDirection][exit]
        // 0 = undefined, 1 = exit, 2 = blocked
        public int[][] path;

        public EditorTile()
        {
            faceIDs = new int[6][];
            isSplit = new bool[6];
            path = new int[6][];

            for (int dir = 0; dir < 6; dir++)
            {
                isSplit[dir] = false;
                faceIDs[dir] = new int[4];
                path[dir] = new int[6];

                for (int i = 0; i < 4; i++)
                {
                    faceIDs[dir][i] = 0;
                }
                for (int i = 0; i < 6; i++)
                {
                    path[dir][i] = 0;
                }
            }
        }

        public void SetAll(int dir, int id)
        {
            for (int i = 0; i < 4; i++)
            {
                faceIDs[dir][i] = id;
            }
        }

        /// <summary>
        /// Set all ids to 0 (default)
        /// </summary>
        public void AddFace(int dir)
        {
            for (int i = 0; i < 4; i++)
            {
                faceIDs[dir][i] = 0;
            }
        }

        /// <summary>
        /// Set all ids to -1 (invalid)
        /// </summary>
        public void RemoveFace(int dir)
        {
            isSplit[dir] = false;
            for (int i = 0; i < 4; i++)
            {
                faceIDs[dir][i] = -1;
            }
        }

        public bool HasFace(int dir)
        {
            if (faceIDs[dir][0] == -1)
                return false;
            return true;
        }

        public void SetPath(int upDirection, int exit, int exitValue)
        {
            path[upDirection][exit] = exitValue;
        }
    }

    [System.Serializable]
    public class SavedTile
    {
        public Vector3Int position;
        public int[] faceUp;
        public int[] faceDown;
        public int[] faceFront;
        public int[] faceBack;
        public int[] faceRight;
        public int[] faceLeft;
        public bool[] isSplit;
        public int[] pathUp;
        public int[] pathDown;
        public int[] pathFront;
        public int[] pathBack;
        public int[] pathRight;
        public int[] pathLeft;

        public SavedTile()
        {
            faceUp = new int[4];
            faceDown = new int[4];
            faceFront = new int[4];
            faceBack = new int[4];
            faceRight = new int[4];
            faceLeft = new int[4];
            isSplit = new bool[6];
            pathUp = new int[6];
            pathDown = new int[6];
            pathFront = new int[6];
            pathBack = new int[6];
            pathRight = new int[6];
            pathLeft = new int[6];
        }

        public int[] GetFace(int dir)
        {
            switch (dir)
            {
                case Direction.UP:
                    return faceUp;
                case Direction.DOWN:
                    return faceDown;
                case Direction.FRONT:
                    return faceFront;
                case Direction.BACK:
                    return faceBack;
                case Direction.RIGHT:
                    return faceRight;
                case Direction.LEFT:
                    return faceLeft;
                default:
                    throw new System.ArgumentException("Invalid Direction Argument");
            }
        }

        public int[] GetPathValues(int dir)
        {
            switch (dir)
            {
                case Direction.UP:
                    return pathUp;
                case Direction.DOWN:
                    return pathDown;
                case Direction.FRONT:
                    return pathFront;
                case Direction.BACK:
                    return pathBack;
                case Direction.RIGHT:
                    return pathRight;
                case Direction.LEFT:
                    return pathLeft;
                default:
                    throw new System.ArgumentException("Invalid Direction Argument");
            }
        }
    }
}