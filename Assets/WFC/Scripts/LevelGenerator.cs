﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Priority_Queue;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Supercluster.KDTree;

#pragma warning disable 0649

namespace WaveFunctionCollapse
{
    public class LevelGenerator : MonoBehaviour
    {
        #region Variables
        [Header("Generator Settings")]
        public Vector3Int gridSize;
        public Vector3Int tileSize;
        [Range(0, 50)][Tooltip("How often the generator will try generating before giving up.")]
        public int numberOfTries = 10;
        [Range(0, 3600)]
        [Tooltip("Time in seconds after which the generator will stop retrying and quit generating." +
            " This will not abort the current try.")]
        public int timeLimit = 240;
        public TileColorSettings colorSettings;
        public GameObject prototypesParent;

        [Header("Border Filter")]
        [Tooltip("Apply a border filter. This can make sure that all edge tiles are proper walls.")]
        public bool useBorderFilter = true;
        [Tooltip("The prototype which is used to check compatibility against.")]
        public TilePrototype borderPrototype;

        [Header("Path Filter")]
        public bool createPaths = true;
        [Tooltip("Number of waypoints to generate. More points make generation more complicated. " +
            "Too many points may cause generation to fail, especially on a small grid.")]
        public int numPoints = 6;
        [Tooltip("Range used for the Weighted Sample Elimination algorithm. " +
            "Two points are only in range of each other within this distance.")]
        public double searchRange = 40;
        [Tooltip("Removes candidates that are not on the generated path, " +
            "yet are marked as tiles that can only spawn on the path")]
        public bool filterPathOnlyTiles = true;

        private Transform levelParent;

        #endregion

        public bool StartGenerator()
        {
            Debug.Log("Start generating...");

            // Create parent object for level
            if (levelParent == null)
            {
                levelParent = GameObject.Find("Level")?.transform;
                if (levelParent == null)
                {
                    levelParent = new GameObject("Level").transform;
                }
            }

            Clear();

            bool success = false;

            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            System.Diagnostics.Stopwatch stopwatchTotal = new System.Diagnostics.Stopwatch();

            try
            {
                stopwatchTotal.Start();
                stopwatch.Start();

                TileBuilder.BuildTilesAndSegments(prototypesParent, useBorderFilter, borderPrototype, out MapTile borderRuleTile, out MapSegment[] segments, out MapTile[] tiles);
                stopwatch.Stop();
                Debug.Log($"Build Tiles: {stopwatch.ElapsedMilliseconds / 1000f}");


                stopwatch.Restart();
                Map map = new Map();
                map.mapSegments = segments;
                map.mapTiles = tiles;

                AdjacencyRules adjacencyRules = new AdjacencyRules();
                adjacencyRules.CreateRules(map, colorSettings, useBorderFilter, borderRuleTile);
                map.adjacencyRules = adjacencyRules;
                stopwatch.Stop();
                Debug.Log($"Adjacency Rules: {stopwatch.ElapsedMilliseconds / 1000f}");
                stopwatch.Restart();

                map.Initialize(gridSize);
                CellBuilder.BuildCells(map, gridSize);
                stopwatch.Stop();
                Debug.Log($"Build Cells: {stopwatch.ElapsedMilliseconds / 1000f}");

                bool mapUsable = map.ApplyBorderFilter(useBorderFilter);
                List<PathPoint> path = null;

                for (int mapGenLoopCount = 0; mapGenLoopCount < numberOfTries && !success && stopwatchTotal.ElapsedMilliseconds < timeLimit * 1000; mapGenLoopCount++)
                {
                    path = null;
                    stopwatch.Restart();
                    for (int pathGenLoopCount = 0; pathGenLoopCount < 100 && path == null && createPaths; pathGenLoopCount++)
                    {
                        path = PathGenerator.Generate(gridSize, numPoints, searchRange);
                    }
                    if (path != null || !createPaths)
                    {
                        stopwatch.Stop();
                        Debug.Log($"Create Path {mapGenLoopCount + 1}: {stopwatch.ElapsedMilliseconds / 1000f}");

                        if (mapGenLoopCount > 0)
                        {
                            map.LoadMapData();
                        }
                        map.ApplyPathFilter(createPaths, filterPathOnlyTiles, path);

                        stopwatch.Restart();
                        success = map.Create();
                        stopwatch.Stop();
                        Debug.Log($"Create Map {mapGenLoopCount+1}: {stopwatch.ElapsedMilliseconds / 1000f}");
                    }
                }


                if (success)
                {
                    for (int i = 0; path != null && i < path.Count; i++)
                    {
                        PathPoint point = path[i];

                        for (int j = 0; j < point.exits.Count; j++)
                        {
                            Debug.DrawRay(levelParent.position + point.position * 3,
                                Direction.ToVector(point.exits[j]) * 3,
                                Color.red, 16f);
                        }
                    }

                    Debug.Log("Success");
                    SpawnMap(map);
                }
                else
                {
                    Debug.LogWarning("Generator failed");
                }

            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                Debug.LogError(e.StackTrace);
            }

            stopwatchTotal.Stop();
            Debug.Log($"Total: {stopwatchTotal.ElapsedMilliseconds / 1000f}");

            return success;
        }

        private void SpawnMap(Map map)
        {
            HashSet<Vector3Int> cellsOccupied = new HashSet<Vector3Int>();

            for (int x = 0; x < gridSize.x; x++)
            {
                for (int y = 0; y < gridSize.y; y++)
                {
                    for (int z = 0; z < gridSize.z; z++)
                    {
                        // Get next cell
                        Cell cell = map.cellGrid[x][y][z];
                        Vector3Int cellPos = new Vector3Int(x, y, z);
                        // Check if cell already occupied
                        if (cellsOccupied.Contains(cellPos))
                            continue;

                        // Get chosen tile for this cell
                        int candidate = cell.candidates.First();
                        MapSegment segment = map.mapSegments[map.mapTiles[candidate].segmentIndex];

                        // Calculate correct spawn position
                        Vector3Int chosenTilePos = map.mapTiles[candidate].position;
                        Vector3Int segmentPos = cellPos - chosenTilePos;
                        Vector3 spawnPos = levelParent.position - TransformHelper.TransformOffset(segment.origin, segment.rotator, segment.reflector) + Vector3.Scale(segmentPos, tileSize);

                        // Spawn segment
                        if (segment.spawn)
                        {
                            GameObject obj = Instantiate(segment.spawnableObject, spawnPos, segment.rotator.ToRotation(), levelParent);
                            obj.transform.localScale = segment.reflector.ToScale(obj.transform.localScale);
                            DestroyImmediate(obj.GetComponent<TilePrototype>());
                        }

                        // Add cell positions occupied by new segment to hashset
                        foreach (Vector3Int tilePos in segment.posToTile.Keys)
                        {
                            cellsOccupied.Add(segmentPos + tilePos);
                        }
                    }
                }
            }
        }

        public void Clear()
        {
            FindLevelParent();

            for (int i = levelParent.childCount - 1; i >= 0 ; i--)
            {
                DestroyImmediate(levelParent.GetChild(i).gameObject);
            }
        }

        public void TestStuff()
        {
            Clear();

            List<PathPoint> path = null;
            for (int i = 0; i < 100 && path == null; i++)
            {
                path = PathGenerator.Generate(gridSize, numPoints, searchRange);
                if (path == null) Debug.Log("NULL");
            }


            for (int i = 0; i < path.Count; i++)
            {
                PathPoint point = path[i];

                for (int j = 0; j < point.exits.Count; j++)
                {
                    Debug.DrawRay(levelParent.position + point.position * 3 + (j == 1? Vector3.right * 0.5f + Vector3.forward * 0.5f : Vector3Int.zero), 
                        Direction.ToVector(point.exits[j]) * 3, 
                        j == 0? Color.red : Color.blue, 6f);
                }
            }




            for (int i = 0; i < path.Count; i++)
            {
                PathPoint point = path[i];

                if (point.isRamp)
                {
                    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    cube.transform.localScale = new Vector3(1, 1, 1);
                    cube.transform.position = levelParent.position + path[i].position * 3;
                    cube.transform.parent = levelParent;
                }
                else
                {
                    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    cube.transform.localScale = new Vector3(1, 1, 1);
                    cube.transform.position = levelParent.position + path[i].position * 3;
                    cube.transform.parent = levelParent;
                }

            }
        }

        public void SpawnStuff(int[][] positions)
        {
            for (int i = 0; i < positions.Length; i++)
            {
                Vector3 position = new Vector3(positions[i][0] * 3, positions[i][1] * 3, positions[i][2] * 3);
                Vector3 scale = new Vector3(3, 3, 3);

                GameObject g = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                g.transform.parent = levelParent;
                g.transform.localScale = scale;
                g.transform.localPosition = position;
                g.transform.parent = null;
            }
        }

        private void FindLevelParent()
        {
            if (levelParent == null)
            {
                levelParent = GameObject.Find("Level")?.transform;
                if (levelParent == null)
                {
                    levelParent = new GameObject("Level").transform;
                }
            }
        }
    }
}