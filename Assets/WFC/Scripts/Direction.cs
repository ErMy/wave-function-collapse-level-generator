using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Direction
{
    public const int UP = 0;
    public const int FRONT = 1;
    public const int RIGHT = 2;
    public const int DOWN = 3;
    public const int BACK = 4;
    public const int LEFT = 5;

    // these are for pathfinding
    public const int UNDEFINED = -1;
    public const int NONE = -2;

    public static int Opposite(int direction) 
    {
        return (direction + 3) % 6;
    }

    public static string ToString(int dir)
    {
        switch (dir)
        {
            case UP:
                return "Up";
            case DOWN:
                return "Down";
            case RIGHT:
                return "Right";
            case LEFT:
                return "Left";
            case FRONT:
                return "Front";
            case BACK:
                return "Back";
            default:
                return "None";
        }
    }

    public static Vector3Int ToVector(int direction)
    {
        switch (direction)
        {
            case UP:
                return new Vector3Int(0, 1, 0);
            case DOWN:
                return new Vector3Int(0, -1, 0);
            case FRONT:
                return new Vector3Int(0, 0, 1);
            case BACK:
                return new Vector3Int(0, 0, -1);
            case RIGHT:
                return new Vector3Int(1, 0, 0);
            case LEFT:
                return new Vector3Int(-1, 0, 0);
            default:
                throw new System.ArgumentException("Not a valid direction");
        }
    }

    public static int FromVector (Vector3Int vector)
    {
        if (vector.x == 1)
        {
            return Direction.RIGHT;
        }
        else if (vector.x == -1)
        {
            return Direction.LEFT;
        }
        else if (vector.y == 1)
        {
            return Direction.UP;
        }
        else if (vector.y == -1)
        {
            return Direction.DOWN;
        }
        else if (vector.z == 1)
        {
            return Direction.FRONT;
        }
        else if(vector.z == -1)
        {
            return Direction.BACK;
        }

        throw new System.ArgumentException($"Invalid argument {vector}");
    }

    /// <summary>
    /// E.g. for the top face, what is forward? (transform.up)
    /// </summary>
    /// <param name="dir">Direction of the face</param>
    public static Vector3 Forward(int dir, Transform transform)
    {
        int reflectX = transform.lossyScale.x < 0 ? -1 : 1;
        int reflectY = transform.lossyScale.y < 0 ? -1 : 1;
        int reflectZ = transform.lossyScale.z < 0 ? -1 : 1;
        switch (dir)
        {
            case UP:
                return transform.up * reflectY;
            case DOWN:
                return -transform.up * reflectY;
            case FRONT:
                return transform.forward * reflectZ;
            case BACK:
                return -transform.forward * reflectZ;
            case RIGHT:
                return transform.right * reflectX;
            case LEFT:
                return -transform.right * reflectX;
            default:
                return Vector3.zero;
        }
    }

    public static float ForwardLength(int dir, Vector3 size)
    {
        switch (dir)
        {
            case UP:
            case DOWN:
                return size.y;
            case FRONT:
            case BACK:
                return size.z;
            case RIGHT:
            case LEFT:
                return size.x;
            default:
                return 0;
        }
    }

    public static Vector3 Up(int dir, Transform transform)
    {
        int reflectY = transform.lossyScale.y < 0 ? -1 : 1;
        int reflectZ = transform.lossyScale.z < 0 ? -1 : 1;
        switch (dir)
        {
            case UP:
                return -transform.forward * reflectZ;
            case DOWN:
                return -transform.forward * reflectZ;
            case FRONT:
                return transform.up * reflectY;
            case BACK:
                return transform.up * reflectY;
            case RIGHT:
                return transform.up * reflectY;
            case LEFT:
                return transform.up * reflectY;
            default:
                return Vector3.zero;
        }
    }

    public static float UpLength(int dir, Vector3 size)
    {
        switch (dir)
        {
            case UP:
            case DOWN:
                return size.z;
            case FRONT:
            case BACK:
                return size.y;
            case RIGHT:
            case LEFT:
                return size.y;
            default:
                return 0;
        }
    }

    public static Vector3 Right(int dir, Transform transform)
    {
        int reflectX = transform.lossyScale.x < 0 ? -1 : 1;
        int reflectZ = transform.lossyScale.z < 0 ? -1 : 1;
        switch (dir)
        {
            case UP:
                return transform.right * reflectX;
            case DOWN:
                return transform.right * reflectX;
            case FRONT:
                return transform.right * reflectX;
            case BACK:
                return transform.right * reflectX;
            case RIGHT:
                return -transform.forward * reflectZ;
            case LEFT:
                return -transform.forward * reflectZ;
            default:
                return Vector3.zero;
        }
    }

    public static float RightLength(int dir, Vector3 size)
    {
        switch (dir)
        {
            case UP:
            case DOWN:
                return size.x;
            case FRONT:
            case BACK:
                return size.x;
            case RIGHT:
            case LEFT:
                return size.z;
            default:
                return 0;
        }
    }

    public static int TransformDirection(int dir, ERotator rotator, EReflector reflector)
    {
        int result = dir;

        switch (rotator)
        {
            case ERotator.x90:
                switch (result)
                {
                    case UP:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = BACK;
                        break;
                    case BACK:
                        result = UP;
                        break;
                    case FRONT:
                        result = DOWN;
                        break;
                }
                break;
            case ERotator.x180:
                switch (result)
                {
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                }
                break;
            case ERotator.x270:
                switch (result)
                {
                    case DOWN:
                        result = FRONT;
                        break;
                    case UP:
                        result = BACK;
                        break;
                    case FRONT:
                        result = UP;
                        break;
                    case BACK:
                        result = DOWN;
                        break;
                }
                break;
            case ERotator.y90:
                switch (result)
                {
                    case LEFT:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = BACK;
                        break;
                    case FRONT:
                        result = RIGHT;
                        break;
                    case BACK:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.y180:
                switch (result)
                {
                    case BACK:
                        result = FRONT;
                        break;
                    case FRONT:
                        result = BACK;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.y270:
                switch (result)
                {
                    case RIGHT:
                        result = FRONT;
                        break;
                    case LEFT:
                        result = BACK;
                        break;
                    case BACK:
                        result = RIGHT;
                        break;
                    case FRONT:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.z90:
                switch (result)
                {
                    case RIGHT:
                        result = UP;
                        break;
                    case LEFT:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = RIGHT;
                        break;
                    case UP:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.z180:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.z270:
                switch (result)
                {
                    case LEFT:
                        result = UP;
                        break;
                    case RIGHT:
                        result = DOWN;
                        break;
                    case UP:
                        result = RIGHT;
                        break;
                    case DOWN:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.x90y90:
                switch (result)
                {
                    case BACK:
                        result = UP;
                        break;
                    case FRONT:
                        result = DOWN;
                        break;
                    case LEFT:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = BACK;
                        break;
                    case UP:
                        result = RIGHT;
                        break;
                    case DOWN:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.x90y180:
                switch (result)
                {
                    case BACK:
                        result = UP;
                        break;
                    case FRONT:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = FRONT;
                        break;
                    case UP:
                        result = BACK;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.x90y270:
                switch (result)
                {
                    case BACK:
                        result = UP;
                        break;
                    case FRONT:
                        result = DOWN;
                        break;
                    case RIGHT:
                        result = FRONT;
                        break;
                    case LEFT:
                        result = BACK;
                        break;
                    case DOWN:
                        result = RIGHT;
                        break;
                    case UP:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.x180y90:
                switch (result)
                {
                    case DOWN:
                        result = UP;
                        break;
                    case UP:
                        result = DOWN;
                        break;
                    case LEFT:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = BACK;
                        break;
                    case BACK:
                        result = RIGHT;
                        break;
                    case FRONT:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.x180y180:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.x180y270:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case RIGHT:
                        result = FRONT;
                        break;
                    case LEFT:
                        result = BACK;
                        break;
                    case FRONT:
                        result = RIGHT;
                        break;
                    case BACK:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.x270y90:
                switch (result)
                {
                    case FRONT:
                        result = UP;
                        break;
                    case BACK:
                        result = DOWN;
                        break;
                    case LEFT:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = BACK;
                        break;
                    case DOWN:
                        result = RIGHT;
                        break;
                    case UP:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.x270y180:
                switch (result)
                {
                    case FRONT:
                        result = UP;
                        break;
                    case BACK:
                        result = DOWN;
                        break;
                    case UP:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = BACK;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.x270y270:
                switch (result)
                {
                    case FRONT:
                        result = UP;
                        break;
                    case BACK:
                        result = DOWN;
                        break;
                    case RIGHT:
                        result = FRONT;
                        break;
                    case LEFT:
                        result = BACK;
                        break;
                    case UP:
                        result = RIGHT;
                        break;
                    case DOWN:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.x90z90:
                switch (result)
                {
                    case BACK:
                        result = UP;
                        break;
                    case FRONT:
                        result = DOWN;
                        break;
                    case RIGHT:
                        result = FRONT;
                        break;
                    case LEFT:
                        result = BACK;
                        break;
                    case DOWN:
                        result = RIGHT;
                        break;
                    case UP:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.x90z180:
                switch (result)
                {
                    case BACK:
                        result = UP;
                        break;
                    case FRONT:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = FRONT;
                        break;
                    case UP:
                        result = BACK;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.x90z270:
                switch (result)
                {
                    case BACK:
                        result = UP;
                        break;
                    case FRONT:
                        result = DOWN;
                        break;
                    case LEFT:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = BACK;
                        break;
                    case UP:
                        result = RIGHT;
                        break;
                    case DOWN:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.x180z90:
                switch (result)
                {
                    case LEFT:
                        result = UP;
                        break;
                    case RIGHT:
                        result = DOWN;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case FRONT:
                        result = BACK;
                        break;
                    case DOWN:
                        result = RIGHT;
                        break;
                    case UP:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.x180z180:
                switch (result)
                {
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.x180z270:
                switch (result)
                {
                    case RIGHT:
                        result = UP;
                        break;
                    case LEFT:
                        result = DOWN;
                        break;
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case UP:
                        result = RIGHT;
                        break;
                    case DOWN:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.x270z90:
                switch (result)
                {
                    case FRONT:
                        result = UP;
                        break;
                    case BACK:
                        result = DOWN;
                        break;
                    case LEFT:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = BACK;
                        break;
                    case DOWN:
                        result = RIGHT;
                        break;
                    case UP:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.x270z180:
                switch (result)
                {
                    case UP:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = BACK;
                        break;
                    case FRONT:
                        result = UP;
                        break;
                    case BACK:
                        result = DOWN;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.x270z270:
                switch (result)
                {
                    case FRONT:
                        result = UP;
                        break;
                    case BACK:
                        result = DOWN;
                        break;
                    case RIGHT:
                        result = FRONT;
                        break;
                    case LEFT:
                        result = BACK;
                        break;
                    case UP:
                        result = RIGHT;
                        break;
                    case DOWN:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.y90z90:
                switch (result)
                {
                    case RIGHT:
                        result = UP;
                        break;
                    case LEFT:
                        result = DOWN;
                        break;
                    case UP:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = BACK;
                        break;
                    case FRONT:
                        result = RIGHT;
                        break;
                    case BACK:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.y90z180:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case RIGHT:
                        result = FRONT;
                        break;
                    case LEFT:
                        result = BACK;
                        break;
                    case FRONT:
                        result = RIGHT;
                        break;
                    case BACK:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.y90z270:
                switch (result)
                {
                    case LEFT:
                        result = UP;
                        break;
                    case RIGHT:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = FRONT;
                        break;
                    case UP:
                        result = BACK;
                        break;
                    case FRONT:
                        result = RIGHT;
                        break;
                    case BACK:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.y180z90:
                switch (result)
                {
                    case RIGHT:
                        result = UP;
                        break;
                    case LEFT:
                        result = DOWN;
                        break;
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case UP:
                        result = RIGHT;
                        break;
                    case DOWN:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.y180z180:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                }
                break;
            case ERotator.y180z270:
                switch (result)
                {
                    case LEFT:
                        result = UP;
                        break;
                    case RIGHT:
                        result = DOWN;
                        break;
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = RIGHT;
                        break;
                    case UP:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.y270z90:
                switch (result)
                {
                    case RIGHT:
                        result = UP;
                        break;
                    case LEFT:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = FRONT;
                        break;
                    case UP:
                        result = BACK;
                        break;
                    case BACK:
                        result = RIGHT;
                        break;
                    case FRONT:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.y270z180:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case LEFT:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = BACK;
                        break;
                    case BACK:
                        result = RIGHT;
                        break;
                    case FRONT:
                        result = LEFT;
                        break;
                }
                break;
            case ERotator.y270z270:
                switch (result)
                {
                    case LEFT:
                        result = UP;
                        break;
                    case RIGHT:
                        result = DOWN;
                        break;
                    case UP:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = BACK;
                        break;
                    case BACK:
                        result = RIGHT;
                        break;
                    case FRONT:
                        result = LEFT;
                        break;
                }
                break;
        }

        switch (reflector)
        {
            case EReflector.NegX:
                if (dir == RIGHT || dir == LEFT)
                    result = Opposite(dir);
                break;
            case EReflector.NegY:
                if (dir == UP || dir == DOWN)
                    result = Opposite(dir);
                break;
            case EReflector.NegZ:
                if (dir == FRONT || dir == BACK)
                    result = Opposite(dir);
                break;
            case EReflector.NegXY:
                if (dir != FRONT && dir != BACK)
                    result = Opposite(dir);
                break;
            case EReflector.NegXZ:
                if (dir != UP && dir != DOWN)
                    result = Opposite(dir);
                break;
            case EReflector.NegYZ:
                if (dir != RIGHT && dir != LEFT)
                    result = Opposite(dir);
                break;
            case EReflector.NegXYZ:
                result = Opposite(dir);
                break;
        }

        return result;
    }

    /// <summary>
    /// Which direction was it before applying rotations/scaling
    /// </summary>
    public static int InverseTransformDirection(int dir, ERotator rotator, EReflector reflector)
    {
        int result = dir;

        switch (rotator)
        {
            case ERotator.x90:
                switch (result)
                {
                    case FRONT:
                        result = UP;
                        break;
                    case BACK:
                        result = DOWN;
                        break;
                    case UP:
                        result = BACK;
                        break;
                    case DOWN:
                        result = FRONT;
                        break;
                }
                break;
            case ERotator.x180:
                switch (result)
                {
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                }
                break;
            case ERotator.x270:
                switch (result)
                {
                    case FRONT:
                        result = DOWN;
                        break;
                    case BACK:
                        result = UP;
                        break;
                    case UP:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = BACK;
                        break;
                }
                break;
            case ERotator.y90:
                switch (result)
                {
                    case FRONT:
                        result = LEFT;
                        break;
                    case BACK:
                        result = RIGHT;
                        break;
                    case RIGHT:
                        result = FRONT;
                        break;
                    case LEFT:
                        result = BACK;
                        break;
                }
                break;
            case ERotator.y180:
                switch (result)
                {
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.y270:
                switch (result)
                {
                    case FRONT:
                        result = RIGHT;
                        break;
                    case BACK:
                        result = LEFT;
                        break;
                    case RIGHT:
                        result = BACK;
                        break;
                    case LEFT:
                        result = FRONT;
                        break;
                }
                break;
            case ERotator.z90:
                switch (result)
                {
                    case UP:
                        result = RIGHT;
                        break;
                    case DOWN:
                        result = LEFT;
                        break;
                    case RIGHT:
                        result = DOWN;
                        break;
                    case LEFT:
                        result = UP;
                        break;
                }
                break;
            case ERotator.z180:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.z270:
                switch (result)
                {
                    case UP:
                        result = LEFT;
                        break;
                    case DOWN:
                        result = RIGHT;
                        break;
                    case RIGHT:
                        result = UP;
                        break;
                    case LEFT:
                        result = DOWN;
                        break;
                }
                break;
            case ERotator.x90y90:
                switch (result)
                {
                    case UP:
                        result = BACK;
                        break;
                    case DOWN:
                        result = FRONT;
                        break;
                    case FRONT:
                        result = LEFT;
                        break;
                    case BACK:
                        result = RIGHT;
                        break;
                    case RIGHT:
                        result = UP;
                        break;
                    case LEFT:
                        result = DOWN;
                        break;
                }
                break;
            case ERotator.x90y180:
                switch (result)
                {
                    case UP:
                        result = BACK;
                        break;
                    case DOWN:
                        result = FRONT;
                        break;
                    case FRONT:
                        result = DOWN;
                        break;
                    case BACK:
                        result = UP;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.x90y270:
                switch (result)
                {
                    case UP:
                        result = BACK;
                        break;
                    case DOWN:
                        result = FRONT;
                        break;
                    case FRONT:
                        result = RIGHT;
                        break;
                    case BACK:
                        result = LEFT;
                        break;
                    case RIGHT:
                        result = DOWN;
                        break;
                    case LEFT:
                        result = UP;
                        break;
                }
                break;
            case ERotator.x180y90:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case FRONT:
                        result = LEFT;
                        break;
                    case BACK:
                        result = RIGHT;
                        break;
                    case RIGHT:
                        result = BACK;
                        break;
                    case LEFT:
                        result = FRONT;
                        break;
                }
                break;
            case ERotator.x180y180:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.x180y270:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case FRONT:
                        result = RIGHT;
                        break;
                    case BACK:
                        result = LEFT;
                        break;
                    case RIGHT:
                        result = FRONT;
                        break;
                    case LEFT:
                        result = BACK;
                        break;
                }
                break;
            case ERotator.x270y90:
                switch (result)
                {
                    case UP:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = BACK;
                        break;
                    case FRONT:
                        result = LEFT;
                        break;
                    case BACK:
                        result = RIGHT;
                        break;
                    case RIGHT:
                        result = DOWN;
                        break;
                    case LEFT:
                        result = UP;
                        break;
                }
                break;
            case ERotator.x270y180:
                switch (result)
                {
                    case UP:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = BACK;
                        break;
                    case FRONT:
                        result = UP;
                        break;
                    case BACK:
                        result = DOWN;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.x270y270:
                switch (result)
                {
                    case UP:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = BACK;
                        break;
                    case FRONT:
                        result = RIGHT;
                        break;
                    case BACK:
                        result = LEFT;
                        break;
                    case RIGHT:
                        result = UP;
                        break;
                    case LEFT:
                        result = DOWN;
                        break;
                }
                break;
            case ERotator.x90z90:
                switch (result)
                {
                    case UP:
                        result = BACK;
                        break;
                    case DOWN:
                        result = FRONT;
                        break;
                    case FRONT:
                        result = RIGHT;
                        break;
                    case BACK:
                        result = LEFT;
                        break;
                    case RIGHT:
                        result = DOWN;
                        break;
                    case LEFT:
                        result = UP;
                        break;
                }
                break;
            case ERotator.x90z180:
                switch (result)
                {
                    case UP:
                        result = BACK;
                        break;
                    case DOWN:
                        result = FRONT;
                        break;
                    case FRONT:
                        result = DOWN;
                        break;
                    case BACK:
                        result = UP;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.x90z270:
                switch (result)
                {
                    case UP:
                        result = BACK;
                        break;
                    case DOWN:
                        result = FRONT;
                        break;
                    case FRONT:
                        result = LEFT;
                        break;
                    case BACK:
                        result = RIGHT;
                        break;
                    case RIGHT:
                        result = UP;
                        break;
                    case LEFT:
                        result = DOWN;
                        break;
                }
                break;
            case ERotator.x180z90:
                switch (result)
                {
                    case UP:
                        result = LEFT;
                        break;
                    case DOWN:
                        result = RIGHT;
                        break;
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = DOWN;
                        break;
                    case LEFT:
                        result = UP;
                        break;
                }
                break;
            case ERotator.x180z180:
                switch (result)
                {
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.x180z270:
                switch (result)
                {
                    case UP:
                        result = RIGHT;
                        break;
                    case DOWN:
                        result = LEFT;
                        break;
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = UP;
                        break;
                    case LEFT:
                        result = DOWN;
                        break;
                }
                break;
            case ERotator.x270z90:
                switch (result)
                {
                    case UP:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = BACK;
                        break;
                    case FRONT:
                        result = LEFT;
                        break;
                    case BACK:
                        result = RIGHT;
                        break;
                    case RIGHT:
                        result = DOWN;
                        break;
                    case LEFT:
                        result = UP;
                        break;
                }
                break;
            case ERotator.x270z180:
                switch (result)
                {
                    case UP:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = BACK;
                        break;
                    case FRONT:
                        result = UP;
                        break;
                    case BACK:
                        result = DOWN;
                        break;
                    case RIGHT:
                        result = LEFT;
                        break;
                    case LEFT:
                        result = RIGHT;
                        break;
                }
                break;
            case ERotator.x270z270:
                switch (result)
                {
                    case UP:
                        result = FRONT;
                        break;
                    case DOWN:
                        result = BACK;
                        break;
                    case FRONT:
                        result = RIGHT;
                        break;
                    case BACK:
                        result = LEFT;
                        break;
                    case RIGHT:
                        result = UP;
                        break;
                    case LEFT:
                        result = DOWN;
                        break;
                }
                break;
            case ERotator.y90z90:
                switch (result)
                {
                    case UP:
                        result = RIGHT;
                        break;
                    case DOWN:
                        result = LEFT;
                        break;
                    case FRONT:
                        result = UP;
                        break;
                    case BACK:
                        result = DOWN;
                        break;
                    case RIGHT:
                        result = FRONT;
                        break;
                    case LEFT:
                        result = BACK;
                        break;
                }
                break;
            case ERotator.y90z180:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case FRONT:
                        result = RIGHT;
                        break;
                    case BACK:
                        result = LEFT;
                        break;
                    case RIGHT:
                        result = FRONT;
                        break;
                    case LEFT:
                        result = BACK;
                        break;
                }
                break;
            case ERotator.y90z270:
                switch (result)
                {
                    case UP:
                        result = LEFT;
                        break;
                    case DOWN:
                        result = RIGHT;
                        break;
                    case FRONT:
                        result = DOWN;
                        break;
                    case BACK:
                        result = UP;
                        break;
                    case RIGHT:
                        result = FRONT;
                        break;
                    case LEFT:
                        result = BACK;
                        break;
                }
                break;
            case ERotator.y180z90:
                switch (result)
                {
                    case UP:
                        result = RIGHT;
                        break;
                    case DOWN:
                        result = LEFT;
                        break;
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = UP;
                        break;
                    case LEFT:
                        result = DOWN;
                        break;
                }
                break;
            case ERotator.y180z180:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                }
                break;
            case ERotator.y180z270:
                switch (result)
                {
                    case UP:
                        result = LEFT;
                        break;
                    case DOWN:
                        result = RIGHT;
                        break;
                    case FRONT:
                        result = BACK;
                        break;
                    case BACK:
                        result = FRONT;
                        break;
                    case RIGHT:
                        result = DOWN;
                        break;
                    case LEFT:
                        result = UP;
                        break;
                }
                break;
            case ERotator.y270z90:
                switch (result)
                {
                    case UP:
                        result = RIGHT;
                        break;
                    case DOWN:
                        result = LEFT;
                        break;
                    case FRONT:
                        result = DOWN;
                        break;
                    case BACK:
                        result = UP;
                        break;
                    case RIGHT:
                        result = BACK;
                        break;
                    case LEFT:
                        result = FRONT;
                        break;
                }
                break;
            case ERotator.y270z180:
                switch (result)
                {
                    case UP:
                        result = DOWN;
                        break;
                    case DOWN:
                        result = UP;
                        break;
                    case FRONT:
                        result = LEFT;
                        break;
                    case BACK:
                        result = RIGHT;
                        break;
                    case RIGHT:
                        result = BACK;
                        break;
                    case LEFT:
                        result = FRONT;
                        break;
                }
                break;
            case ERotator.y270z270:
                switch (result)
                {
                    case UP:
                        result = LEFT;
                        break;
                    case DOWN:
                        result = RIGHT;
                        break;
                    case FRONT:
                        result = UP;
                        break;
                    case BACK:
                        result = DOWN;
                        break;
                    case RIGHT:
                        result = BACK;
                        break;
                    case LEFT:
                        result = FRONT;
                        break;
                }
                break;
        }

        switch (reflector)
        {
            case EReflector.NegX:
                if (result == RIGHT || result == LEFT)
                    result = Opposite(result);
                break;
            case EReflector.NegY:
                if (result == UP || result == DOWN)
                    result = Opposite(result);
                break;
            case EReflector.NegZ:
                if (result == FRONT || result == BACK)
                    result = Opposite(result);
                break;
            case EReflector.NegXY:
                if (result != FRONT && result != BACK)
                    result = Opposite(result);
                break;
            case EReflector.NegXZ:
                if (result != UP && result != DOWN)
                    result = Opposite(result);
                break;
            case EReflector.NegYZ:
                if (result != RIGHT && result != LEFT)
                    result = Opposite(result);
                break;
            case EReflector.NegXYZ:
                result = Opposite(result);
                break;
        }

        return result;
    }

    public static ERotator FromToDirection(int inDirection, int outDirection)
    {
        switch (inDirection)
        {
            case Direction.UP:
                switch (outDirection)
                {
                    case Direction.UP:
                        return ERotator.None;
                    case Direction.DOWN:
                        return ERotator.z180;
                    case Direction.RIGHT:
                        return ERotator.z270;
                    case Direction.LEFT:
                        return ERotator.z90;
                    case Direction.FRONT:
                        return ERotator.x90;
                    case Direction.BACK:
                        return ERotator.x270;
                }
                break;
            case Direction.DOWN:
                switch (outDirection)
                {
                    case Direction.UP:
                        return ERotator.z180;
                    case Direction.DOWN:
                        return ERotator.None;
                    case Direction.RIGHT:
                        return ERotator.z90;
                    case Direction.LEFT:
                        return ERotator.z270;
                    case Direction.FRONT:
                        return ERotator.x270;
                    case Direction.BACK:
                        return ERotator.x90;
                }
                break;
            case Direction.RIGHT:
                switch (outDirection)
                {
                    case Direction.UP:
                        return ERotator.z90;
                    case Direction.DOWN:
                        return ERotator.z270;
                    case Direction.RIGHT:
                        return ERotator.None;
                    case Direction.LEFT:
                        return ERotator.y180;
                    case Direction.FRONT:
                        return ERotator.y270;
                    case Direction.BACK:
                        return ERotator.y90;
                }
                break;
            case Direction.LEFT:
                switch (outDirection)
                {
                    case Direction.UP:
                        return ERotator.z270;
                    case Direction.DOWN:
                        return ERotator.z90;
                    case Direction.RIGHT:
                        return ERotator.y180;
                    case Direction.LEFT:
                        return ERotator.None;
                    case Direction.FRONT:
                        return ERotator.y90;
                    case Direction.BACK:
                        return ERotator.y270;
                }
                break;
            case Direction.FRONT:
                switch (outDirection)
                {
                    case Direction.UP:
                        return ERotator.x270;
                    case Direction.DOWN:
                        return ERotator.x90;
                    case Direction.RIGHT:
                        return ERotator.y90;
                    case Direction.LEFT:
                        return ERotator.y270;
                    case Direction.FRONT:
                        return ERotator.None;
                    case Direction.BACK:
                        return ERotator.y180;
                }
                break;
            case Direction.BACK:
                switch (outDirection)
                {
                    case Direction.UP:
                        return ERotator.x90;
                    case Direction.DOWN:
                        return ERotator.x270;
                    case Direction.RIGHT:
                        return ERotator.y270;
                    case Direction.LEFT:
                        return ERotator.y90;
                    case Direction.FRONT:
                        return ERotator.y180;
                    case Direction.BACK:
                        return ERotator.None;
                }
                break;
        }

        throw new System.ArgumentException("Invalid outDirection");
    }
}
