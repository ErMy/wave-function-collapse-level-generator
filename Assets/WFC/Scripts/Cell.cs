using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;

namespace WaveFunctionCollapse
{
    public class Cell
    {
        public HashSet<int> candidates;
        public bool isCollapsed;
        /// <summary>
        /// [tileIndex][direction] = enablercount
        /// </summary>
        public int[][] tileEnablerCounts;
        float entropy;
        float totalWeight;
        float weightLogWeight;

        public Cell(HashSet<int> candidates, int[][] tileEnablerCounts, float weightSum, float logWeights)
        {
            this.candidates = new HashSet<int>(candidates);
            this.tileEnablerCounts = CopyArray(tileEnablerCounts);
            
            totalWeight = weightSum;
            weightLogWeight = logWeights;
            
            entropy = Mathf.Log(totalWeight, 2f) - weightLogWeight / totalWeight;
        }

        public Cell(Cell original)
        {
            this.candidates = new HashSet<int>(original.candidates);
            this.isCollapsed = original.isCollapsed;
            this.tileEnablerCounts = CopyArray(original.tileEnablerCounts);
            this.entropy = original.entropy;
            this.totalWeight = original.totalWeight;
            this.weightLogWeight = original.weightLogWeight;
        }

        private int[][] CopyArray(int[][] source)
        {
            int len = source.Length;
            int[][] dest = new int[len][];

            for (int i = 0; i < len; i++)
            {
                int[] inner = source[i];
                int innerLen = inner.Length;
                int[] newer = new int[innerLen];
                Array.Copy(inner, newer, innerLen);
                dest[i] = newer;
            }

            return dest;
        }

        public float UpdateEntropyAndWeightCache(float weight)
        {
            totalWeight -= weight;
            weightLogWeight -= weight * Mathf.Log(weight, 2f);
            entropy = Mathf.Log(totalWeight, 2f) - weightLogWeight / totalWeight;

            return entropy;
        }

        public float AddNoiseToEntropy(float noise)
        {
            entropy += noise;
            return entropy;
        }

        public float GetEntropy() => entropy;

        public float GetTotalWeight() => totalWeight;
    }
}