using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace WaveFunctionCollapse
{
    public struct MapTile
    {
        public int segmentIndex;
        public Vector3Int position;
        public int[][] faceIDs;
        public int[][] pathValues;
    }

    public struct MapSegment
    {
        /// <summary>
        /// [localPos][tileIndex]
        /// </summary>
        public Dictionary<Vector3Int, int> posToTile;
        public float weight;
        public bool spawn;
        public bool isRamp;
        public bool onlyOnPath;
        public bool onlyOnNonPath;
        public GameObject spawnableObject;
        public HashSet<GameObject> invalidNeighbors;
        public Vector3 origin;
        public ERotator rotator;
        public EReflector reflector;
    }

    public static class TileBuilder
    {
        public static void BuildTilesAndSegments(in GameObject prototypesParent, bool useBorderRule, in TilePrototype borderPrototype, 
            out MapTile borderRuleTile, out MapSegment[] segmentsArr, out MapTile[] tilesArr)
        {
            List<MapSegment> segments = new List<MapSegment>();
            List<MapTile> tiles = new List<MapTile>();

            TilePrototype[] prototypes = prototypesParent.GetComponentsInChildren<TilePrototype>();

            // Loop through all prototypes and create segments with different rotators & reflectors

            for (int i = 0, segmentIndex = 0; i < prototypes.Length; i++)
            {
                if (prototypes[i].weight <= 0) continue;

                int prototypeIndex = segmentIndex;

                foreach (ERotator rotator in Enum.GetValues(typeof(ERotator)))
                {
                    foreach (EReflector reflector in Enum.GetValues(typeof(EReflector)))
                    {
                        MapSegment segment = new MapSegment
                        {
                            weight = prototypes[i].weight,
                            spawn = prototypes[i].spawn,
                            isRamp = prototypes[i].isRamp,
                            onlyOnPath = prototypes[i].onlyOnPath,
                            onlyOnNonPath = prototypes[i].onlyOnNonPath,
                            spawnableObject = prototypes[i].gameObject,
                            invalidNeighbors = new HashSet<GameObject>(prototypes[i].invalidNeighbors),
                            origin = prototypes[i].origin,
                            rotator = rotator,
                            reflector = reflector,
                            posToTile = new Dictionary<Vector3Int, int>()
                        };

                        // Create tiles and add them to their segment
                        BuildTilesFromSegment(segmentIndex, prototypes[i], tiles.Count, ref segment, out List<MapTile> newTiles);

                        if (IsUnique(segment, newTiles, segments, tiles, prototypeIndex))
                        {
                            segments.Add(segment);
                            tiles.AddRange(newTiles);
                            segmentIndex++;
                        }
                    }
                }
            }

            segmentsArr = segments.ToArray();
            tilesArr = tiles.ToArray();

            // Create tile to use for border rule
            borderRuleTile = new MapTile();
            if (useBorderRule)
            {
                borderRuleTile.faceIDs = new int[6][];
                for (int dir = 0; dir < 6; dir++)
                {
                    borderRuleTile.faceIDs[dir] = GetAdjustedFaceIDs(dir, ERotator.None, EReflector.None, borderPrototype.savedTiles[0]);
                }
            }
        }

        private static bool IsUnique(in MapSegment segment, in List<MapTile> newTiles, in List<MapSegment> segments, in List<MapTile> tiles, int prototypeIndex)
        {
            // Compare new segment with every variation added so far
            for (int i = prototypeIndex; i < segments.Count; i++)
            {
                MapSegment compareSegment = segments[i];
                var newPosToTile = segment.posToTile;
                bool isUnique = false;

                // compare every tile between the segments
                foreach (var posToTile in compareSegment.posToTile)
                {
                    // If the new segment has a tile at the same position
                    if (newPosToTile.TryGetValue(posToTile.Key, out int newTileIndex))
                    {
                        MapTile newTile = newTiles[newTileIndex - tiles.Count];
                        MapTile compareTile = tiles[posToTile.Value];
                        for (int dir = 0; dir < 6 && !isUnique; dir++)
                        {
                            for (int faceindex = 0; faceindex < 4; faceindex++)
                            {
                                if (newTile.faceIDs[dir][faceindex] != compareTile.faceIDs[dir][faceindex])
                                {
                                    isUnique = true;
                                    break;
                                }
                            }
                        }
                        
                    } else
                    {
                        isUnique = true;
                        break;
                    }
                }

                if (!isUnique)
                {
                    return false;
                }
            }

            // first segment of its prototype
            return true;
        }

        private static IEnumerable<MapTile> BuildTilesFromSegment(int segmentIndex, in TilePrototype prototype, 
            int currentCount, ref MapSegment segment, out List<MapTile> newTiles)
        {
            newTiles = new List<MapTile>();

            //Debug.Log($"Rotator: {segment.rotator}, Reflector: {segment.reflector}");
            for (int i = 0; i < prototype.savedTiles.Count; i++)
            {
                //Debug.Log($"Position: {Transformer.TransformPosition(prototype.savedTiles[i].position, segment.rotator, segment.reflector)}");
                MapTile tile = new MapTile
                {
                    position = TransformHelper.TransformPosition(prototype.savedTiles[i].position, segment.rotator, segment.reflector),
                    segmentIndex = segmentIndex
                };

                // Copy faceid information over
                tile.faceIDs = new int[6][];
                tile.pathValues = new int[6][];
                for (int dir = 0; dir < 6; dir++)
                {
                    tile.faceIDs[dir] = GetAdjustedFaceIDs(dir, segment.rotator, segment.reflector, prototype.savedTiles[i]);
                    tile.pathValues[dir] = GetAdjustedPathValues(dir, segment.rotator, segment.reflector, prototype.savedTiles[i]);
                }

                newTiles.Add(tile);
                segment.posToTile.Add(tile.position, currentCount++);
            }

            return newTiles;
        }

        private static int[] GetAdjustedPathValues(int dir, ERotator rotator, EReflector reflector, SavedTile savedTile)
        {
            if (rotator == ERotator.None && reflector == EReflector.None)
            {
                return (int[])savedTile.GetPathValues(dir).Clone();
            }

            int adjustedDir = Direction.InverseTransformDirection(dir, rotator, reflector);

            int[] adjustedPathValues = new int[6];
            for (int i = 0; i < 6; i++)
            {
                int exitDir = Direction.InverseTransformDirection(i, rotator, reflector);
                adjustedPathValues[i] = savedTile.GetPathValues(adjustedDir)[exitDir];
            }

            return adjustedPathValues;
            //return (int[])savedTile.GetPathValues(adjustedDir).Clone();
        }

        private static int[] GetAdjustedFaceIDs(int dir, ERotator rotator, EReflector reflector, in SavedTile savedTile)
        {
            if (rotator == ERotator.None && reflector == EReflector.None)
            {
                return (int[])savedTile.GetFace(dir).Clone();
            }

            int adjustedDir = Direction.InverseTransformDirection(dir, rotator, reflector);

            return ReorderfaceIDs(savedTile.GetFace(adjustedDir), savedTile.isSplit[adjustedDir], dir, rotator, reflector);
        }

        private static int[] ReorderfaceIDs(in int[] faceIDs, bool isSplit, int dir, ERotator rotator, EReflector reflector)
        {
            int[] newfaceIDs = (int[])faceIDs.Clone();

            if (!isSplit)
            {
                return newfaceIDs;
            }
            
            switch (rotator)
            {
                case ERotator.x90:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                    }
                    break;
                case ERotator.x180:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                    }
                    break;
                case ERotator.x270:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                    }
                    break;
                case ERotator.y90:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                    }
                    break;
                case ERotator.y180:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                    }
                    break;
                case ERotator.y270:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                    }
                    break;
                case ERotator.z90:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                    }
                    break;
                case ERotator.z180:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                    }
                    break;
                case ERotator.z270:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                    }
                    break;
                case ERotator.x90y90:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                    }
                    break;
                case ERotator.x90y180:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                    }
                    break;
                case ERotator.x90y270:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                    }
                    break;
                case ERotator.x180y90:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                    }
                    break;
                case ERotator.x180y180:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                    }
                    break;
                case ERotator.x180y270:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                    }
                    break;
                case ERotator.x270y90:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                    }
                    break;
                case ERotator.x270y180:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                    }
                    break;
                case ERotator.x270y270:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                    }
                    break;
                case ERotator.x90z90:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                    }
                    break;
                case ERotator.x90z180:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                    }
                    break;
                case ERotator.x90z270:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                    }
                    break;
                case ERotator.x180z90:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                    }
                    break;
                case ERotator.x180z180:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                    }
                    break;
                case ERotator.x180z270:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                    }
                    break;
                case ERotator.x270z90:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                    }
                    break;
                case ERotator.x270z180:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                    }
                    break;
                case ERotator.x270z270:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                    }
                    break;
                case ERotator.y90z90:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                    }
                    break;
                case ERotator.y90z180:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                    }
                    break;
                case ERotator.y90z270:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                    }
                    break;
                case ERotator.y180z90:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[3], newfaceIDs[0], newfaceIDs[2] };
                            break;
                    }
                    break;
                case ERotator.y180z180:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                    }
                    break;
                case ERotator.y180z270:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                    }
                    break;
                case ERotator.y270z90:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                    }
                    break;
                case ERotator.y270z180:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[1], newfaceIDs[2], newfaceIDs[0] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                    }
                    break;
                case ERotator.y270z270:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[0], newfaceIDs[3], newfaceIDs[1] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[2], newfaceIDs[1], newfaceIDs[3] };
                            break;
                    }
                    break;
            }

            reflector = reflector.RotateReflector(rotator);

            switch (reflector)
            {
                case EReflector.NegX:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                    }
                    break;
                case EReflector.NegY:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                    }
                    break;
                case EReflector.NegZ:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                    }
                    break;
                case EReflector.NegXY:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                    }
                    break;
                case EReflector.NegXZ:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[1], newfaceIDs[0], newfaceIDs[3], newfaceIDs[2] };
                            break;
                    }
                    break;
                case EReflector.NegYZ:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[2], newfaceIDs[3], newfaceIDs[0], newfaceIDs[1] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                    }
                    break;
                case EReflector.NegXYZ:
                    switch (dir)
                    {
                        case Direction.UP:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.DOWN:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.FRONT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.BACK:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.RIGHT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                        case Direction.LEFT:
                            newfaceIDs = new int[] { newfaceIDs[3], newfaceIDs[2], newfaceIDs[1], newfaceIDs[0] };
                            break;
                    }
                    break;
                default:
                    newfaceIDs = new int[] { newfaceIDs[0], newfaceIDs[1], newfaceIDs[2], newfaceIDs[3] };
                    break;
            }

            return newfaceIDs;
        }
    }


}
