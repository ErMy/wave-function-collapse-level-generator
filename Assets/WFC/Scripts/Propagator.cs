using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace WaveFunctionCollapse
{
    public class Propagator
    {
        /// <summary>
        /// Runs multiple tasks to work through all removal and entropy updates.
        /// </summary>
        /// <returns>True if no errors.</returns>
        public static bool Propagate(Map map, bool enqueue = true)
        {
            // loop until this thread has no more removal updates
            while (map.removalUpdates.Count > 0)
            {
                // Get next removal update
                RemovalUpdate removalUpdate = map.removalUpdates.Dequeue();

                // for each direction
                for (int dir = 0; dir < 6; dir++)
                {
                    int oppositeDir = Direction.Opposite(dir);
                    Vector3Int neighborPos = removalUpdate.cellPosition;
                    if (!map.TryGetNeighborInDirection(dir, ref neighborPos, out Cell neighbor) || neighbor.isCollapsed)
                    {
                        continue;
                    }

                    int[] compatibles = map.adjacencyRules.GetCompatibleTiles(removalUpdate.tileIndex, dir);

                    // for every compatible tile in direction
                    for (int i = 0; i < compatibles.Length; i++)
                    {
                        int compatibleTile = compatibles[i];

                        if (!neighbor.candidates.Contains(compatibleTile))
                            continue;

                        // get enabler count for that tile
                        int enablerCount = neighbor.tileEnablerCounts[compatibleTile][oppositeDir];

                        // if the tile being removed is that tile's last enabler
                        if (enablerCount == 1)
                        {
                            // remove that tile and its siblings from candidates
                            var siblings = map.GetSiblings(compatibleTile, neighborPos);

                            for (int r = 0; r < siblings.Count; r++)
                            {
                                if (siblings[r].cell.candidates.Remove(siblings[r].tileIndex))
                                {
                                    if (siblings[r].cell.candidates.Count > 0)
                                    {
                                        map.removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[r].cellPosition, tileIndex = siblings[r].tileIndex });
                                    }
                                    else
                                    {
                                        // if they were their cell's last candidate, return error
                                        return false;
                                    }
                                }
                            }
                        }

                        neighbor.tileEnablerCounts[compatibleTile][oppositeDir] = enablerCount - 1;
                    }
                }

                // update entropy for cell
                Cell updatedCell = map.cellGrid[removalUpdate.cellPosition.x][removalUpdate.cellPosition.y][removalUpdate.cellPosition.z];

                float entropy = updatedCell.UpdateEntropyAndWeightCache(map.mapSegments[map.mapTiles[removalUpdate.tileIndex].segmentIndex].weight);
                if (enqueue)
                {
                    map.cellQueue.Enqueue(removalUpdate.cellPosition, entropy);
                }
            }

            return true;
        }

        internal static void FilterPaths(Map map, IEnumerable<PathPoint> pathPoints, bool filterPathOnlyTiles)
        {
            HashSet<Vector3Int> pathPointSet = new HashSet<Vector3Int>();

            // apply path filter
            foreach (var point in pathPoints)
            {
                Cell cell = map.cellGrid[point.position.x][point.position.y][point.position.z];
                HashSet<int> candidates = new HashSet<int>(cell.candidates);
                foreach (var candidate in candidates)
                {
                    MapTile tile = map.mapTiles[candidate];

                    // Candidate should be ramp but isn't or vice versa
                    if (map.mapSegments[tile.segmentIndex].isRamp != point.isRamp)
                    {
                        var siblings = map.GetSiblings(candidate, point.position);
                        for (int r = 0; r < siblings.Count; r++)
                        {
                            if (siblings[r].cell.candidates.Remove(siblings[r].tileIndex))
                            {
                                map.removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[r].cellPosition, tileIndex = siblings[r].tileIndex });
                            }
                        }
                        continue;
                    }

                    for (int i = 0; i < point.exits.Count; i++)
                    {
                        // Candidate doesn't have an exit where generated path needs an exit
                        if (tile.pathValues[point.upDirection][point.exits[i]] != 1)
                        {
                            var siblings = map.GetSiblings(candidate, point.position);
                            for (int r = 0; r < siblings.Count; r++)
                            {
                                if (siblings[r].cell.candidates.Remove(siblings[r].tileIndex))
                                {
                                    map.removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[r].cellPosition, tileIndex = siblings[r].tileIndex });
                                }
                            }
                            break;
                        }
                    }
                }

                pathPointSet.Add(point.position);
            }

            // filter out tiles that may only spawn on path and nowhere else
            if (filterPathOnlyTiles)
            {
                for (int x = 0; x < map.cellGrid.Length; x++)
                {
                    for (int y = 0; y < map.cellGrid[x].Length; y++)
                    {
                        for (int z = 0; z < map.cellGrid[x][y].Length; z++)
                        {
                            Vector3Int pos = new Vector3Int(x, y, z);
                            // if this point is not on the path, filter out all candidates that can only spawn on path
                            if (!pathPointSet.Contains(pos))
                            {
                                Cell cell = map.cellGrid[x][y][z];
                                HashSet<int> candidates = new HashSet<int>(cell.candidates);
                                foreach (var candidate in candidates)
                                {
                                    MapTile tile = map.mapTiles[candidate];

                                    if (map.mapSegments[tile.segmentIndex].onlyOnPath)
                                    {
                                        var siblings = map.GetSiblings(candidate, pos);
                                        for (int r = 0; r < siblings.Count; r++)
                                        {
                                            if (siblings[r].cell.candidates.Remove(siblings[r].tileIndex))
                                            {
                                                map.removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[r].cellPosition, tileIndex = siblings[r].tileIndex });
                                            }
                                        }
                                    }
                                }
                            } else 
                            {
                                // if it's on the path, filter out candidates that can't spawn on path
                                Cell cell = map.cellGrid[x][y][z];
                                HashSet<int> candidates = new HashSet<int>(cell.candidates);
                                foreach (var candidate in candidates)
                                {
                                    MapTile tile = map.mapTiles[candidate];

                                    if (map.mapSegments[tile.segmentIndex].onlyOnNonPath)
                                    {
                                        var siblings = map.GetSiblings(candidate, pos);
                                        for (int r = 0; r < siblings.Count; r++)
                                        {
                                            if (siblings[r].cell.candidates.Remove(siblings[r].tileIndex))
                                            {
                                                map.removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[r].cellPosition, tileIndex = siblings[r].tileIndex });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Remove segments that don't fit into the grid
        /// </summary>
        /// <param name="map"></param>
        internal static void FilterMisfits(Map map)
        {
            for (int x = 0; x < map.cellGrid.Length; x++)
            {
                for (int y = 0; y < map.cellGrid[0].Length; y++)
                {
                    for (int z = 0; z < map.cellGrid[0][0].Length; z++)
                    {
                        Cell cell = map.cellGrid[x][y][z];
                        HashSet<int> candidates = new HashSet<int>(cell.candidates);
                        foreach (var candidate in candidates)
                        {
                            var siblings = map.GetSiblings(candidate, new Vector3Int(x, y, z));
                            if (siblings.Any(x => x.cell == null))
                            {
                                for (int r = 0; r < siblings.Count; r++)
                                {
                                    if (siblings[r].cell.candidates.Remove(siblings[r].tileIndex))
                                    {
                                        map.removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[r].cellPosition, tileIndex = siblings[r].tileIndex });
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Remove tiles from the border of the grid that don't fit the border rule
        /// </summary>
        public static void FilterBorderCells(Map map)
        {
            // x = 0, left border
            // loop through all tiles on the left border
            for (int y = 0; y < map.cellGrid[0].Length; y++)
            {
                for (int z = 0; z < map.cellGrid[0][y].Length; z++)
                {
                    Cell cell = map.cellGrid[0][y][z];
                    int[] incompatibles = map.adjacencyRules.GetIncompatibleBorderTiles(Direction.LEFT);
                    // loop through all tiles that are not allowed on the left border
                    for (int i = 0; i < incompatibles.Length; i++)
                    {
                        // Remove them from candidates (if found) and enqueue them
                        if (cell.candidates.Contains(incompatibles[i]))
                        {
                            var siblings = map.GetSiblings(incompatibles[i], new Vector3Int(0, y, z));
                            for (int r = 0; r < siblings.Count; r++)
                            {
                                if (siblings[r].cell.candidates.Remove(siblings[r].tileIndex))
                                {
                                    map.removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[r].cellPosition, tileIndex = siblings[r].tileIndex });
                                }
                            }
                        }
                    }
                }
            }

            // x = max, right border
            int bounds = map.cellGrid.GetUpperBound(0);
            for (int y = 0; y < map.cellGrid[bounds].Length; y++)
            {
                for (int z = 0; z < map.cellGrid[bounds][y].Length; z ++)
                {
                    Cell cell = map.cellGrid[bounds][y][z];
                    int[] incompatibles = map.adjacencyRules.GetIncompatibleBorderTiles(Direction.RIGHT);
                    for (int i = 0; i < incompatibles.Length; i++)
                    {
                        if (cell.candidates.Contains(incompatibles[i]))
                        {
                            var siblings = map.GetSiblings(incompatibles[i], new Vector3Int(bounds, y, z));
                            for (int r = 0; r < siblings.Count; r++)
                            {
                                if (siblings[r].cell.candidates.Remove(siblings[r].tileIndex))
                                {
                                    map.removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[r].cellPosition, tileIndex = siblings[r].tileIndex });
                                }
                            }
                        }
                    }
                }
            }

            // y = 0, bottom border
            for (int x = 0; x < map.cellGrid.Length; x++)
            {
                for (int z = 0; z < map.cellGrid[x][0].Length; z ++)
                {
                    Cell cell = map.cellGrid[x][0][z];
                    int[] incompatibles = map.adjacencyRules.GetIncompatibleBorderTiles(Direction.DOWN);
                    for (int i = 0; i < incompatibles.Length; i++)
                    {
                        if (cell.candidates.Contains(incompatibles[i]))
                        {
                            var siblings = map.GetSiblings(incompatibles[i], new Vector3Int(x, 0, z));
                            for (int r = 0; r < siblings.Count; r++)
                            {
                                if (siblings[r].cell.candidates.Remove(siblings[r].tileIndex))
                                {
                                    map.removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[r].cellPosition, tileIndex = siblings[r].tileIndex });
                                }
                            }
                        }
                    }
                }
            }

            // y = max, top border
            bounds = map.cellGrid[0].GetUpperBound(0);
            for (int x = 0; x < map.cellGrid.Length; x++)
            {
                for (int z = 0; z < map.cellGrid[x][bounds].Length; z ++)
                {
                    Cell cell = map.cellGrid[x][bounds][z];
                    int[] incompatibles = map.adjacencyRules.GetIncompatibleBorderTiles(Direction.UP);
                    for (int i = 0; i < incompatibles.Length; i++)
                    {
                        if (cell.candidates.Contains(incompatibles[i]))
                        {
                            var siblings = map.GetSiblings(incompatibles[i], new Vector3Int(x, bounds, z));
                            for (int r = 0; r < siblings.Count; r++)
                            {
                                if (siblings[r].cell.candidates.Remove(siblings[r].tileIndex))
                                {
                                    map.removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[r].cellPosition, tileIndex = siblings[r].tileIndex });
                                }
                            }
                        }
                    }
                }
            }

            // z = 0, back border
            for (int x = 0; x < map.cellGrid.Length; x++)
            {
                for (int y = 0; y < map.cellGrid[x].Length; y ++)
                {
                    Cell cell = map.cellGrid[x][y][0];
                    int[] incompatibles = map.adjacencyRules.GetIncompatibleBorderTiles(Direction.BACK);
                    for (int i = 0; i < incompatibles.Length; i++)
                    {
                        if (cell.candidates.Contains(incompatibles[i]))
                        {
                            var siblings = map.GetSiblings(incompatibles[i], new Vector3Int(x, y, 0));
                            for (int r = 0; r < siblings.Count; r++)
                            {
                                if (siblings[r].cell.candidates.Remove(siblings[r].tileIndex))
                                {
                                    map.removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[r].cellPosition, tileIndex = siblings[r].tileIndex });
                                }
                            }
                        }
                    }
                }
            }

            // z = max, forward border
            bounds = map.cellGrid[0][0].GetUpperBound(0);
            for (int x = 0; x < map.cellGrid.Length; x++)
            {
                for (int y = 0; y < map.cellGrid[x].Length; y ++)
                {
                    Cell cell = map.cellGrid[x][y][bounds];
                    int[] incompatibles = map.adjacencyRules.GetIncompatibleBorderTiles(Direction.FRONT);
                    for (int i = 0; i < incompatibles.Length; i++)
                    {
                        if (cell.candidates.Contains(incompatibles[i]))
                        {
                            var siblings = map.GetSiblings(incompatibles[i], new Vector3Int(x, y, bounds));
                            for (int r = 0; r < siblings.Count; r++)
                            {
                                if (siblings[r].cell.candidates.Remove(siblings[r].tileIndex))
                                {
                                    map.removalUpdates.Enqueue(new RemovalUpdate { cellPosition = siblings[r].cellPosition, tileIndex = siblings[r].tileIndex });
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
