using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Priority_Queue;
using System.Linq;
using Supercluster.KDTree;

namespace WaveFunctionCollapse
{
    public static class PathGenerator
    {
        private static readonly Vector3Int[] neighborsXZ = new Vector3Int[] { new Vector3Int(1, 0, 0), new Vector3Int(-1, 0, 0), new Vector3Int(0, 0, 1), new Vector3Int(0, 0, -1) };
        private static readonly Vector3Int[] neighborsXY = new Vector3Int[] { new Vector3Int(1, 0, 0), new Vector3Int(-1, 0, 0), new Vector3Int(0, 1, 0), new Vector3Int(0, -1, 0) };
        private static readonly Vector3Int[] neighborsYZ = new Vector3Int[] { new Vector3Int(0, 1, 0), new Vector3Int(0, -1, 0), new Vector3Int(0, 0, 1), new Vector3Int(0, 0, -1) };

        public class Ramp
        {
            public PathPoint[] tiles;
            public Vector3Int enter;
            public Vector3Int exit;
            public Vector3Int[] padding;
            public int outUpDirection;

            public Ramp(PathPoint[] tiles, Vector3Int enter, Vector3Int exit, int outUpDirection, Vector3Int[] padding)
            {
                this.tiles = tiles;
                this.enter = enter;
                this.exit = exit;
                this.outUpDirection = outUpDirection;
                this.padding = padding;
            }

            public Ramp(Ramp ramp)
            {
                this.tiles = new PathPoint[ramp.tiles.Length];
                for (int i = 0; i < ramp.tiles.Length; i++)
                {
                    this.tiles[i] = new PathPoint(ramp.tiles[i].position, ramp.tiles[i].upDirection, true);
                }
                this.enter = ramp.enter;
                this.exit = ramp.exit;
                this.outUpDirection = ramp.outUpDirection;
                this.padding = new Vector3Int[ramp.padding.Length];
                ramp.padding.CopyTo(this.padding, 0);
            }

            public static Ramp UpRamp()
            {
                PathPoint[] tiles = new PathPoint[] { 
                    new PathPoint(new Vector3Int(0, 0, 1), Direction.UP, true),
                    new PathPoint(new Vector3Int(0, 1, 1), Direction.UNDEFINED, true),
                    new PathPoint(new Vector3Int(0, 0, 2), Direction.UNDEFINED, true), 
                    new PathPoint(new Vector3Int(0, 1, 2), Direction.BACK, true) };
                Vector3Int enter = new Vector3Int(0, 0, 0);
                Vector3Int exit = new Vector3Int(0, 2, 2);
                int outUpDirection = Direction.BACK;
                Vector3Int[] padding = new Vector3Int[]
                {
                    new Vector3Int(1, 0, 0),
                    new Vector3Int(1, 0, 1),
                    new Vector3Int(1, 0, 2),
                    new Vector3Int(1, 1, 2),
                    new Vector3Int(1, 2, 2),
                    new Vector3Int(-1, 0, 0),
                    new Vector3Int(-1, 0, 1),
                    new Vector3Int(-1, 0, 2),
                    new Vector3Int(-1, 1, 2),
                    new Vector3Int(-1, 2, 2),
                    new Vector3Int(0, -1, 1),
                    new Vector3Int(0, -1, 2),
                    new Vector3Int(0, -1, 3),
                    new Vector3Int(0, 0, 3),
                    new Vector3Int(0, 1, 3),
                    new Vector3Int(0, 1, 1)
                };
                return new Ramp(tiles, enter, exit, outUpDirection, padding);
            }

            public static Ramp DownRamp()
            {
                PathPoint[] tiles = new PathPoint[] { 
                    new PathPoint(new Vector3Int(0, 0, 1), Direction.UP, true),
                    new PathPoint(new Vector3Int(0, -1, 1), Direction.UNDEFINED, true),
                    new PathPoint(new Vector3Int(0, -1, 2), Direction.UNDEFINED, true),
                    new PathPoint(new Vector3Int(0, -2, 1), Direction.UNDEFINED, true),
                    new PathPoint(new Vector3Int(0, -2, 2), Direction.UNDEFINED, true),
                    new PathPoint(new Vector3Int(0, -2, 3), Direction.UNDEFINED, true),
                    new PathPoint(new Vector3Int(0, -2, 3), Direction.FRONT, true)};
                Vector3Int enter = new Vector3Int(0, 0, 0);
                Vector3Int exit = new Vector3Int(0, -3, 3);
                int outUpDirection = Direction.FRONT;
                Vector3Int[] padding = new Vector3Int[]
                {
                    new Vector3Int(1, 0, 0),
                    new Vector3Int(-1, 0, 0),
                    new Vector3Int(1, 0, 1),
                    new Vector3Int(-1, 0, 1),
                    new Vector3Int(0, 0, 2),
                    new Vector3Int(1, 0, 2),
                    new Vector3Int(-1, 0, 2),
                    new Vector3Int(0, 0, 3),
                    new Vector3Int(1, 0, 3),
                    new Vector3Int(-1, 0, 3),
                    new Vector3Int(1, -1, 1),
                    new Vector3Int(-1, -1, 1),
                    new Vector3Int(1, -1, 2),
                    new Vector3Int(-1, -1, 2),
                    new Vector3Int(0, -1, 3),
                    new Vector3Int(1, -1, 3),
                    new Vector3Int(-1, -1, 3),
                    new Vector3Int(1, -2, 1),
                    new Vector3Int(-1, -2, 1),
                    new Vector3Int(1, -2, 2),
                    new Vector3Int(-1, -2, 2),
                    new Vector3Int(1, -2, 3),
                    new Vector3Int(-1, -2, 3),
                    new Vector3Int(1, -3, 3),
                    new Vector3Int(-1, -3, 3)
                };
                return new Ramp(tiles, enter, exit, outUpDirection, padding);
            }

            public static Ramp RightRollRamp()
            {
                PathPoint[] tiles = new PathPoint[] { 
                    new PathPoint(new Vector3Int(0, 0, 1), Direction.UP, true), 
                    new PathPoint(new Vector3Int(0, 0, 2), Direction.RIGHT, true) };
                Vector3Int enter = new Vector3Int(0, 0, 0);
                Vector3Int exit = new Vector3Int(0, 0, 3);
                int outUpDirection = Direction.RIGHT;
                Vector3Int[] padding = new Vector3Int[]
                {
                    new Vector3Int(1, 0, 0),
                    new Vector3Int(1, 0, 1),
                    new Vector3Int(1, 0, 2),
                    new Vector3Int(0, 0, 3),
                    new Vector3Int(-1, 0, 0),
                    new Vector3Int(-1, 0, 1),
                    new Vector3Int(-1, 0, 2),
                    new Vector3Int(-1, 0, 3),
                    new Vector3Int(0, -1, 1),
                    new Vector3Int(0, -1, 2),
                    new Vector3Int(0, 1, 1),
                    new Vector3Int(0, 1, 2)
                };
                return new Ramp(tiles, enter, exit, outUpDirection, padding);
            }

            public static Ramp LeftRollRamp()
            {
                PathPoint[] tiles = new PathPoint[] { new PathPoint(new Vector3Int(0, 0, 1), Direction.UP, true), 
                    new PathPoint(new Vector3Int(0, 0, 2), Direction.LEFT, true) };
                Vector3Int enter = new Vector3Int(0, 0, 0);
                Vector3Int exit = new Vector3Int(0, 0, 3);
                int outUpDirection = Direction.LEFT;
                Vector3Int[] padding = new Vector3Int[]
                {
                    new Vector3Int(1, 0, 0),
                    new Vector3Int(1, 0, 1),
                    new Vector3Int(1, 0, 2),
                    new Vector3Int(0, 0, 3),
                    new Vector3Int(-1, 0, 0),
                    new Vector3Int(-1, 0, 1),
                    new Vector3Int(-1, 0, 2),
                    new Vector3Int(-1, 0, 3),
                    new Vector3Int(0, -1, 1),
                    new Vector3Int(0, -1, 2),
                    new Vector3Int(0, 1, 1),
                    new Vector3Int(0, 1, 2)
                };
                return new Ramp(tiles, enter, exit, outUpDirection, padding);
            }
        }


        public static List<PathPoint> Generate(Vector3Int gridSize, int numWayPoints, double searchRange)
        {
            int[][] waypoints = GetSamplePoints(gridSize, numWayPoints, searchRange);

            HashSet<Vector3Int> goals = new HashSet<Vector3Int>();
            for (int i = 0; i < waypoints.Length; i++)
            {
                goals.Add(new Vector3Int(waypoints[i][0], waypoints[i][1], waypoints[i][2]));
            }

            if (goals.Count < 2)
            {
                throw new System.Exception("Not enough samples for path generation available");
            }

            return FindPaths(gridSize, goals);
        }

        public static List<PathPoint> FindPaths(Vector3Int gridSize, HashSet<Vector3Int> goals)
        {
            PathPoint current = new PathPoint(goals.First(), Direction.UP, false);

            List<PathPoint> paths = new List<PathPoint>();
            HashSet<Vector3Int> traveled = new HashSet<Vector3Int>();

            paths.Add(current);
            traveled.Add(current.position);

            while (goals.Count > 1)
            {
                goals.Remove(current.position);
                Vector3Int goal = FindClosestGoal(current.position, goals);

                List<PathPoint> nextPath = new List<PathPoint>();
                bool success = false;



                if (HeightDifference(current, goal) == 0)
                {
                    // place random tiles from start to goal
                    success = PlaceTiles(current, goal, nextPath, traveled, gridSize);

                    if (!success)
                    {
                        return null;
                    }

                    current = new PathPoint(goal, current.upDirection, false);
                }
                else
                {
                    List<int> triedRamps = new List<int>();
                    Ramp adjustedRamp = null;
                    PathPoint rampExit = null;

                    do
                    {
                        Ramp ramp = ChooseRandomRamp(current, goal, triedRamps, gridSize);

                        if (ramp == null)
                        {
                            return null;
                        }

                        for (int i = 0; i < 2 && !success; i++)
                        {
                            adjustedRamp = FindExitDirectionAndTransformRamp(current, goal, ramp, out int exitUpDirection, out Vector3Int exitPosition, out Vector3Int offsetVector, i);
                            success = PlaceRamp(current, adjustedRamp, nextPath, traveled, exitUpDirection, exitPosition, offsetVector, gridSize, out rampExit);
                        }
                    } while (!success);



                    // place tiles from start to ramp and from ramp to goal
                    success = PlaceTiles(current, adjustedRamp.enter, nextPath, traveled, gridSize, adjustedRamp);
                    if (!success)
                    {
                        return null;
                    }

                    success = PlaceTiles(rampExit, goal, nextPath, traveled, gridSize);
                    if (!success)
                    {
                        return null;
                    }

                    current = new PathPoint(goal, adjustedRamp.outUpDirection, false);
                }

                paths.AddRange(nextPath);
            }

            return paths;
        }

        private static Ramp FindExitDirectionAndTransformRamp(PathPoint current, Vector3Int goal, Ramp ramp, out int exitUpDirection, out Vector3Int exitPosition, out Vector3Int offsetVector, int loopCount)
        {
            // Find direction of exit
            Vector3Int diff = goal - current.position;
            exitUpDirection = -1;
            exitPosition = new Vector3Int();
            offsetVector = new Vector3Int();

            Ramp adjustedRamp = new Ramp(ramp);

            if (current.upDirection == Direction.UP || current.upDirection == Direction.DOWN)
            {
                if ((Mathf.Abs(diff.x) > Mathf.Abs(diff.z) && loopCount == 0) || loopCount != 0)
                {
                    exitUpDirection = diff.z > 0 ? Direction.FRONT : Direction.BACK;
                    TransformRamp(current, goal, exitUpDirection, adjustedRamp);
                    exitPosition = new Vector3Int((current.position.x + goal.x) / 2, current.position.y + ramp.exit.y, goal.z);
                    offsetVector = new Vector3Int(1, 0, 0);
                }
                else
                {
                    exitUpDirection = diff.x > 0 ? Direction.RIGHT : Direction.LEFT;
                    TransformRamp(current, goal, exitUpDirection, adjustedRamp);
                    exitPosition = new Vector3Int(goal.x, current.position.y + ramp.exit.y, (current.position.z + goal.z) / 2);
                    offsetVector = new Vector3Int(0, 0, 1);
                }
            }
            else if (current.upDirection == Direction.RIGHT || current.upDirection == Direction.LEFT)
            {
                if ((Mathf.Abs(diff.z) > Mathf.Abs(diff.y) && loopCount == 0) || loopCount != 0)
                {
                    exitUpDirection = diff.y > 0 ? Direction.UP : Direction.DOWN;
                    TransformRamp(current, goal, exitUpDirection, adjustedRamp);
                    exitPosition = new Vector3Int(current.position.x + ramp.exit.x, goal.y, (current.position.z + goal.z) / 2);
                    offsetVector = new Vector3Int(0, 0, 1);
                }
                else
                {
                    exitUpDirection = diff.z > 0 ? Direction.FRONT : Direction.BACK;
                    TransformRamp(current, goal, exitUpDirection, adjustedRamp);
                    exitPosition = new Vector3Int(current.position.x + ramp.exit.x, (current.position.y + goal.y) / 2, goal.z);
                    offsetVector = new Vector3Int(0, 1, 0);
                }
            }
            else if (current.upDirection == Direction.FRONT || current.upDirection == Direction.BACK)
            {
                if ((Mathf.Abs(diff.x) > Mathf.Abs(diff.y) && loopCount == 0) || loopCount != 0)
                {
                    exitUpDirection = diff.y > 0 ? Direction.UP : Direction.DOWN;
                    TransformRamp(current, goal, exitUpDirection, adjustedRamp);
                    exitPosition = new Vector3Int((current.position.x + goal.x) / 2, goal.y, current.position.z + ramp.exit.z);
                    offsetVector = new Vector3Int(1, 0, 0);
                }
                else
                {
                    exitUpDirection = diff.x > 0 ? Direction.RIGHT : Direction.LEFT;
                    TransformRamp(current, goal, exitUpDirection, adjustedRamp);
                    exitPosition = new Vector3Int(goal.x, (current.position.y + goal.y) / 2, current.position.z + ramp.exit.z);
                    offsetVector = new Vector3Int(0, 1, 0);
                }
            }

            return adjustedRamp;
        }

        private static bool PlaceRamp(PathPoint current, Ramp ramp, List<PathPoint> paths, HashSet<Vector3Int> traveled, 
            int exitUpDirection, Vector3Int exitPosition, Vector3Int offsetVector, Vector3Int gridSize, out PathPoint rampExit)
        {
            // create random exit positions by adding different offsets
            List<Vector3Int> possibleExits = new List<Vector3Int>();
            for (int i = 0; i < 8; i++)
            {
                Vector3Int pos = exitPosition + offsetVector * i;
                if (IsInGrid(gridSize, pos))
                {
                    int diff = (Vector3Int.Scale(current.position, offsetVector) - Vector3Int.Scale(pos, offsetVector)).sqrMagnitude;
                    if (diff > 2)
                    {
                        possibleExits.Add(pos);
                    }
                }
                if (i == 0) continue;
                pos = exitPosition - offsetVector * i;
                if (IsInGrid(gridSize, pos))
                {
                    int diff = Mathf.Abs(Vector3Int.Scale(current.position, offsetVector).sqrMagnitude - Vector3Int.Scale(pos, offsetVector).sqrMagnitude);
                    if (diff > 1)
                    {
                        possibleExits.Add(pos);
                    }
                }
            }

            // go through all random positions and take the first one that fits
            for (int i = 0; i < possibleExits.Count; i++)
            {
                // test exit and enter if already traveled
                if (traveled.Contains(possibleExits[i]) || traveled.Contains(possibleExits[i] - ramp.exit))
                {
                    continue;
                }

                // test ramp tiles if already traveled
                bool success = true;
                Vector3Int[] rampTiles = new Vector3Int[ramp.tiles.Length];
                for (int r = 0; r < ramp.tiles.Length; r++)
                {
                    rampTiles[r] = ramp.tiles[r].position + (possibleExits[i] - ramp.exit);
                    if (traveled.Contains(rampTiles[r]))
                    {
                        success = false;
                        break;
                    }
                }

                if (success)
                {
                    for (int r = 0; r < ramp.padding.Length; r++)
                    {
                        ramp.padding[r] = ramp.padding[r] + (possibleExits[i] - ramp.exit);
                        traveled.Add(ramp.padding[r]);
                    }
                    ramp.enter = possibleExits[i] - ramp.exit;
                    ramp.exit = possibleExits[i];
                    for (int r = 0; r < ramp.tiles.Length; r++)
                    {
                        ramp.tiles[r].position = rampTiles[r];
                        traveled.Add(ramp.tiles[r].position);
                        paths.Add(ramp.tiles[r]);
                    }

                    rampExit = new PathPoint(ramp.exit, exitUpDirection, false);
                    int dir = Direction.FromVector(ramp.exit - ramp.tiles.Last().position);
                    ramp.tiles.Last().exits.Add(dir);
                    rampExit.exits.Add(Direction.Opposite(dir));

                    traveled.Add(ramp.exit);
                    paths.Add(rampExit);

                    return true;
                }
            }

            rampExit = null;
            return false;
        }

        private static void TransformRamp(PathPoint current, Vector3Int goal, int exitUpDir, in Ramp ramp)
        {
            // align ramp with current updirection
            ERotator rotator = 0;

            switch (current.upDirection)
            {
                case Direction.DOWN:
                    rotator = ERotator.x180;
                    break;
                case Direction.RIGHT:
                    rotator = ERotator.z270;
                    break;
                case Direction.LEFT:
                    rotator = ERotator.z90;
                    break;
                case Direction.FRONT:
                    rotator = ERotator.x90;
                    break;
                case Direction.BACK:
                    rotator = ERotator.x270;
                    break;
            }

            if (current.upDirection != Direction.UP)
            {
                for (int i = 0; i < ramp.tiles.Length; i++)
                {
                    ramp.tiles[i].position = TransformHelper.TransformPosition(ramp.tiles[i].position, rotator, EReflector.None);
                    if (ramp.tiles[i].upDirection >= 0)
                    {
                        ramp.tiles[i].upDirection = Direction.TransformDirection(ramp.tiles[i].upDirection, rotator, EReflector.None);
                    }
                }
                for (int i = 0; i < ramp.padding.Length; i++)
                {
                    ramp.padding[i] = TransformHelper.TransformPosition(ramp.padding[i], rotator, EReflector.None);
                }
                ramp.exit = TransformHelper.TransformPosition(ramp.exit, rotator, EReflector.None);
                ramp.outUpDirection = Direction.TransformDirection(ramp.outUpDirection, rotator, EReflector.None);
            }

            // align ramp with goal plane
            rotator = Direction.FromToDirection(ramp.outUpDirection, exitUpDir);
            for (int i = 0; i < ramp.tiles.Length; i++)
            {
                ramp.tiles[i].position = TransformHelper.TransformPosition(ramp.tiles[i].position, rotator, EReflector.None);
                if (ramp.tiles[i].upDirection >= 0)
                {
                    ramp.tiles[i].upDirection = Direction.TransformDirection(ramp.tiles[i].upDirection, rotator, EReflector.None);
                }
            }
            for (int i = 0; i < ramp.padding.Length; i++)
            {
                ramp.padding[i] = TransformHelper.TransformPosition(ramp.padding[i], rotator, EReflector.None);
            }
            ramp.exit = TransformHelper.TransformPosition(ramp.exit, rotator, EReflector.None);
            ramp.outUpDirection = Direction.TransformDirection(ramp.outUpDirection, rotator, EReflector.None);
        }

        private static Ramp ChooseRandomRamp(PathPoint current, Vector3Int goal, List<int> triedRamps, Vector3Int gridSize)
        {
            List<System.Tuple<Ramp, float>> ramps = new List<System.Tuple<Ramp, float>>();

            int distToCeil = DistanceToBorder(current, gridSize, true);
            int distToFloor = DistanceToBorder(current, gridSize, false);

            if (distToCeil > 2)
            {
                ramps.Add(new System.Tuple<Ramp, float>(Ramp.UpRamp(), 1f));
            }
            if (distToFloor > 3)
            {
                ramps.Add(new System.Tuple<Ramp, float>(Ramp.DownRamp(), 1f));
            }

            //ramps.Add(new System.Tuple<Ramp, float>(Ramp.RightRollRamp(), 0.3f));
            //ramps.Add(new System.Tuple<Ramp, float>(Ramp.LeftRollRamp(), 0.3f));

            foreach (var num in triedRamps.OrderByDescending(x => x))
            {
                ramps.RemoveAt(num);
            }

            if (ramps.Count == 0)
            {
                return null;
            }

            float randVal = Random.value * ramps.Sum(x => x.Item2) - Mathf.Epsilon;
            int chosen = -1;
            

            for (int i = 0; i < ramps.Count; i++)
            {
                randVal -= ramps[i].Item2;
                if (randVal <= 0)
                {
                    chosen = i;
                    break;
                }
            }

            if (chosen == -1) return null;

            triedRamps.Add(chosen);

            return (ramps[chosen].Item1);
        }

        private static bool PlaceTiles(PathPoint start, Vector3Int goal, List<PathPoint> paths, HashSet<Vector3Int> traveled, Vector3Int gridSize, Ramp ramp = null)
        {
            Queue<PathPoint> candidateQueue = new Queue<PathPoint>();
            Dictionary<PathPoint, PathPoint> path = new Dictionary<PathPoint, PathPoint>();
            candidateQueue.Enqueue(start);

            while (candidateQueue.Count > 0)
            {
                PathPoint current = candidateQueue.Dequeue();

                List<PathPoint> nextCandidates = GetNeighboringCandidates(start, current, traveled, path, gridSize);

                for (int i = 0; i < nextCandidates.Count; i++)
                {
                    path.Add(nextCandidates[i], current);
                    if (nextCandidates[i].position == goal)
                    {

                        if (ramp != null)
                        {
                            int dir = Direction.FromVector(ramp.tiles[0].position - nextCandidates[i].position);
                            nextCandidates[i].exits.Add(dir);
                            ramp.tiles[0].exits.Add(Direction.Opposite(dir));
                        }

                        //Reconstruct path
                        // add to paths and traveled
                        ReconstructAndAddPath(start, nextCandidates[i], path, paths, traveled);

                        return true;
                    } else
                    {
                        candidateQueue.Enqueue(nextCandidates[i]);
                    }
                }
            }

            return false;
        }

        private static void ReconstructAndAddPath(PathPoint start, PathPoint end, Dictionary<PathPoint, PathPoint> path, List<PathPoint> paths, HashSet<Vector3Int> traveled)
        {
            PathPoint current = end;

            while (true)
            {
                PathPoint next = path[current];
                int dir = Direction.FromVector(next.position - current.position);
                current.exits.Add(dir);
                next.exits.Add(Direction.Opposite(dir));

                paths.Add(current);
                traveled.Add(current.position);

                if (next.position == start.position)
                {
                    break;
                }

                current = next;
            }
        }

        private static List<PathPoint> GetNeighboringCandidates(PathPoint start, PathPoint current, HashSet<Vector3Int> traveled, Dictionary<PathPoint, PathPoint> path, Vector3Int gridSize)
        {
            List<PathPoint> candidates = new List<PathPoint>();

            switch (current.upDirection)
            {
                case Direction.UP:
                case Direction.DOWN:
                    for (int i = 0; i < neighborsXZ.Length; i++)
                    {
                        Vector3Int neighborPos = current.position + neighborsXZ[i];
                        PathPoint neighbor = new PathPoint(neighborPos, start.upDirection, false);
                        if (!path.ContainsKey(neighbor) && !traveled.Contains(neighborPos) && IsInGrid(gridSize, neighborPos))
                        {
                            candidates.Add(neighbor);
                        }
                    }
                    break;
                case Direction.RIGHT:
                case Direction.LEFT:
                    for (int i = 0; i < neighborsYZ.Length; i++)
                    {
                        Vector3Int neighborPos = current.position + neighborsYZ[i];
                        PathPoint neighbor = new PathPoint(neighborPos, start.upDirection, false);
                        if (!path.ContainsKey(neighbor) && !traveled.Contains(neighborPos) && IsInGrid(gridSize, neighborPos))
                        {
                            candidates.Add(neighbor);
                        }
                    }
                    break;
                case Direction.FRONT:
                case Direction.BACK:
                    for (int i = 0; i < neighborsXY.Length; i++)
                    {
                        Vector3Int neighborPos = current.position + neighborsXY[i];
                        PathPoint neighbor = new PathPoint(neighborPos, start.upDirection, false);
                        if (!path.ContainsKey(neighbor) && !traveled.Contains(neighborPos) && IsInGrid(gridSize, neighborPos))
                        {
                            candidates.Add(neighbor);
                        }
                    }
                    break;
            }

            return candidates;
        }

        private static Vector3Int FindClosestGoal(Vector3Int start, HashSet<Vector3Int> goals)
        {
            Vector3Int closest = new Vector3Int();
            int distance = int.MaxValue;

            foreach (var goal in goals)
            {
                int current = (goal - start).sqrMagnitude;
                if (current < distance)
                {
                    closest = goal;
                    distance = current;
                }
            }

            return closest;
        }

        /// <summary>
        /// Return 0 if current and goal are on same plane. 1 if goal is above current (local y), -1 if below
        /// </summary>
        private static int HeightDifference(PathPoint current, Vector3Int goal)
        {
            switch(current.upDirection)
            {
                case Direction.UP:
                    return goal.y - current.position.y;
                case Direction.DOWN:
                    return  current.position.y - goal.y;
                case Direction.RIGHT:
                    return goal.x - current.position.x;
                case Direction.LEFT:
                    return current.position.x - goal.x;
                case Direction.FRONT:
                    return goal.z - current.position.z;
                case Direction.BACK:
                    return current.position.z - goal.z;
                default:
                    throw new System.ArgumentException("PathPoint has invalid upDirection");
            }
        }

        private static int DistanceToBorder(PathPoint current, Vector3Int gridSize, bool ceiling)
        {
            switch (current.upDirection)
            {
                case Direction.UP:
                    if (ceiling)
                    {
                        return gridSize.y - current.position.y - 1;
                    } else
                    {
                        return current.position.y;
                    }
                case Direction.DOWN:
                    if (ceiling)
                    {
                        return current.position.y;
                    }
                    else
                    {
                        return gridSize.y - current.position.y - 1;
                    }
                case Direction.RIGHT:
                    if (ceiling)
                    {
                        return gridSize.x - current.position.x - 1;
                    }
                    else
                    {
                        return current.position.x;
                    }
                case Direction.LEFT:
                    if (ceiling)
                    {
                        return current.position.x;
                    }
                    else
                    {
                        return gridSize.x - current.position.x - 1;
                    }
                case Direction.FRONT:
                    if (ceiling)
                    {
                        return gridSize.z - current.position.z - 1;
                    }
                    else
                    {
                        return current.position.z;
                    }
                case Direction.BACK:
                    if (ceiling)
                    {
                        return current.position.z;
                    }
                    else
                    {
                        return gridSize.z - current.position.z - 1;
                    }
                default:
                    throw new System.ArgumentException("PathPoint has invalid upDirection");
            }
        }

        private static bool IsInGrid(Vector3Int gridSize, Vector3Int point)
        {
            if (point.x < 0 || point.x >= gridSize.x
                 || point.y < 0 || point.y >= gridSize.y
                 || point.z < 0 || point.z >= gridSize.z)
            {
                return false;
            }

            return true;
        }

        public static int[][] GetSamplePoints(Vector3Int gridSize, int desiredSamples, double searchRange)
        {
            // define metric function
            System.Func<int[], int[], double> L2Norm = (p, q) =>
            {
                double dist = 0;
                for (int i = 0; i < p.Length; i++)
                {
                    dist += (p[i] - q[i]) * (p[i] - q[i]);
                }

                return dist;
            };

            // generate sample points and build tree
            int[][] treeData = GenerateRandomPoints(gridSize, 64);
            var treeNodes = Enumerable.Range(0, treeData.Length).ToArray();
            var tree = new KDTree<int, int>(3, treeData, treeNodes, L2Norm);

            // max heap
            SimplePriorityQueue<int, int> heap = new SimplePriorityQueue<int, int>((x, y) => y - x);

            // calc weight for every sample point and add to priority queue
            for (int i = 0; i < treeData.Length; i++)
            {
                var neighbors = tree.RadialSearch(treeData[i], searchRange);
                heap.Enqueue(treeNodes[i], neighbors.Length);
            }

            HashSet<int> removedIds = new HashSet<int>();

            // Until only the desired amount of points are left
            while (heap.Count > desiredSamples)
            {
                // dequeue point with most neighbors and remove it
                int id = heap.Dequeue();
                removedIds.Add(id);

                int[] sample = treeData[id];

                var neighbors = tree.RadialSearch(sample, searchRange);

                // update weight of all neighbors in radius
                for (int i = 0; i < neighbors.Length; i++)
                {
                    if (removedIds.Contains(neighbors[i].Item2)) continue;

                    int weight = heap.GetPriority(neighbors[i].Item2);

                    heap.UpdatePriority(neighbors[i].Item2, weight - 1);
                }
            }

            return heap.Select(x => treeData[x]).ToArray();
        }

        private static int[][] GenerateRandomPoints(Vector3Int gridSize, int sampleCount)
        {
            int[][] points = new int[sampleCount][];

            for (int i = 0; i < sampleCount; i++)
            {
                int mod = i % 8;
                switch (mod)
                {
                    case 0:
                        points[i] = new int[] { Random.Range(0, gridSize.x/2), Random.Range(0, gridSize.y/2), Random.Range(0, gridSize.z/2) };
                        break;
                    case 1:
                        points[i] = new int[] { Random.Range(gridSize.x / 2, gridSize.x), Random.Range(0, gridSize.y / 2), Random.Range(0, gridSize.z / 2) };
                        break;
                    case 2:
                        points[i] = new int[] { Random.Range(0, gridSize.x / 2), Random.Range(gridSize.y / 2, gridSize.y), Random.Range(0, gridSize.z / 2) };
                        break;
                    case 3:
                        points[i] = new int[] { Random.Range(0, gridSize.x / 2), Random.Range(0, gridSize.y / 2), Random.Range(gridSize.z / 2, gridSize.z) };
                        break;
                    case 4:
                        points[i] = new int[] { Random.Range(gridSize.x / 2, gridSize.x), Random.Range(gridSize.y / 2, gridSize.y), Random.Range(0, gridSize.z / 2) };
                        break;
                    case 5:
                        points[i] = new int[] { Random.Range(gridSize.x / 2, gridSize.x), Random.Range(0, gridSize.y / 2), Random.Range(gridSize.z / 2, gridSize.z) };
                        break;
                    case 6:
                        points[i] = new int[] { Random.Range(0, gridSize.x / 2), Random.Range(gridSize.y / 2, gridSize.y), Random.Range(gridSize.z / 2, gridSize.z) };
                        break;
                    case 7:
                        points[i] = new int[] { Random.Range(gridSize.x / 2, gridSize.x), Random.Range(gridSize.y / 2, gridSize.y), Random.Range(gridSize.z / 2, gridSize.z) };
                        break;
                }
            }

            return points;
        }
    }


    public class PathPoint : System.IEquatable<PathPoint>
    {
        /// <summary>
        /// position on the grid
        /// </summary>
        public Vector3Int position;
        public int upDirection;
        public bool isRamp;
        public List<int> exits;

        public PathPoint(Vector3Int position, int upDirection, bool isRamp)
        {
            this.position = position;
            this.upDirection = upDirection;
            this.isRamp = isRamp;
            this.exits = new List<int>(2);
        }

        public bool Equals(PathPoint other)
        {
            if (this.position == other.position)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return 0;
        }
    }

    //public class WalkPoint : System.IEquatable<WalkPoint>
    //{
    //    /// <summary>
    //    /// position on the grid
    //    /// </summary>
    //    public Vector3Int position;
    //    /// <summary>
    //    /// normal vector of the walkable ground
    //    /// </summary>
    //    public int upDirection;
    //    /// <summary>
    //    /// direction from previous point to this point
    //    /// </summary>
    //    public int toCurrentDirection;
    //    /// <summary>
    //    /// how many points in a row we've walked in the same direction
    //    /// </summary>
    //    public int rowCount;
    //    /// <summary>
    //    /// if >0, forced to continue in same direction as toCurrentDirection
    //    /// </summary>
    //    public int forceCount;

    //    public WalkPoint(Vector3Int position, int upDirection, int toCurrentDirection, int rowCount, int forceCount)
    //    {
    //        this.position = position;
    //        this.rowCount = rowCount;
    //        this.upDirection = upDirection;
    //        this.toCurrentDirection = toCurrentDirection;
    //        this.forceCount = forceCount;
    //    }

    //    public bool Equals(WalkPoint other)
    //    {
    //        if (this.position == other.position)
    //        {
    //            return true;
    //        } else
    //        {
    //            return false;
    //        }
    //    }

    //    public override int GetHashCode()
    //    {
    //        return 0;
    //    }
    //}
}









//public static Graph CreateGraph(Vector3Int gridSize, int waypoints, double searchRange)
//{
//    Graph graph = new Graph();

//    graph.points = GetSamplePoints(gridSize, waypoints, searchRange);

//    HashSet<int> alreadyConnected = new HashSet<int>() { 0 };
//    int startPoint = 0;

//    List<int[]> connections = new List<int[]>();

//    while (alreadyConnected.Count < graph.points.Length)
//    {
//        // find closest waypoint to start waypoint
//        float dist = float.MaxValue;
//        int candidate = -1;

//        for (int i = 0; i < graph.points.Length; i++)
//        {
//            if (alreadyConnected.Contains(i)) continue;

//            float newDist = (graph.points[startPoint][0] - graph.points[i][0]) * (graph.points[startPoint][0] - graph.points[i][0])
//                + (graph.points[startPoint][1] - graph.points[i][1]) * (graph.points[startPoint][1] - graph.points[i][1])
//                + (graph.points[startPoint][2] - graph.points[i][2]) * (graph.points[startPoint][2] - graph.points[i][2]);

//            if (newDist < dist)
//            {
//                dist = newDist;
//                candidate = i;
//            }
//        }

//        connections.Add(new int[] { startPoint, candidate });
//        alreadyConnected.Add(candidate);
//        startPoint = candidate;
//    }

//    graph.connections = connections.ToArray();

//    return graph;
//}







//public static List<List<WalkPoint>> FindPaths(Vector3Int gridSize, HashSet<Vector3Int> goals)
//{
//    List<List<WalkPoint>> paths = new List<List<WalkPoint>>();

//    WalkPoint start = new WalkPoint(goals.First(), Direction.UP, -1, 1, 0);

//    while (goals.Count > 1)
//    {
//        WalkPoint end = null;
//        goals.Remove(start.position);

//        // Perform BFS until a goal point is found
//        Dictionary<WalkPoint, WalkPoint> traveled = new Dictionary<WalkPoint, WalkPoint>();
//        HashSet<Vector3Int> chosenPoints = new HashSet<Vector3Int>();

//        Queue<WalkPoint> searchQueue = new Queue<WalkPoint>();
//        searchQueue.Enqueue(start);
//        traveled.Add(start, null);

//        // Use BFS to find shortest path to closest goal
//        while (searchQueue.Count > 0)
//        {
//            WalkPoint current = searchQueue.Dequeue();
//            List<WalkPoint> neighbors = GetNeighbors(gridSize, current, traveled);

//            for (int i = 0; i < neighbors.Count; i++)
//            {
//                if (goals.Contains(neighbors[i].position))
//                {
//                    // found goal
//                    end = neighbors[i];
//                    traveled.Add(end, current);
//                    break;
//                }
//                else if (!traveled.ContainsKey(neighbors[i]) && !chosenPoints.Contains(neighbors[i].position))
//                {
//                    // add neighbors to queue
//                    searchQueue.Enqueue(neighbors[i]);
//                    traveled.Add(neighbors[i], current);
//                }
//            }
//        }

//        // Reconstruct path
//        List<WalkPoint> foundPath = ReconstructPath(end, traveled);
//        paths.Add(foundPath);
//        foundPath.ForEach(x => chosenPoints.Add(x.position));

//        // set found goal point as next start
//        start = end;
//    }

//    return paths;
//}

//private static List<WalkPoint> ReconstructPath(WalkPoint end, Dictionary<WalkPoint, WalkPoint> traveled)
//{
//    List<WalkPoint> path = new List<WalkPoint>();
//    path.Add(end);

//    while (true)
//    {
//        WalkPoint point = traveled[path[path.Count - 1]];
//        if (point != null)
//        {
//            path.Add(point);
//        }
//        else
//        {
//            break;
//        }
//    }

//    return path;
//}

//public static List<WalkPoint> GetNeighbors(Vector3Int gridSize, WalkPoint current, in Dictionary<WalkPoint, WalkPoint> traveled)
//{
//    List<WalkPoint> neighbors = new List<WalkPoint>();

//    // forced to add only neighbor in decided direction
//    if (current.forceCount > 0)
//    {
//        WalkPoint neighbor = new WalkPoint(current.position + Direction.ToVector(current.toCurrentDirection),
//            current.upDirection, current.toCurrentDirection, current.rowCount + 1, current.forceCount - 1);

//        if (!traveled.ContainsKey(neighbor))
//        {
//            neighbors.Add(neighbor);
//        }
//    }
//    // else add all neighbors around current
//    else
//    {
//        // find neighbor in every direction
//        for (int dir = 0; dir < 6; dir++)
//        {
//            Vector3Int neighborPos = current.position + Direction.ToVector(dir);

//            // check if neighbor in grid
//            if (!IsInGrid(gridSize, neighborPos))
//            {
//                continue;
//            }

//            WalkPoint neighbor = new WalkPoint(neighborPos, 0, 0, 0, 0);

//            // if direction is local upward
//            if (dir == current.upDirection)
//            {
//                // enough tiles in a row yet?
//                if (current.rowCount < upForcedCount)
//                {
//                    continue;
//                }

//                // check if enough space in grid
//                Vector3Int testPos = neighbor.position + Direction.ToVector(dir) * upForcedCount;
//                if (!IsInGrid(gridSize, testPos))
//                {
//                    continue;
//                }

//                // set walkpoint values
//                neighbor.upDirection = Direction.Opposite(current.toCurrentDirection);
//                neighbor.rowCount = 0;
//                neighbor.forceCount = 0;
//                neighbor.toCurrentDirection = dir;

//                if (!traveled.ContainsKey(neighbor))
//                {
//                    neighbors.Add(neighbor);
//                }

//            }
//            // if direction is downward
//            else if (dir == Direction.Opposite(current.upDirection))
//            {
//                // enough tiles in a row yet?
//                if (current.rowCount < downForcedCount)
//                {
//                    continue;
//                }

//                // check if enough space in grid
//                Vector3Int testPos = neighbor.position + Direction.ToVector(dir) * downForcedCount;
//                if (!IsInGrid(gridSize, testPos))
//                {
//                    continue;
//                }

//                // set walkpoint values like upvector
//                neighbor.upDirection = current.toCurrentDirection;
//                neighbor.rowCount = -1;
//                neighbor.forceCount = 1;
//                neighbor.toCurrentDirection = dir;

//                if (!traveled.ContainsKey(neighbor))
//                {
//                    neighbors.Add(neighbor);
//                }
//            }
//            // if horizontal neighbor
//            else
//            {
//                neighbor.upDirection = current.upDirection;
//                neighbor.toCurrentDirection = dir;

//                if (current.toCurrentDirection == dir)
//                {
//                    neighbor.rowCount = current.rowCount + 1;
//                }
//                else
//                {
//                    neighbor.rowCount = 1;
//                }
//            }

//            // check if neighbor already visited
//            if (!traveled.ContainsKey(neighbor))
//            {
//                neighbors.Add(neighbor);
//            }
//        }
//    }


//    return neighbors;
//}