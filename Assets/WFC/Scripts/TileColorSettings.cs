﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Contains the available colors used to edit tiles, as well as the color compatibility matrix
/// </summary>
[CreateAssetMenu(fileName = "Color Settings", menuName = "ScriptableObjects/TileColorSettings")]
public class TileColorSettings : ScriptableObject
{
    /// <summary>
    /// Available colors
    /// </summary>
    public Color[] colors = new Color[] { Color.red, Color.black};
    /// <summary>
    /// Textures generated from available colors. Used for inspector buttons
    /// </summary>
    [HideInInspector]
    public Texture2D[] textures;
    /// <summary>
    /// Color compatibility matrix
    /// </summary>
    [HideInInspector]
    public bool[] compatibilityMatrix;

    /// <summary>
    /// Create textures from colors, used for displaying in the inspector as buttons
    /// </summary>
    public void UpdateButtonTextures()
    {
        textures = new Texture2D[colors.Length];
        for (int i = 0; i < colors.Length; i++)
        {
            var texture = new Texture2D(Screen.width, Screen.height);
            Color[] pixels = Enumerable.Repeat(colors[i], Screen.width * Screen.height).ToArray();
            texture.SetPixels(pixels);
            texture.Apply();
            textures[i] = texture;
        }
    }

    public void InitializeMatrix()
    {
        int offset = 0;
        compatibilityMatrix = new bool[(colors.Length * colors.Length + colors.Length) / 2];

        for (int i = 0; i < colors.Length; ++i)
        {
            int j = 0;
            for (; j <= i; ++j)
            {
                compatibilityMatrix[i + j + offset] = i == j;
            }
            offset += j-1;
        }
    }

    public void AdjustMatrix()
    {
        bool[] newMatrix = new bool[(colors.Length * colors.Length + colors.Length) / 2];
        int offset = 0;

        for (int i = 0; i < colors.Length; ++i)
        {
            int j = 0;
            for (; j <= i; ++j)
            {
                if (i+j+offset < compatibilityMatrix.Length)
                {
                    newMatrix[i + j + offset] = compatibilityMatrix[i + j + offset];
                } else
                {
                    newMatrix[i + j + offset] = i == j;
                }
            }
            offset += j - 1;
        }

        compatibilityMatrix = newMatrix;
    }

    public bool ColorsAreCompatible(int colorId1, int colorId2)
    {
        int higher = Mathf.Max(colorId1, colorId2);
        int lower = Mathf.Min(colorId1, colorId2);
        if (lower == -1) return false;  // inner face

        return compatibilityMatrix[(higher * higher + higher) / 2 + lower];
    }
}
