﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// Custom inspector for Tile Color Settings 
/// </summary>
[CustomEditor(typeof(TileColorSettings))]
public class TileColorSettingsEditor : Editor
{

    private void Awake()
    {
        ((TileColorSettings)target).UpdateButtonTextures();
    }


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GUILayout.Space(30f);
        TileColorSettings settings = (TileColorSettings)target;

        // Initialize matrix if not done yet
        if (settings.compatibilityMatrix == null)
        {
            settings.InitializeMatrix();
        }

        // If colors were added/removed, adjust matrix
        if (settings.compatibilityMatrix.Length != settings.colors.Length)
        {
            settings.AdjustMatrix();
        }

        // If colors were added/removed, adjust textures
        if (settings.textures == null || settings.colors.Length != settings.textures.Length)
        {
            settings.UpdateButtonTextures();
        }

        // Draw color matrix
        GUILayout.Label("Compatibility Matrix");

        GUIStyle style = GUI.skin.customStyles[3];
        style.fixedHeight = 14;
        style.fixedWidth = 14;
        style.margin = new RectOffset(4, 6, 5, 5);
        style.padding = new RectOffset(0, 0, 0, 3);

        GUILayout.BeginHorizontal(style);
        GUILayout.Space(24);

        for (int i = 0; i < settings.colors.Length; ++i)
        {
            GUILayout.Box(settings.textures[i], style);
        }
        GUILayout.EndHorizontal();

        int offset = 0;
        for (int i = 0; i < settings.colors.Length; ++i)
        {
            GUILayout.BeginHorizontal(style);

            GUILayout.Box(settings.textures[i], style);

            int j = 0;
            for (; j <= i; ++j)
            {
                EditorGUI.BeginChangeCheck();
                bool newValue = GUILayout.Toggle(settings.compatibilityMatrix[i + j + offset], "");
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(settings, "Changed compatibility");
                    settings.compatibilityMatrix[i + j + offset] = newValue;
                    EditorUtility.SetDirty(settings);
                    PrefabUtility.RecordPrefabInstancePropertyModifications(settings);
                }
            }
            offset += j-1;

            GUILayout.EndHorizontal();
        }

        GUILayout.Space(30);

        if (GUILayout.Button("Reset Matrix"))
        {
            settings.InitializeMatrix();
            EditorUtility.SetDirty(settings);
        }

        GUILayout.Space(10);

        if (GUILayout.Button("Print Compatibility"))
        {
            for (int i = 0; i < settings.colors.Length; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    Debug.Log($"{i},{j}: {settings.ColorsAreCompatible(i, j)}");
                }
            }
        }
    }
}
