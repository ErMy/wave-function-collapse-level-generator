﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace WaveFunctionCollapse
{
    [CustomEditor(typeof(LevelGenerator))]
    public class LevelGeneratorEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            LevelGenerator gen = (LevelGenerator)target;

            GUILayout.Space(10f);

            if (GUILayout.Button("Generate"))
            {
                if (gen.StartGenerator())
                {
                    EditorSceneManager.MarkSceneDirty(gen.gameObject.scene);
                }
            }

            GUILayout.Space(10f);

            if (GUILayout.Button("Clear"))
            {
                gen.Clear();
            }

            GUILayout.Space(10f);

            if (GUILayout.Button("Test Stuff"))
            {
                gen.TestStuff();
            }

            GUILayout.Space(20f);
            base.OnInspectorGUI();

        }
    }
}
