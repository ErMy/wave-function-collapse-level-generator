﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Linq;
using System;

namespace WaveFunctionCollapse
{
    /// <summary>
    /// Custom editor to allow fast editing of TilePrototype settings in the Scene View
    /// </summary>
    [CustomEditor(typeof(TilePrototype))]
    public class TilePrototypeEditor : Editor
    {
        /// <summary>
        /// Maps ControllIDs to its handle in the scene. Generated every GUI Update.
        /// </summary>
        Dictionary<int, FaceHandle> controllIdToFaceHandle;

        Dictionary<int, PathHandle> controllIdToPathHandle;
        /// <summary>
        /// Scale of the rectangle handles. Max 1.
        /// </summary>
        readonly float faceScale = 0.8f;

        readonly Color[] pathColors = new Color[] { new Color(0,0,0,0.5f), new Color(0,1,0,0.5f), new Color(1,0,0,0.5f) };

        private void Awake()
        {
            TilePrototype prototype = (TilePrototype)target;
            // Generate a tile for the TilePrototype if there are none. 
            // There should always be at least one.
            if (prototype.posToTile.Count == 0)
            {
                if (prototype.posToTile.Count == 0)
                {
                    prototype.AddTile(new Vector3Int(0, 0, 0));
                }
                else
                {
                    prototype.FillDictionaryWithSavedTiles();
                }
            }
            // Load the color settings file or create one if unavailable.
            if (prototype.colorSettings == null)
                prototype.colorSettings = LoadColorSettings();
            prototype.colorSettings.UpdateButtonTextures();
        }

        protected virtual void OnSceneGUI()
        {
            TilePrototype prototype = (TilePrototype)target;
            Transform protoTrans = prototype.transform;

            // Wait until color settings are loaded
            if (prototype.colorSettings == null)
                return;

            // ControllID of the handle closest to the mouse
            int nearestControl = HandleUtility.nearestControl;

            if (selectedMode == (int) TileEditMode.Path)
            {
                CreatePathHandles(prototype);

                switch(Event.current.type)
                {
                    case EventType.MouseDown:
                        if (Event.current.button == 0)
                        {
                            GUIUtility.hotControl = nearestControl;
                            GUIUtility.keyboardControl = nearestControl;
                            Event.current.Use();
                            // Change path handle value
                            TogglePathHandle(nearestControl, prototype);
                        }
                        break;
                    case EventType.MouseUp:
                        if (controllIdToPathHandle.ContainsKey(GUIUtility.hotControl) && (Event.current.button == 0))
                        {
                            GUIUtility.hotControl = 0;
                            Event.current.Use();
                        }
                        break;
                    case EventType.Repaint:
                        Vector3 cameraPos = SceneView.lastActiveSceneView.camera.transform.position;

                        // sort faces by distance
                        var enumerable = controllIdToPathHandle.OrderByDescending(x =>
                        {
                            Vector3 tilePosition = prototype.transform.position + prototype.origin + prototype.transform.rotation * Vector3.Scale(prototype.tileSize, x.Value.tilePosition);
                            Vector3 forward = Direction.Forward(x.Value.direction, prototype.transform) * Direction.ForwardLength(x.Value.direction, prototype.tileSize) * 0.5f;
                            return Vector3.Distance(cameraPos, tilePosition + forward);
                        });


                        foreach (KeyValuePair<int, PathHandle> idToPathHandle in enumerable)
                        {
                            Handles.DrawSolidRectangleWithOutline(GetPathHandleVertices(prototype, idToPathHandle.Value), pathColors[idToPathHandle.Value.value], Color.white);
                        }
                        break;
                    case EventType.Layout:
                        cameraPos = SceneView.lastActiveSceneView.camera.transform.position;

                        // sort faces by distance
                        enumerable = controllIdToPathHandle.OrderByDescending(x =>
                        {
                            Vector3 tilePosition = prototype.transform.position + prototype.origin + prototype.transform.rotation * Vector3.Scale(prototype.tileSize, x.Value.tilePosition);
                            Vector3 forward = Direction.Forward(x.Value.direction, prototype.transform) * Direction.ForwardLength(x.Value.direction, prototype.tileSize) * 0.5f;
                            return Vector3.Distance(cameraPos, tilePosition + forward);
                        });

                        foreach (KeyValuePair<int, PathHandle> idToPathHandle in enumerable)
                        {
                            AddPathControl(idToPathHandle.Key, prototype, idToPathHandle.Value, prototype.tileSize);
                        }
                        break;
                }

            } else
            {
                switch (Event.current.type)
                {
                    // On mouse click on a handle, execute the function of the tool selected in the inspector
                    case EventType.MouseDown:
                        CreateFaceHandles(prototype);
                        if (controllIdToFaceHandle.ContainsKey(nearestControl))
                        {
                            if (Event.current.button == 0)
                            {
                                GUIUtility.hotControl = nearestControl;
                                GUIUtility.keyboardControl = nearestControl;
                                Event.current.Use();
                                switch ((TileEditMode)selectedMode)
                                {
                                    case TileEditMode.AddTile:
                                        AddTile(nearestControl, prototype);
                                        break;
                                    case TileEditMode.DeleteTile:
                                        DeleteTile(nearestControl, prototype);
                                        break;
                                    case TileEditMode.PaintFace:
                                        PaintFace(nearestControl, prototype);
                                        break;
                                    case TileEditMode.SplitFace:
                                        SplitFace(nearestControl, prototype);
                                        break;
                                    case TileEditMode.MergeFace:
                                        MergeFace(nearestControl, prototype);
                                        break;
                                }
                            }
                        }
                        break;
                    case EventType.MouseUp:
                        CreateFaceHandles(prototype);
                        if (controllIdToFaceHandle.ContainsKey(GUIUtility.hotControl) && (Event.current.button == 0))
                        {
                            GUIUtility.hotControl = 0;
                            Event.current.Use();
                        }
                        break;
                    // Draws the rectangles visible in the scene
                    case EventType.Repaint:
                        CreateFaceHandles(prototype);
                        Color hoverColor = prototype.colorSettings.colors[selectedColor];
                        Vector3 cameraPos = SceneView.lastActiveSceneView.camera.transform.position;

                        // sort faces by distance
                        var enumerable = controllIdToFaceHandle.OrderByDescending(x =>
                        {
                            Vector3 tilePosition = prototype.transform.position + prototype.origin + prototype.transform.rotation * Vector3.Scale(prototype.tileSize, x.Value.tilePosition);
                            Vector3 forward = Direction.Forward(x.Value.direction, prototype.transform) * Direction.ForwardLength(x.Value.direction, prototype.tileSize) * 0.5f;
                            return Vector3.Distance(cameraPos, tilePosition + forward);
                        });


                        foreach (KeyValuePair<int, FaceHandle> idToFaceHandle in enumerable)
                        {
                            if (idToFaceHandle.Value.isSplit || idToFaceHandle.Value.subfaceIndex == 0)
                            {
                                Color notHoverColor = prototype.colorSettings.colors[GetFaceColorId(prototype, idToFaceHandle.Value)];
                                if (nearestControl == idToFaceHandle.Key)
                                    // Use different colors when using PaintFace Mode
                                    if (selectedMode == (int)TileEditMode.PaintFace)
                                        Handles.DrawSolidRectangleWithOutline(GetFaceVertices(prototype, idToFaceHandle.Value), hoverColor, Color.white);
                                    else
                                        Handles.DrawSolidRectangleWithOutline(GetFaceVertices(prototype, idToFaceHandle.Value), notHoverColor, Color.white);
                                else
                                    Handles.DrawSolidRectangleWithOutline(GetFaceVertices(prototype, idToFaceHandle.Value), notHoverColor, Color.black);
                            }
                        }
                        break;
                    // "Draw" interactable rectangles with a ControllID at the same positions as the visible rectangles
                    case EventType.Layout:
                        CreateFaceHandles(prototype);

                        cameraPos = SceneView.lastActiveSceneView.camera.transform.position;

                        // sort faces by distance
                        enumerable = controllIdToFaceHandle.OrderByDescending(x =>
                        {
                            Vector3 tilePosition = prototype.transform.position + prototype.origin + prototype.transform.rotation * Vector3.Scale(prototype.tileSize, x.Value.tilePosition);
                            Vector3 forward = Direction.Forward(x.Value.direction, prototype.transform) * Direction.ForwardLength(x.Value.direction, prototype.tileSize) * 0.5f;
                            return Vector3.Distance(cameraPos, tilePosition + forward);
                        });

                        foreach (KeyValuePair<int, FaceHandle> idToFaceHandle in enumerable)
                        {
                            AddControl(idToFaceHandle.Key, prototype, idToFaceHandle.Value, prototype.tileSize);
                        }
                        break;
                }
            }


        }

        /// <summary>
        /// Read the tile data from TilePrototype and create the handle data used to draw the handles
        /// </summary>
        void CreateFaceHandles(TilePrototype prototype)
        {
            controllIdToFaceHandle = new Dictionary<int, FaceHandle>();
            int controllId = -10;
            foreach (KeyValuePair<Vector3Int, EditorTile> posToTile in prototype.posToTile)
            {
                for (int dir = 0; dir < 6; dir++)
                {
                    if (posToTile.Value.HasFace(dir))
                    {
                        if (posToTile.Value.isSplit[dir])
                        {
                            for (int faceIndex = 0; faceIndex < 4; faceIndex++)
                            {
                                AddFaceHandle(controllId--, posToTile.Key, dir, faceIndex, true);
                            }
                        }
                        else
                        {
                            AddFaceHandle(controllId--, posToTile.Key, dir, 0, false);
                        }
                    }
                }
            }
        }

        private void AddFaceHandle(int id, Vector3Int position, int direction, int subfaceIndex, bool isSplit)
        {
            FaceHandle faceHandle = new FaceHandle()
            {
                tilePosition = position,
                direction = direction,
                subfaceIndex = subfaceIndex,
                isSplit = isSplit
            };
            controllIdToFaceHandle.Add(id, faceHandle);
        }

        /// <summary>
        /// Calculate the vertices of the rectangle handle/face we're going to draw
        /// </summary>
        Vector3[] GetFaceVertices(TilePrototype tile, FaceHandle face)
        {
            // position of the pivot (0,0,0)
            Vector3 tilePosition = tile.transform.position;

            Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, tile.transform.rotation, tile.transform.localScale.SignOnly());
            Vector3 offset = Vector3.Scale(tile.tileSize, face.tilePosition);
            tilePosition += m.MultiplyPoint3x4(offset + tile.origin);

            Vector3 forward = Direction.Forward(face.direction, tile.transform) * Direction.ForwardLength(face.direction, tile.tileSize) * 0.5f;
            Vector3 right = Direction.Right(face.direction, tile.transform) * Direction.RightLength(face.direction, tile.tileSize) * 0.5f;
            Vector3 up = Direction.Up(face.direction, tile.transform) * Direction.UpLength(face.direction, tile.tileSize) * 0.5f;

            if (face.isSplit)
            {
                if (face.subfaceIndex == 0)
                    tilePosition += -0.5f * right + 0.5f * up;
                else if (face.subfaceIndex == 1)
                    tilePosition += 0.5f * right + 0.5f * up;
                else if (face.subfaceIndex == 2)
                    tilePosition += -0.5f * right - 0.5f * up;
                else
                    tilePosition += 0.5f * right - 0.5f * up;
                right *= 0.5f * faceScale;
                up *= 0.5f * faceScale;
            }
            else
            {
                right *= faceScale;
                up *= faceScale;
            }
            return new Vector3[]
            {
            tilePosition + forward + right + up,
            tilePosition + forward + right - up,
            tilePosition + forward - right - up,
            tilePosition + forward - right + up
            };
        }

        /// <summary>
        /// Get the id of the color this face has (id is the index of the color in ColorSettings)
        /// </summary>
        private int GetFaceColorId(TilePrototype prototype, FaceHandle faceHandle)
        {
            return prototype.posToTile[faceHandle.tilePosition].faceIDs[faceHandle.direction][faceHandle.subfaceIndex];
        }

        /// <summary>
        /// "Draw" the interactable rectangles. Used for the GUI Layout event
        /// </summary>
        void AddControl(int controllID, TilePrototype prototype, FaceHandle face, Vector3 tileSize)
        {
            Transform tileT = prototype.transform;
            Vector3 tilePosition = tileT.position;

            // offset by tile position and rotation, scale
            Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, tileT.rotation, tileT.localScale.SignOnly());
            Vector3 offset = Vector3.Scale(prototype.tileSize, face.tilePosition);
            tilePosition += m.MultiplyPoint3x4(offset + prototype.origin);

            Vector3 forward = Direction.Forward(face.direction, tileT) * Direction.ForwardLength(face.direction, tileSize) * 0.5f;
            float upLength = Direction.UpLength(face.direction, tileSize);
            float rightLength = Direction.RightLength(face.direction, tileSize);

            if (face.isSplit)
            {
                if (face.subfaceIndex == 0)
                {
                    tilePosition += -Direction.Right(face.direction, tileT) * rightLength * 0.25f + Direction.Up(face.direction, tileT) * upLength * 0.25f;

                }
                else if (face.subfaceIndex == 1)
                {
                    tilePosition += Direction.Right(face.direction, tileT) * rightLength * 0.25f + Direction.Up(face.direction, tileT) * upLength * 0.25f;
                }
                else if (face.subfaceIndex == 2)
                {
                    tilePosition += -Direction.Right(face.direction, tileT) * rightLength * 0.25f - Direction.Up(face.direction, tileT) * upLength * 0.25f;
                }
                else
                {
                    tilePosition += Direction.Right(face.direction, tileT) * rightLength * 0.25f - Direction.Up(face.direction, tileT) * upLength * 0.25f;
                }
                upLength *= 0.5f * faceScale;
                rightLength *= 0.5f * faceScale;
            }
            else //if not split
            {
                upLength *= faceScale;
                rightLength *= faceScale;
            }
            //tileT.rotation * Quaternion.LookRotation(-Direction.Forward(face.direction, tileT))
            // for uniform tilesizes
            if (Mathf.Approximately(upLength, rightLength))
            {
                HandleUtility.AddControl(controllID,
                    HandleUtility.DistanceToRectangle(tilePosition + forward,
                    Quaternion.LookRotation(Direction.Forward(face.direction, tileT)),
                    rightLength * 0.5f));
            }
            // for non-uniform tilesizes
            else if (upLength < rightLength)
            {
                int divisions = Mathf.CeilToInt(rightLength / upLength);
                for (int i = 0; i < divisions; ++i)
                {
                    HandleUtility.AddControl(controllID,
                        HandleUtility.DistanceToRectangle(tilePosition
                        + forward
                        - rightLength * Direction.Right(face.direction, tileT) * 0.5f
                        + upLength * Direction.Right(face.direction, tileT) * 0.5f
                        + (rightLength - upLength) * i / (divisions - 1) * Direction.Right(face.direction, tileT),
                        Quaternion.LookRotation(Direction.Forward(face.direction, tileT)),
                        upLength * 0.5f));
                }
            }
            else
            {
                int divisions = Mathf.CeilToInt(upLength / rightLength);
                for (int i = 0; i < divisions; ++i)
                {
                    HandleUtility.AddControl(controllID,
                        HandleUtility.DistanceToRectangle(tilePosition
                        + forward
                        - upLength * Direction.Up(face.direction, tileT) * 0.5f
                        + rightLength * Direction.Up(face.direction, tileT) * 0.5f
                        + (upLength - rightLength) * i / (divisions - 1) * Direction.Up(face.direction, tileT),
                        Quaternion.LookRotation(Direction.Forward(face.direction, tileT)),
                        rightLength * 0.5f));
                }
            }
        }

        /// <summary>
        /// Split a single face/handle into 4
        /// </summary>
        void SplitFace(int controllID, TilePrototype prototype)
        {
            if (controllIdToFaceHandle.TryGetValue(controllID, out FaceHandle faceHandle))
            {
                EditorTile tile = prototype.posToTile[faceHandle.tilePosition];
                if (tile.HasFace(faceHandle.direction))
                {
                    int id = tile.faceIDs[faceHandle.direction][faceHandle.subfaceIndex];
                    Undo.RecordObject(target, "Split Face");
                    tile.isSplit[faceHandle.direction] = true;
                    faceHandle.isSplit = true;
                    tile.SetAll(faceHandle.direction, id);
                    prototype.SaveToList();
                    EditorUtility.SetDirty(prototype);
                }
            }
        }

        /// <summary>
        /// Merge 4 faces/handles into a single one
        /// </summary>
        void MergeFace(int controllID, TilePrototype prototype)
        {
            if (controllIdToFaceHandle.TryGetValue(controllID, out FaceHandle faceHandle))
            {
                EditorTile tile = prototype.posToTile[faceHandle.tilePosition];
                if (tile.HasFace(faceHandle.direction))
                {
                    int id = tile.faceIDs[faceHandle.direction][faceHandle.subfaceIndex];
                    Undo.RecordObject(target, "Merge Face");
                    tile.isSplit[faceHandle.direction] = false;
                    faceHandle.isSplit = false;
                    tile.SetAll(faceHandle.direction, id);
                    prototype.SaveToList();
                    EditorUtility.SetDirty(prototype);
                }
            }
        }

        /// <summary>
        /// Set the color id of a face/handle
        /// </summary>
        void PaintFace(int controllID, TilePrototype prototype)
        {
            if (controllIdToFaceHandle.TryGetValue(controllID, out FaceHandle faceHandle))
            {
                EditorTile tile = prototype.posToTile[faceHandle.tilePosition];
                if (tile.faceIDs[faceHandle.direction][faceHandle.subfaceIndex] != selectedColor)
                {
                    Undo.RecordObject(target, "Paint Face");
                    if (tile.isSplit[faceHandle.direction])
                    {
                        tile.faceIDs[faceHandle.direction][faceHandle.subfaceIndex] = selectedColor;
                    }
                    else
                    {
                        for (int i = 0; i < tile.faceIDs[faceHandle.direction].Length; i++)
                        {
                            tile.faceIDs[faceHandle.direction][i] = selectedColor;
                        }
                    }
                    prototype.SaveToList();
                    EditorUtility.SetDirty(prototype);
                }
            }
        }

        void AddTile(int controllID, TilePrototype prototype)
        {
            FaceHandle faceHandle = controllIdToFaceHandle[controllID];
            prototype.AddTile(faceHandle.tilePosition + Direction.ToVector(faceHandle.direction));
            EditorUtility.SetDirty(prototype);
            EditorSceneManager.MarkSceneDirty(prototype.gameObject.scene);
        }

        void DeleteTile(int controllID, TilePrototype prototype)
        {
            if (controllIdToFaceHandle.TryGetValue(controllID, out FaceHandle faceHandle))
            {
                prototype.RemoveTile(faceHandle.tilePosition);
                EditorUtility.SetDirty(prototype);
                EditorSceneManager.MarkSceneDirty(prototype.gameObject.scene);
            }
        }

        struct FaceHandle
        {
            public Vector3Int tilePosition;
            /// <summary>
            /// Which direction is the tile facing in its local coordinate system. E.g. top tile -> Direction.Up
            /// </summary>
            public int direction;
            public int subfaceIndex;
            public bool isSplit;
        }



        /////////////////////////////////////////////////
        // Path Handles
        ////////////////////////////////////////////////

        struct PathHandle
        {
            public Vector3Int tilePosition;
            public int direction;
            public int value;
        }
        void CreatePathHandles(TilePrototype prototype)
        {
            controllIdToPathHandle = new Dictionary<int, PathHandle>();
            int controllId = -10;

            foreach (KeyValuePair<Vector3Int, EditorTile> posToTile in prototype.posToTile)
            {
                for (int dir = 0; dir < 6; dir++)
                {
                    controllIdToPathHandle.Add(controllId--, new PathHandle { direction = dir, tilePosition = posToTile.Key, value = posToTile.Value.path[selectedSubmode][dir] });
                }
            }
        }

        void TogglePathHandle(int controllID, TilePrototype prototype)
        {
            if (controllIdToPathHandle.TryGetValue(controllID, out PathHandle pathHandle))
            {
                EditorTile tile = prototype.posToTile[pathHandle.tilePosition];

                Undo.RecordObject(target, "Toggle Path Handle");
                tile.path[selectedSubmode][pathHandle.direction] = (tile.path[selectedSubmode][pathHandle.direction] + 1) % 3;
                prototype.SaveToList();
                EditorUtility.SetDirty(prototype);
            }
        }

        Vector3[] GetPathHandleVertices(TilePrototype tile, PathHandle pathHandle)
        {
            // position of the pivot (0,0,0)
            Vector3 tilePosition = tile.transform.position;

            Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, tile.transform.rotation, tile.transform.localScale.SignOnly());
            Vector3 offset = Vector3.Scale(tile.tileSize, pathHandle.tilePosition);
            tilePosition += m.MultiplyPoint3x4(offset + tile.origin);

            Vector3 forward = Direction.Forward(pathHandle.direction, tile.transform) * Direction.ForwardLength(pathHandle.direction, tile.tileSize) * 0.4f;
            Vector3 right = Direction.Right(pathHandle.direction, tile.transform) * Direction.RightLength(pathHandle.direction, tile.tileSize) * 0.4f;
            Vector3 up = Direction.Up(pathHandle.direction, tile.transform) * Direction.UpLength(pathHandle.direction, tile.tileSize) * 0.4f;

            right *= 0.7f;
            up *= 0.7f;

            return new Vector3[]
            {
                tilePosition + forward + right + up,
                tilePosition + forward + right - up,
                tilePosition + forward - right - up,
                tilePosition + forward - right + up
            };
        }

        void AddPathControl(int controllID, TilePrototype prototype, PathHandle handle, Vector3 tileSize)
        {
            Transform tileT = prototype.transform;
            Vector3 tilePosition = tileT.position;

            // offset by tile position and rotation, scale
            Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, tileT.rotation, tileT.localScale.SignOnly());
            Vector3 offset = Vector3.Scale(prototype.tileSize, handle.tilePosition);
            tilePosition += m.MultiplyPoint3x4(offset + prototype.origin);

            Vector3 forward = Direction.Forward(handle.direction, tileT) * Direction.ForwardLength(handle.direction, tileSize) * 0.4f;
            float upLength = Direction.UpLength(handle.direction, tileSize);
            float rightLength = Direction.RightLength(handle.direction, tileSize);

            upLength *= 0.7f;
            rightLength *= 0.7f;

            //tileT.rotation * Quaternion.LookRotation(-Direction.Forward(face.direction, tileT))
            // for uniform tilesizes
            if (Mathf.Approximately(upLength, rightLength))
            {
                HandleUtility.AddControl(controllID,
                    HandleUtility.DistanceToRectangle(tilePosition + forward,
                    Quaternion.LookRotation(Direction.Forward(handle.direction, tileT)),
                    rightLength * 0.4f));
            }
            // for non-uniform tilesizes
            else if (upLength < rightLength)
            {
                int divisions = Mathf.CeilToInt(rightLength / upLength);
                for (int i = 0; i < divisions; ++i)
                {
                    HandleUtility.AddControl(controllID,
                        HandleUtility.DistanceToRectangle(tilePosition
                        + forward
                        - rightLength * Direction.Right(handle.direction, tileT) * 0.4f
                        + upLength * Direction.Right(handle.direction, tileT) * 0.4f
                        + (rightLength - upLength) * i / (divisions - 1) * Direction.Right(handle.direction, tileT),
                        Quaternion.LookRotation(Direction.Forward(handle.direction, tileT)),
                        upLength * 0.4f));
                }
            }
            else
            {
                int divisions = Mathf.CeilToInt(upLength / rightLength);
                for (int i = 0; i < divisions; ++i)
                {
                    HandleUtility.AddControl(controllID,
                        HandleUtility.DistanceToRectangle(tilePosition
                        + forward
                        - upLength * Direction.Up(handle.direction, tileT) * 0.4f
                        + rightLength * Direction.Up(handle.direction, tileT) * 0.4f
                        + (upLength - rightLength) * i / (divisions - 1) * Direction.Up(handle.direction, tileT),
                        Quaternion.LookRotation(Direction.Forward(handle.direction, tileT)),
                        rightLength * 0.4f));
                }
            }
        }

        /////////////////////////////////////////////////
        // Custom Inspector
        /////////////////////////////////////////////////

        /// <summary>
        /// Which Edit Mode is currently selected in the inspector
        /// </summary>
        int selectedMode = 0;
        /// <summary>
        /// Which color is currently selected in the inspector
        /// </summary>
        int selectedColor = 0;
        /// <summary>
        /// Which submode is currently selected
        /// </summary>
        int selectedSubmode = 0;
        enum TileEditMode
        {
            PaintFace = 0, AddTile = 1, DeleteTile = 2, SplitFace = 3, MergeFace = 4, Path = 5
        }

        enum PathEditMode
        {
            Top = 0, Front = 1, Right = 2, Down = 3, Back = 4, Left = 5
        }

        /// <summary>
        /// Draw the custom inspector
        /// </summary>
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            TilePrototype prototype = (TilePrototype)target;

            GUILayout.Space(30f);

            int previousMode = selectedMode;
            int previousSubMode = selectedSubmode;

            selectedMode = GUILayout.Toolbar(selectedMode, new string[] { "Paint", "Add", "Del", "Split", "Merge", "Path" });

            GUILayout.Space(20f);

            if (selectedMode == (int)TileEditMode.Path)
            {
                // Path Menu
                selectedSubmode = GUILayout.Toolbar(selectedSubmode, new string[] { "Top", "Front", "Right", "Down", "Back", "Left" });

            } else
            {
                // Color Selector
                if (prototype.colorSettings == null || prototype.colorSettings.textures == null)
                    return;

                GUIStyle style = new GUIStyle(GUI.skin.button);
                style.fixedHeight = 50;
                style.fixedWidth = 50;

                int temp = selectedColor;
                selectedColor = GUILayout.SelectionGrid(selectedColor, prototype.colorSettings.textures, 4, style);

                if (temp != selectedColor)
                {
                    selectedMode = 0;
                }
            }

            if (selectedMode != previousMode || selectedSubmode != previousSubMode)
            {
                EditorWindow view = EditorWindow.GetWindow<SceneView>();
                view.Repaint();
            }

            // Debug Info
            GUILayout.Space(20f);

            GUILayout.Label($"Tiles: {prototype.posToTile.Count}");

            foreach (var tile in prototype.posToTile)
            {
                for (int dir = 0; dir < 6; dir++)
                {
                    GUILayout.Label($"Position: {tile.Key}");
                    GUILayout.Label($"{Direction.ToString(dir)}: {tile.Value.faceIDs[dir][0]}, {tile.Value.faceIDs[dir][1]}, {tile.Value.faceIDs[dir][2]}, {tile.Value.faceIDs[dir][3]}");
                }
            }
        }

        /// <summary>
        /// Find the color settings scriptable object or create one and return it
        /// </summary>
        private TileColorSettings LoadColorSettings()
        {
            string[] results;
            results = AssetDatabase.FindAssets("WFC Color Settings t:TileColorSettings", new[] { "Assets/WFC/Settings" });

            string path;
            if (results.Length > 0)
            {
                path = AssetDatabase.GUIDToAssetPath(results[0]);
                return (TileColorSettings)AssetDatabase.LoadAssetAtPath(path, typeof(TileColorSettings));
            }
            else
            {
                path = "Assets/WFC/Settings/WFC Color Settings.asset";
                TileColorSettings settings = CreateInstance<TileColorSettings>();
                AssetDatabase.CreateAsset(settings, path);
                return settings;
            }
        }
    }
}